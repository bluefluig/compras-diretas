/** Jean Varlet - Naturezas de Despesas --- */

function naturezaField(CODNATUREZA, posicao){
    let urlRest = "/api/public/ecm/dataset/search?datasetId=ds_VW_VALOR_NATUREZA&searchField=DESCRICAO";
   
    
    autocompleteAcao = FLUIGC.autocomplete('#naturezaProd___'+posicao, {
        source: {
            url: urlRest,
            patternKey: "searchValue",
            limit: 100,
            root: "content"
        },
        highlight: true,
        displayKey: 'DESCRICAO',
        type: 'tagAutocomplete',
        minLength: 0,
        searchTimeout: 100,
        maxTags: 1,
        tagMaxWidth: 500
    }, function (err, data){
        if (err) {
            
        } else {
            
        }
    })
    
    autocompleteAcao.on('fluig.autocomplete.itemAdded', function (result){
        console.log(result.item.CODTB1FAT)
        $("#naturezaAno___"+posicao).val(result.item.ANO);
        $("#naturezaSald___"+posicao).val(parseFloat(result.item.VALORLIQUIDO).toFixed(2).toString());

       //fAnalisaSaldoOrcamento(posicao, result.item.CODUNIDADE, parseInt($("input", $("#dataEntrega")).val().split('/')[1]), parseInt($("input", $("#dataEntrega")).val().split('/')[2]))
    })
}

function selNaturezaItem(CODTB1FAT, posicao){
    
    let retorno = DatasetFactory.getDataset('ds_VW_VALOR_NATUREZA', null, null, null);
    if(!jQuery.isEmptyObject(retorno)){
        if(retorno.values.length){
            for(let i = 0; i < retorno.values.length; i++){
                
                if(CODTB1FAT == retorno.values[i].CODTB1FAT){
                    
                     $("#naturezaProdSel___"+posicao).val(retorno.values[i].DESCRICAO);  
                     $("#naturezaAno___"+posicao).val(retorno.values[i].ANO);
                     $("#naturezaCod___"+posicao).val(retorno.values[i].CODTB1FAT);
                     $("#naturezaSald___"+posicao).val(parseFloat(retorno.values[i].VALORLIQUIDO).toFixed(2).toString().replace('.',',')); 
                     $("#naturezaProdSel___"+posicao).prop("readonly", true);

                     if(!parseFloat(retorno.values[i].VALORLIQUIDO).toFixed(2) > 0){
                        toast('Atenção!', 'A natureza orçamentária para o item selecionada não possui recursos.', 'danger');
                        $("#dv"+posicao).css("background-color", "Coral");
                        $("#dv"+posicao).css("color", "white");
                     }else{
                        $("#dv"+posicao).css("background-color", "white");
                        $("#dv"+posicao).css("color", "black");
                     }
                     
                }
            }
    
        }
            
    }else{
        toast('Erro!', 'Não foi possível retornar a natureza para esse item de compra', 'danger');
    }

}

// Validador de naturezas saldos

var itensCompra = [];
function mapaNaturezas(){
    let _categorias = [];
    let _parsed = JSON.parse($("#conteudoRateios").val());
    //console.log(_parsed);
    if(_parsed.length){
        for(let c = 0; c < _parsed.length; c++){
             _categorias.push(_parsed[c].codNatureza);
        }
        _categorias = _categorias.filter(function(test, i){
            return _categorias.indexOf(test) === i;
        })
        console.log(_categorias)
        for(let cat = 0; cat < _categorias.length; cat++){//percorre as categorias (naturezas)
            var _temp = [];
            for(let i = 0; i < _parsed.length; i ++){// percorre os produtos 
                var codNat = _parsed[i].codNatureza;
                var saldNat = _parsed[i].saldoNatureza;
                var idProd = _parsed[i].idProduto;
                var nomProd = _parsed[i].titulo;
                var _qtd = _parsed[i].quantidade;
                if(codNat == _categorias[i]){
                    var _obj = {
                        "codigoNatureza": codNat,
                        "saldoNatureza": saldNat,
                        "idProduto": idProd,
                        "titulo": nomProd,
                        "quantidade": _qtd

                    }
                    _temp.push(_obj)    
                }
                itensCompra.push(_temp);

            }

        }
        
    }

}
//mapaNaturezas()