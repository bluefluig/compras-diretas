/*Função de cria a estrutura de diretórios que ficarão armazenadas os anexos da solicitação*/
//TODO: simplificar essa função
function createFolderStructure(rootFolder){
	var idUserFolder;
	if($("#idSolicitacao").val() == "" ){
		const solicitacaoNum = `Solicitação ${document.getElementById('codigoSolicitacao').value}`;
		const matriculaColab = `Matrícula ${document.getElementById('matriculaSolicitante').value}`;
		console.log('createFolderStructure() -> solicitacaoNum'+solicitacaoNum);
		
		let response = ajaxApi('listarDocumentos', rootFolder);
		
		var myfolder = response.content.filter(function(folder){
			if(folder.type == 1 && folder.description == "Anexos de Solicitações"){
				return folder;
	        }
			});
		
		//se a pasta de anexos não existe, consequentemente as filhas também não
		if(!myfolder.length){
			
			var anexosSolicitacao = ajaxApi("criarPasta", createFolderJson(rootFolder, "Anexos de Solicitações"));
			var pastaSolicitacao = ajaxApi('criarPasta', createFolderJson(anexosSolicitacao.content.documentId, solicitacaoNum));
			var anexosUser = ajaxApi('criarPasta', createFolderJson(pastaSolicitacao.content.documentId, matriculaColab));
			idUserFolder = anexosUser.content.documentId;
			
		}else{
			var pastaSolicitacao = ajaxApi('listarDocumentos', myfolder[0].id).content.filter(function(folder){
				if(folder.type == 1 && folder.description == solicitacaoNum){
					return folder;
				}
			});
			
			if(!pastaSolicitacao.length){
				
				var solicitacao = ajaxApi('criarPasta',createFolderJson(myfolder[0].id, solicitacaoNum));
				var matColab = ajaxApi('criarPasta', createFolderJson(solicitacao.content.documentId, matriculaColab));
				idUserFolder = matColab.content.documentId
				
			}else{
				var pastaUsuario = ajaxApi('listarDocumentos', pastaSolicitacao[0].id).content.filter(function(folder){
					if(folder.type == 1 && folder.description == matriculaColab){
						return folder;
					}
				});
				
				if(!pastaUsuario.length){
					
					ajaxApi('criarPasta', createFolderJson(pastaSolicitacao[0].id, matriculaColab));
					
				}else{
					//se a pasta já existe, é necessário guardar seu id para uso posterior
					idUserFolder = pastaUsuario[0].id
				}
			}
		}
		//guardamos o id da pasta do usuário para usos posteriores
		$("#pastaAnexos").val(idUserFolder);
		
	}else{
		
		setTimeout(function(){
			const solicitacaoNum = `Solicitação ${document.getElementById('codigoSolicitacao').value}`;
			const matriculaColab = `Matrícula ${document.getElementById('matriculaSolicitante').value}`;
			console.log('createFolderStructure() -> solicitacaoNum'+solicitacaoNum);
			
			let response = ajaxApi('listarDocumentos', rootFolder);
			
			var myfolder = response.content.filter(function(folder){
				if(folder.type == 1 && folder.description == "Anexos de Solicitações"){
					return folder;
		        }
				});
			
			//se a pasta de anexos não existe, consequentemente as filhas também não
			if(!myfolder.length){
				
				var anexosSolicitacao = ajaxApi("criarPasta", createFolderJson(rootFolder, "Anexos de Solicitações"));
				var pastaSolicitacao = ajaxApi('criarPasta', createFolderJson(anexosSolicitacao.content.documentId, solicitacaoNum));
				var anexosUser = ajaxApi('criarPasta', createFolderJson(pastaSolicitacao.content.documentId, matriculaColab));
				idUserFolder = anexosUser.content.documentId;
				
			}else{
				var pastaSolicitacao = ajaxApi('listarDocumentos', myfolder[0].id).content.filter(function(folder){
					if(folder.type == 1 && folder.description == solicitacaoNum){
						return folder;
					}
				});
				
				if(!pastaSolicitacao.length){
					
					var solicitacao = ajaxApi('criarPasta',createFolderJson(myfolder[0].id, solicitacaoNum));
					var matColab = ajaxApi('criarPasta', createFolderJson(solicitacao.content.documentId, matriculaColab));
					idUserFolder = matColab.content.documentId
					
				}else{
					var pastaUsuario = ajaxApi('listarDocumentos', pastaSolicitacao[0].id).content.filter(function(folder){
						if(folder.type == 1 && folder.description == matriculaColab){
							return folder;
						}
					});
					
					if(!pastaUsuario.length){
						
						ajaxApi('criarPasta', createFolderJson(pastaSolicitacao[0].id, matriculaColab));
						
					}else{
						//se a pasta já existe, é necessário guardar seu id para uso posterior
						idUserFolder = pastaUsuario[0].id
					}
				}
			}
			//guardamos o id da pasta do usuário para usos posteriores
			$("#pastaAnexos").val(idUserFolder);
			
		},2000)
	}
	
	
}
function ajaxApi(funcao, dadosJson) {	
    let restApiEcm = 'api/public/ecm/document';
    let url;
    let msgSucesso;
    let assincrono = false;
    let resultado = null;
    var metodo ='POST';      
    switch (funcao) {
          case 'criarArquivo':
               url = `/${restApiEcm}/createDocument`;
               msgSucesso = 'Documento inserido com sucesso!';
               break;
          case 'criarPasta':
               url = `/api/public/2.0/folderdocuments/create`;
               msgSucesso = 'Pasta criada com sucesso!';
               break;
          case 'listarDocumentos':
               url =  `/${restApiEcm}/listDocument/${dadosJson}`;
               metodo = 'GET';
               msgSucesso = 'Listagem realizada com sucesso!';
               break;
          case 'remover':
               url = `/${restApiEcm}/remove`;
               msgSucesso = 'Remoção realizada com sucesso!';
               break;
          case 'alterar':
               url = `/${restApiEcm}/updateDocumentFolder`;
               msgSucesso = 'Documento alterado com sucesso!';
               break;
          case 'enviarAnexo':
        	  url = `/${restApiEcm}/`
        	  break
          default:
               toast('Erro!', 'Função não encontrada.',  'danger', 4000);
               console.log('Função não encontrada.');
               return null;
    }
    console.log(metodo)
    $.ajax({
          async: assincrono,
          url: url,
          type: metodo,
          data: dadosJson,
          contentType: 'application/json',
          success: function (dados) {
               console.log(`Requisição para URL: ${url}  executada com sucesso!`);
               resultado = dados;
          },
          error: function (objErro, status, msg) {
               console.log(`Erro: ${msg}`);
               resultado = objErro;
          }
    });
    return resultado;
  }

/**cria a estrutura primária necessária para criação de uma pasta através da api
 * 
 * @param parentFolderId {int} [obrigatorio]
 * @param documentDescription {string} [obrigatorio]
 * 
*/
function createFolderJson(parentFolderId, documentDescription, expires=false, publisherId="Todos", inheritSecurity=true){
	return JSON.stringify({
		"parentFolderId": parentFolderId,
		"documentDescription": documentDescription,
		"expires": expires,
		"publisherId": publisherId,
		"inheritSecurity": inheritSecurity
	});
}
//recebe uma lista de arquivos e os põe na lista de anexos
function appendFilesInUi(fileList, listAtt){
	
	//atualiza o contador de arquivos
	$('span.badge').text(fileList.length) ;
	
	Array.from(fileList).forEach(function(file, i){
		
		var i = document.createElement("i");
		var classOfI = "flaticon flaticon-@ icon-md fs-md-margin-right"
		
		if(file.type.includes('image')){
			classOfI = classOfI.replace("@", "image");
		}else{
			classOfI = classOfI.replace("@", "file");
		}
		i.setAttribute("class", classOfI);

		var li = document.createElement("li");
		li.setAttribute("class","list-group-item d-flex justify-content-between");
		li.setAttribute("id",`image-item-${i}`);
		li.appendChild(i);
		li.appendChild(document.createTextNode(file.name));
		
		var btn = document.createElement("button");
		btn.setAttribute("class", "close");
		btn.setAttribute("title", `cancelar o envio de ${file.name}`);
		btn.setAttribute("type", "button");
		btn.setAttribute("aria-label", "Close");
		var spn = document.createElement("span");
		spn.setAttribute("aria-hidden","true");
		spn.innerText="x";
		btn.appendChild(spn);
		
		/*exclui o anexo da lista e atualiza o contador de arquivos*/
		btn.onclick = () =>{
			var liDeletedId = btn.parentNode.id;
			$(`#${liDeletedId}`).remove();
			
			/**
			 * FileLists não podem ser alteradas, portanto, salvamos os nomes dos arquivos
			 * que o usuário desistiu de enviar para filtrarmos na hora do envio dos anexos mais tarde.
			 * */
			anexosDeletados.push(file.name);
			
			badge = $('span.badge');
			console.log(badge[0].innerText , 'BADGE')
			$('span.badge').text((parseInt(badge[0].innerText) -1)) ;
			
		}

		
		li.appendChild(btn);
		listAtt.append(li);
	});
}
var anexosDeletados = [];
function createModalOfAttachments(atividade){
	var modal = 
		'<div id="modal-test">'+
			'<ul id="listaAnexos" class="list-group"></ul>'+
			'<input type="file" accept="image/*, .pdf, .doc" id="arquivoAnexado" name="arquivoAnexado" multiple="multiple"style="display:none;">'+
			'<div class="row fs-md-padding-left justify-content-center">'+
				'<button type="button" class="btn btn-primary" id="btnAnexos">'+
				  'Escolha os arquivos <span class="badge badge-light">0</span>'+
				'</button>'+
			'</div>'+
		'</div>'
		
	FLUIGC.modal({
	    title: 'Upload de arquivos',
	    content: modal,
	    id: 'fluig-modal',
	    actions: [{
	        'label': 'Enviar',
	        'bind': 'data-open-modal',
	    },{
	        'label': 'Fechar',
	        'autoClose': true,
	        'bind' :'data-close-modal'
	        
	    }]
	}, function(err, data) {
	    if(err) {
	    	throw err;
	    } else {

	    	$("#btnAnexos").on('click',function(){
	    		$("#arquivoAnexado").click();
	    	});
	    	
	    	/*Escuta a adição de imagens*/
	    	$("#arquivoAnexado").on("change", function(){
	    		$("#listaAnexos").html("");
	    		appendFilesInUi(this.files, $("#listaAnexos"));
	    	});
	    	
	    	/*Escuta o envio das imagens*/
	    	$("button[data-open-modal]").click(function(){
	    		var inputFiles = $("#arquivoAnexado")[0].files;
	    		if(inputFiles.length){
	    			sendAttachments(inputFiles, modo);
	    		}
	    	});
	    	
	    	/*quando o modal é fechado o array de anexos excluidos é limpo*/
	    	$('button[data-close-modal').click(function(){
	    		anexosDeletados = []
	    		setTimeout(function(){
	    			console.log('chmando fnLinkArquivos()-> avitidade -> '+atividade)
	    			fnLinkArquivos($("#pastaAnexos").val(), atividade);		
	    		},200);
	    	});
	    }
	});
}



/**
 * Remove caracteres especiais de um nome de arquivo.
 * @param {File}file
 * @returns {File} 
 */
function normalizeFile(file) {

    var name = file.name.normalize('NFC').replace(/[\u0300-\u036f]/g, "");
    var blob = file.slice(0, file.size, file.type); 
    return new File( [ blob ] , name , {type: file.type});
}



/*Função que percorre o array de imagens e envia cada uma individualmente para a função responsável pelo envio ao GED*/
function sendAttachments(fileList, modo) {
    Array.from(fileList).forEach(file => {
        if (!anexosDeletados.includes(file.name)) {
        	
        	var normalizedFile = normalizeFile(file);
            
        	uploadFileInGED(normalizedFile, modo);
        }
	});	
}
/**Função que envia os anexos do usuário para o GED
*
* @param file {File} obrigatório
*
*/
var arquivonome = 0;
async function uploadFileInGED(file, modo){
    
    
    console.log('uploadFileInGED() ')
	let urlBase = parent.WCMAPI.serverURL;
	let idPasta = $("#pastaAnexos").val();
	if(file != null){


		var myFile = new FormData();
        myFile.append('file', file);
        
        await fetch(urlBase+'/ecm/upload', {
			method: 'POST',
			body: myFile,
		})
		.then( 

			async (data) => {
				await data.json()
				.then( (data) => console.log("Arquivo enviado para upload ", data) )
			
			}
		)

        var data = JSON.stringify({
			"description": file.name,
			"parentId": parseInt(idPasta),
			"expirationDate": "9999-12-31",
			"inheritSecurity": true,
			"attachments": [{
				"fileName": file.name
			}],
        });

        $.ajax({
			method: 'POST',
			url: urlBase + '/api/public/ecm/document/createDocument',
			data: data,
			contentType: 'application/json',
			success: function (data) {
				$("li", $("#listaAnexos")).css('background-color', '#45CD64');
				$("li", $("#listaAnexos")).css('color', 'white');
					
                FLUIGC.toast({
                    title: 'Uplodad de Arquivo:',
                    message: '<br>Arquivo Enviado com Sucesso!',
                    type: 'success'
                });
                console.log(data);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				FLUIGC.toast({
                    title: 'Uplodad de Arquivo:',
                    message: '<br>Não foi possível salvar o arquivo <strong>'+ JSON.parse(data).description +'</strong>',
                    type: 'danger'
                });
                console.log(jqXHR, textStatus, errorThrown);
			},
        });
	}
}



/*
async function uploadFileInGED(file){
	//let urlBase = 'http://fluighomolog.pe.sebrae.com.br:8080';
	let urlBase = parent.WCMAPI.serverURL;
	if(file != null){
		var myFile = new FormData();
		myFile.append('file', file);

		var res = await fetch(urlBase+'/ecm/upload', {method: 'POST',body: myFile});
		arquivonome = parseInt(arquivonome)+1
		if(res){
			
			var response = await res.json();
			var idPasta = $('#pastaAnexos').val();

			response.files.forEach(function(file){

				var data = JSON.stringify({
					//"description": file.name,
					"description": 'ArquivoAnexado-'+arquivonome,
					"parentId": parseInt(idPasta),
					"expirationDate": "9999-12-31",
					"inheritSecurity": false,
					"attachments": [{
						"fileName": file.name
					}],
				});

				fetch(urlBase+'/api/public/ecm/document/createDocument', {
					method : 'POST',
					body: data,
					headers: new Headers({"Content-Type": "application/json"}),
			
				})
				.then(function(res){ //Upload ok == true
					$("li", $("#listaAnexos")).css('background-color', '#45CD64');
					$("li", $("#listaAnexos")).css('color', 'white');
					
					FLUIGC.toast({
						title: 'Uplodad de Arquivo:',
						message: '<br>Arquivo Enviado com Sucesso!',
						type: 'success'
					});
					console.log(res);
				})
				.catch(function(err){ //Upload ok == false
					FLUIGC.toast({
						title: 'Uplodad de Arquivo:',
						message: '<br>Não foi possível salvar o arquivo!',
						type: 'danger'
					});
					console.log(err);
				});
			});
		}
	}

	
}
*/

/*Update - 14/02/2020*/
function abreDocumento(docId, docVersao = 1000, titulo = 'Visualizador de Documentos', maximizado = true) {
	var parentOBJ;

	if (window.opener) {
		parentOBJ = window.opener.parent;
	} else {
		parentOBJ = parent;
	}

	var cfg = {
		url: '/ecm_documentview/documentView.ftl',
		maximized: maximizado,
		title: titulo,
        panel: '',
		callBack: function () {
			parentOBJ.ECM.documentView.getDocument(docId, docVersao);
		},
		customButtons: []
	};

	parentOBJ.ECM.documentView.panel = parentOBJ.WCMC.panel(cfg);
}
function fnLinkArquivos(folderId, atividade){	
	console.log('folderId -> '+ folderId);
	$.ajax({
		url: parent.WCMAPI.serverURL+"/api/public/ecm/document/listDocumentWithChildren/"+parseInt(folderId) ,
		async : false,        
        dataType : 'application/json; charset=utf-8',
		type : "GET",
        dataType : 'JSON',        	
		success : function(data){
            console.log(data);
            console.log(data.content[0].children.length)
			if(data.content[0].children.length > 0 ){
				if($("#galeria").children().length > 0){
            		$("#galeria").children().remove();
            	}
				if($("#documentos").children().length > 0){
            		$("#documentos").children().remove();
				}
				$("#dvShowAnexos").fadeIn();
				$("#anexado").val('sim');
				for(var p = 0; p < data.content[0].children.length; p++){
                    if(data.content[0].children[p]["removed"] == false){
                        console.log(data.content[0].children[p]["fileURL"])
                        console.log('added link')
                        var numero = p + 1;
                        var fileKind = data.content[0].children[p]["fileURL"];
                        console.log(fileKind);
						if(fileKind.indexOf('jpeg') != -1 
							|| fileKind.indexOf('jpg') != -1 
							|| fileKind.indexOf('bmp') != -1 
							|| fileKind.indexOf('gif') != -1 
							|| fileKind.indexOf('img') != -1 
							|| fileKind.indexOf('tif') != -1 
							|| fileKind.indexOf('png') != -1
							|| fileKind.indexOf('PNG') != -1){
                        	if(atividade == 5 || atividade == 'cotacao'){
								$(".showAnexos:last").fadeIn();
								$(".showAnexos:last").children().remove();
								$(".showAnexos:last").append(''+
								'<div class="col-xs-12 col-md-2 col-sm-6">'+ 
                        			'	<a id="'+data.content[0].children[p]["id"]+'"  class="thumbnail" >' +
                                	'		<img src="'+data.content[0].children[p]["fileURL"]+'" alt="..." style=" height:30%">'+
                               		'	</a>' +
                               		'	<i id="trash_'+data.content[0].children[p]["id"]+'" class="fluigicon delete fluigicon-trash icon-md" style="color:red;"></i>'+
                           			'</div> ');		
							}else{

							
                        	$('#galeria').append( ''+
                        			'<div class="col-xs-12 col-md-2 col-sm-6">'+ 
                        			'	<a id="'+data.content[0].children[p]["id"]+'"  class="thumbnail" >' +
                                	'		<img src="'+data.content[0].children[p]["fileURL"]+'" alt="..." style=" height:30%">'+
                               		'	</a>' +
                               		'	<i id="trash_'+data.content[0].children[p]["id"]+'" class="fluigicon delete fluigicon-trash icon-md" style="color:red;"></i>'+
                           			'</div> ');
							$("#divgaleria").fadeIn();
							}
						}
						else if(fileKind.indexOf('doc') != -1 
							|| fileKind.indexOf('docx') != -1 
							|| fileKind.indexOf('pdf') != -1 
							|| fileKind.indexOf('xlsx') != -1 
							|| fileKind.indexOf('odt') != -1 
							|| fileKind.indexOf('ppt') != -1 
							|| fileKind.indexOf('pptx') != -1 
							|| fileKind.indexOf('txt') != -1 
							|| fileKind.indexOf('xls') != -1){  
								if(atividade == 5 || atividade == 'cotacao'){
									$(".showAnexos:last").fadeIn();
									$(".showAnexos:last").children().remove();
									$(".showAnexos:last").append(''+
									 '	<div class="row">'+
									 '		<div class="col-md-1"></div>'+
                        			 '		<div class="col-md-5 col-sm-9 form-group">'+
                        			 '			<label style="margin-left:33px"><i class="fluigicon fluigicon-download icon-sm docs"></i>&nbsp;  <a name="visualizarArquivo" id="'+data.content[0].children[p]["id"]+'" class="docs" target="_blank" >&nbsp;'+ data.content[0].children[p]["description"] +'</a></label></div>'+
                        			 '			<div class="col-md-2 col-sm-3 form-group">'+
                        			 '			<label><a name="deletar" id="delete_'+data.content[0].children[p]["id"]+'" class="delete"> <i  id="trash_'+data.content[0].children[p]["id"]+'" class="fluigicon delete fluigicon-trash icon-md" style="color:red;"></i> </a></label>'+
                        			 '	</div></div><!--row-->');		
								}else{
                        	 $("#documentos").append(
								 	 '	<div class="row">'+
                        			 '		<div class="col-md-5 col-sm-9 form-group">'+
                        			 '			<label style="margin-left:33px"><i class="fluigicon fluigicon-download icon-sm docs"></i>&nbsp;  <a name="visualizarArquivo" id="'+data.content[0].children[p]["id"]+'" class="docs" target="_blank" >&nbsp;'+ numero +' - '+data.content[0].children[p]["description"]+'</a></label></div>'+
                        			 '			<div class="col-md-2 col-sm-3 form-group">'+
                        			 '			<label><a name="deletar" id="delete_'+data.content[0].children[p]["id"]+'" class="delete"> <i  id="trash_'+data.content[0].children[p]["id"]+'" class="fluigicon delete fluigicon-trash icon-md" style="color:red;"></i> </a></label>'+
                        			 '	</div></div><!--row-->');
							 $("#divdocumentos").fadeIn();
							 }
                        }
                        
                       
                    }                   
				}
		
				
				$(".docs").click(function(){
					console.log('abreDocumento() -> id -> '+ $(this).attr('id'));
					abreDocumento($(this).attr("id") )
				});
				$(".delete").click(function(){
					console.log('removerArquivoPasta() -> id -> '+ $(this).attr('id'));
					removerArquivoPasta($(this).attr("id").split('_')[1] );
					setTimeout(function(){
						console.log('setTimeout() -> chamar ->fnLinkArquivos() ');
						fnLinkArquivos($("#pastaAnexos").val(), atividade);
					},700)
					console.log('fim do timeout ')
				});
				$(".delete").hover(function() {
				    $(this).attr("style", "cursor:pointer" );
				  }, function() {
				    $(this).attr("style", "cursor:default" );
				  }
				);
				/*
				$(".delete").mouseover(function(){
				    let id = $(this).attr("id");
				    $("#"+id).attr('class', 'fluigicon delete fluigicon-trash icon-lg');
				});
				$(".delete").mouseout(function(){
				    let id = $(this).attr("id");
				    $("#"+id).attr('class', 'fluigicon delete fluigicon-trash icon-md')
				});
				*/
				$(".thumbnail").click(function(){
					console.log('abreDocumento() -> id -> '+ $(this).attr('id'));					
				    abreDocumento($(this).attr("id") )
				});
				/*
				$(".thumbnail").mouseover(function(){
				    let id = $(this).attr("id");
				    $("#"+id).css('width', '135px');
				});
				$(".thumbnail").mouseout(function(){
				    let id = $(this).attr("id");
				    $("#"+id).css('width', '100px');
				});
				*/
				$(".thumbnail").hover(function() {
					let id = $(this).attr("id");
				    $(this).attr("style", "cursor:pointer" );				    
				    $("#"+id).css('width', '135px');
				  }, function() {
				    $(this).attr("style", "cursor:default" );
				    let id = $(this).attr("id");
				    $("#"+id).css('width', '130px');
				  }
				);
			}else{
				if($(".row", $("#documentos")).length){
					$(".row", $("#documentos")).remove();
				}
				if($("a", $("#galeria")).length){
					$("a", $("#galeria")).parent().remove();
				}
				console.log('zero files')
			}					
		}, error: function(e){
            console.log('error-> ')
            console.log(e.responseText.values)
        }
	});	
}
/**
 * Função para remover arquivo ou pasta de acordo com o id informado.
 * 
 * @param {string} idArquivo Id do arquivo no fluig que deve ser removido.
 * 
 * @returns O retorno do método ajax de remoção de arquivo ou pasta.
 */
function removerArquivoPasta(idArquivo) {
	let dado = JSON.stringify({
		'id': idArquivo
	});
	return ajaxApi('remover', dado);
}
//TODO: passar o id retornado por foldersCotacoes para as funcoes de envio e listagem de arquivos.
/** 23/09/2020 - Cria pastas para anexar cotações 
 * @author{Jean Varlet}
 * @param {int} -> indexer->  posicao da cotação no grid
 * @param {int} -> rootFolder -> id da pasta principal da solicitação
 * @returns {int} -> id da pasta para onde enviar o arquivo.
 */
function foldersCotacoes(indexer, rootFolder){
	var folderCotacoes = ajaxApi("criarPasta", createFolderJson(rootFolder, "Anexos de Solicitações"));
	var folderCotacao = ajaxApi("criarPasta", createFolderJson(folderCotacoes.content.documentId, "Cotação-"+indexer));
	return folderCotacao.content.documentId;
}