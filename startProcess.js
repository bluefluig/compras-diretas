var arrayParticipantes;
function fnRetornaParticipantes(){
	arrayParticipantes = [];
    let counter = $(".participantes").length;
    let colaboradores = $(".participantes");
    let solicitante = $("#matriculaLogado").val();
    let papelSolicitante = $("#papelSolicitante").val();
    let idSolicitacao = $("#idSolicitacao").val();
    let pastaAnexos = $("#pastaAnexos").val();
    let consulta1,colaborador, papelParticipante, relacao, gerenteSolicitante,CODSECAOSIMPLES;
    if(papelSolicitante == 'colaborador'){
        papelSolicitante = 2;
        gerenteSolicitante = $("#gerenteSolicitante").val();
    }else if(papelSolicitante == 'gerente'){
        papelSolicitante = 1;
        gerenteSolicitante = solicitante;
    }

    if($("#solParticipa").val() == 'sim'){
        console.log('Solicitante Participa -> sim')
        arrayParticipantes.push([1, 'v2_Solicitacao_Execucao_Capacitacao', 0, solicitante, solicitante, idSolicitacao, papelSolicitante, papelSolicitante, 3, gerenteSolicitante, pastaAnexos]);
    }
    if(counter){
            console.log('Possui Participantes -> '+ counter)
           for(let i = 0; i < counter; i++){
        	var colaboradorMatRM = toRM(colaboradores[i].id);
			console.log('Matricula participante -> '+ colaboradorMatRM)
            colaborador = DatasetFactory.createConstraint('CHAPA',  colaboradorMatRM,  colaboradorMatRM, ConstraintType.MUST);
            consulta1 = DatasetFactory.getDataset('ds_retornaPessoa', null, new Array(colaborador), null);
            if(consulta1.values.length){
                console.log('Consultando -> '+ colaboradores[i].id)
                gerenteParticipante = consulta1.values[0].GERENTE;
                if(colaboradores[i].id == consulta1.values[0].GERENTE){
                    console.log('Participante é um gerente -> '+consulta1.values[0].NOME);
                    if($("#CODSECAOSIMPLES").val() == consulta1.values[0].CODSECAOSIMPLES){
                       papelParticipante = 1; 
                       relacao = 1
                    }else{
                        papelParticipante = 1;
                        relacao = 2
                    }
                   
                }else{
                    if($("#CODSECAOSIMPLES").val() == consulta1.values[0].CODSECAOSIMPLES){
                        papelParticipante = 2;
                        relacao = 1;
                    }else{
                         papelParticipante = 2;
                         relacao = 2;
                    }
                }
            }
            arrayParticipantes.push([1, 'v2_Solicitacao_Execucao_Capacitacao', 0, solicitante, colaboradores[i].id, idSolicitacao, papelSolicitante, papelParticipante, relacao,gerenteParticipante, pastaAnexos]);     
       
        }// end for
        
    } //end if counter
 
    //arrayParticipantes.push([solicitante, colaboradores[i].id, idSolicitacao, papelSolicitante, papelParticipante, relacao])
}

function fnLaunchProcess(){
	let counter = arrayParticipantes.length;
	var ehOferta = $("#isOferta").val();
	var codOferta = $("#idOferta").val();
	/*
	if(isOferta == 'nao' && idOferta == ''){
		idOferta = '0';
	}
	*/
	if(counter){
		for(let s = 0; s < counter; s++){
			console.log('Iniciando Solicitacao -> ')
			console.log('codOferta' + codOferta);
			console.log('ehOferta' + ehOferta);
			startProcess(arrayParticipantes[s][0], arrayParticipantes[s][1],arrayParticipantes[s][2],arrayParticipantes[s][3],arrayParticipantes[s][4], arrayParticipantes[s][5],
						arrayParticipantes[s][6], arrayParticipantes[s][7], arrayParticipantes[s][8], arrayParticipantes[s][9], arrayParticipantes[s][10],
						 $("#isOferta").val(), $("#idOferta").val(), $("#temaCapacitacaoh").val(), $("#objetivoParticipacao").val(), $("#resultadoParticipacao").val());
			
		}		
	}
	
}
function startProcess(_companyId, _processo, _atividadeInicio,  _solicitante, _participante, _idSolicitacao,
		_papelSolicitante, _papelParticipante, _relacao,  _gerenteParticipante, _pastaAnexos, _isOferta, _idOferta,
		_temaCapacitacao, _objetivoParticipacao, _resultadoParticipacao){
    let idProcesso;
    console.log('startProcess()->')
    console.log(_companyId,_processo, _atividadeInicio,  _solicitante, _participante, _idSolicitacao, _papelSolicitante, _papelParticipante, _relacao,  _gerenteParticipante, _pastaAnexos, _isOferta, _idOferta,
    		_temaCapacitacao, _objetivoParticipacao, _resultadoParticipacao);
    let env = `
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.workflow.ecm.technology.totvs.com/">
        <soapenv:Header/>
        <soapenv:Body>
        <ws:startProcess>
            <username>fluigdev</username>
            <password>@#s0admin</password>
            <companyId>${_companyId}</companyId>
            <processId>${_processo}</processId>
            <choosedState>${_atividadeInicio}</choosedState>
            <colleagueIds>
                <item>${_participante}</item>
            </colleagueIds>
            <comments>Solicitação de Capacitação Iniciada</comments>
            <userId>${_participante}</userId>
            <completeTask>true</completeTask>
            <attachments></attachments>
            <cardData>
               
                <item>
                    <item>idSolicitacao</item>
                    <item>${_idSolicitacao}</item>
                </item>
                <item>
                    <item>matriculaParticipante</item>
                    <item>${_participante}</item>
                </item>
                <item>
                    <item>papelParticipante</item>
                    <item>${_papelParticipante}</item>
                </item>
                <item>
                    <item>papelSolicitante</item>
                    <item>${_papelSolicitante}</item>
                </item>
                <item>
                    <item>relacao</item>
                    <item>${_relacao}</item>
                </item>
		    	<item>
			    	<item>pastaAnexos</item>
			    	<item>${_pastaAnexos}</item>
		    	</item>
		    	<item>
			    	<item>gerenteParticipante</item>
			    	<item>${_gerenteParticipante}</item>
		    	</item>
		    	<item>
			    	<item>isOferta</item>
			    	<item>${_isOferta}</item>
		    	</item>
		    	<item>
			    	<item>idOferta</item>
			    	<item>${_idOferta}</item>
		    	</item>
		    	<item>
			    	<item>temaCapacitacao</item>
			    	<item>${_temaCapacitacao}</item>
		    	</item>
		    	<item>
			    	<item>objetivoParticipacao</item>
			    	<item>${_objetivoParticipacao}</item>
		    	</item>
		    	<item>
			    	<item>resultadoParticipacao</item>
			    	<item>${_resultadoParticipacao}</item>
		    	</item>
		    
		    	
		    	
            </cardData>
            <appointment></appointment>
            <managerMode>false</managerMode>
        </ws:startProcess>
        </soapenv:Body>
    </soapenv:Envelope>`

    $.ajax({
        url: "http://fluig.pe.sebrae.com.br:8080/webdesk/ECMWorkflowEngineService?wsdl",
        crossDomain: true,
        processData: false,
        async: false,
        type: 'POST',
        data: env,
        contentType: "text/xml; charset=UTF-8",
        dataType: "xml"
    }).done(function(data, status, req) {
        let doc = $.parseXML(req.responseText);
        idProcesso = $(doc).find("soap\\:Envelope")
            .find("soap\\:Body")
            .find("ns1\\:startProcessResponse")
            .find('result :nth-child(5)')
            .find('item :nth-child(2)').text();
        //console.log(idProcesso);
    });
    //$("#idTempProcesso").val(idProcesso);
    return idProcesso;
}