/**
*   Scripts para persistência de dados dos itens de compra e Rateios
*   Jean Varlet - 08/09/2020 - 
*/

/** @function saveProdutos() - Percorre os itens de compra da solicitação, realiza a 
 * conversão em formato JSON e salva no campo conteudoRateios. 
 * @author {Jean Varlet}
 */
function saveProdutos(){
    let qtdProds = $(".quantidade", $(".item:visible")).length;
    let produto = [];
    
    let _qtdrows = 0;
    for(let y = 0; y < qtdProds; y++){//Itens de compra -
        var _posy = y + 1;
        let arrRateios = [];
		 _qtdrows = $("tr", $("#bodyRateiostbl___"+_posy)).length; // rows de rateios no item de compra 
		//var objRateios, objProduto;
        console.log('Exec -> y -> _posy-> '+ _posy)
		for(let x = 0; x < _qtdrows; x++){
            var _posx = x + 1;
			var objRateios = {
				"produto": _posy,
                "ordenacao": _posx,
				"unidade": $(".unidadeTbl", $("#trRateiotbl___"+_posy+"___"+_posx)).text(),
				"projeto": $(".projetoTbl", $("#trRateiotbl___"+_posy+"___"+_posx)).text(),
				"acao": $(".acaoTbl", $("#trRateiotbl___"+_posy+"___"+_posx)).text(),
				"codUnidade": $(".codUnidadeTbl", $("#trRateiotbl___"+_posy+"___"+_posx)).text(),
				"percentual": $(".percentualTbl", $("#trRateiotbl___"+_posy+"___"+_posx)).text(),
				"valor": $(".valItemTblRat", $("#trRateiotbl___"+_posy+"___"+_posx)).text()
				
			}
            arrRateios.push(objRateios);
            console.log(arrRateios);	
            console.log('Exec -> x -> _posx-> '+ _posx)		
		}
			
        var _produto = $("#produtoRm___"+ _posy).val();
        var _idProd = $("#idProduto___"+ _posy).val();
        var _codProd = $("#codProduto___"+ _posy).val();
        var _tituloCad = $("#tituloProd___"+ _posy).val();
        var _espec = $("#especificacao___"+ _posy).val();
        var _valAprox = $("#valCustoAproxItem___"+ _posy).val();
        var _qtdItem = $("#quantidade___"+ _posy).val();
        var _cadastrarProd = 'nao';
        var _naturezaProd = '';

        if($("#naturezaProdSel___"+_posy).val() != ''){
            _naturezaProd = $("#naturezaProdSel___"+_posy).val(); 
        }else{
            _naturezaProd = $("#naturezaProd___"+_posy).val(); 
        }
        var _naturezaAno = $("#naturezaAno___"+_posy).val();
        var _naturezaCod = $("#naturezaCod___"+_posy).val();
        var _naturezaSald = $("#naturezaSald___"+_posy).val();
         
        var _tituloCadastrar = '';
        if($("#nomeCadastrar___"+_posy).is(":visible") && $("#nomeCadastrar___"+_posy).val() != ''){
            _tituloCadastrar =$("#nomeCadastrar___"+_posy).val();
            _cadastrarProd = 'sim';
        }
        var  objProduto = {
            "ordem": _posy,
            "produto": _produto,
            "cadastrar": _cadastrarProd,
            "tituloCadastrar": _tituloCadastrar,
            "idProduto": _idProd,
            "titulo": _tituloCad,
            "produto": _produto,
            "especificacao": _espec,
            "valorAproximado": _valAprox,
            "quantidade": _qtdItem,
            "natureza": _naturezaProd,
            "anoNatureza": _naturezaAno,
            "codNatureza": _naturezaCod,
            "saldoNatureza": _naturezaSald,
			"rateios": arrRateios
          
        }
        if($("#obsItem___"+_posy).val() == ''){
            objProduto.obs = 'none';
        }else{
            objProduto.obs = $("#obsItem___"+_posy).val();
        }
		console.log(objProduto)
        produto.push(objProduto)    
    }
 
    $("#conteudoRateios").val(JSON.stringify(produto));
	if($("#conteudoRateios").val() != ''){
		return true;	
	}else{
		return false;
	}
	
}

/** @function saveCotacoes() - Percorre os itens de cotações na etapa 5  da solicitação, realiza a 
 * conversão em formato JSON e salva no campo conteudoCotacoes. 
 * @author {Jean Varlet}
 */
function saveCotacoes(){
    let arrFornecedores = [];
    // Checa se existe entradas de cotações -> 
    if($(".entradaCotacao").length){
        let qtdcotacoes = $(".entradaCotacao").length;
        
        for(let f = 0; f < qtdcotacoes; f++ ){ // 1 - percorre os fornecedores e dados relativos
            var _posf = f + 1;
            let qtItensCot = $("tr:visible", $("#itensCompCot___"+_posf)).length;
            let arrPropostas = [];
            if($("tr:visible", $("#itensCompCot___"+_posf)).length){
                for( let p = 0; p < qtItensCot; p++){
                    var _posp = p + 1;
                    var _possuiItem;
                    var _motivo;
                    var _qtdItens, _valorUnit, _tituloItem, _qtdDesejada, _idProd;
                    /* 19/11/2020 - Jean Varlet - 
                    Provável erro - checar se a linha abaixo consegue avaliar a seleção de valor correta de 
                    um campo do tipo switch. Caso não, alterar para checar a visibilidade das divs -> #dvDisponivel___1___1 &  #dvInDisponivel___1___1 .

                     */
                    if(!$("#itemDispCot___"+_posf+"___"+_posp).is(":checked")){//Checa se o fornecedor possui o item
                        _possuiItem = 'nao';
                        _motivo = $("#motivoIndCot___"+_posf+"___"+_posp).val();
                        _qtdItens = 0;
                        _valorUnit = 0;
                        _idProd = $("#idprod___"+_posf+"___"+_posp).text();
                        _tituloItem = $("#tituloItem___"+_posf+"___"+_posp).text();
                        _qtdDesejada = $("#qtdItem___"+_posf+"___"+_posp).text();

                    }else{

                        _possuiItem = 'sim'; 
                        _motivo = 'none';  
                        _qtdItens = $("#qtdItemCot___"+_posf+"___"+_posp).val();   
                        _valorUnit = $("#valItemCot___"+_posf+"___"+_posp).val(); 
                        _idProd = $("#idprod___"+_posf+"___"+_posp).text();    
                        _tituloItem = $("#tituloItem___"+_posf+"___"+_posp).text();  
                        _qtdDesejada = $("#qtdItem___"+_posf+"___"+_posp).text();  

                    }

                    var objItensProp = {
                        "disponivel": _possuiItem,
                        "motivo": _motivo,
                        "quantidade": _qtdItens,
                        "preco":_valorUnit,
                        "titulo": _tituloItem,
                        "qtdDesejada": _qtdDesejada,
                        "idProduto": _idProd
                    }
                    arrPropostas.push(objItensProp)
                }    
            }else{
                console.log('Não retornou produtos para essa cotação');
            }
            var  objFornecedor = {
                "ordem": _posf,
                "cnpj" : $("#selCNPJForn___"+_posf).val(),
                "razao" : $("#razaoForn___"+_posf).val(),
                "fantasia" : $("#fantasiaForn___"+_posf).val(),
                "proposta" : 'none',
                "entrega" : $("input", $("#EntPrevisCot___"+_posf)).val(),
                "valorTotal" : $("#valTotCot___"+_posf).val(),
                
                "cotacao": arrPropostas
            }
            if(objFornecedor.cnpj.length == 18){
                objFornecedor.cnd = [{"situacao": $("#cndSit___"+_posf).text().split(':')[1], "validade": $("#cndVal___"+_posf).text().split(':')[1]}];
                objFornecedor.fgts = [{"situacao": $("#fgtsSit___"+_posf).text().split(':')[1], "validade": $("#fgtsVal___"+_posf).text().split(':')[1]}];
            }else{
                objFornecedor.cnd = "isento";
                objFornecedor.fgts = "isento";
            }
            if($("#obscotacoes___"+_posf).val() == ''){
                objFornecedor.obsCotacao = 'none';
            }else{
                objFornecedor.obsCotacao = $("#obscotacoes___"+_posf).val();
            }
            arrFornecedores.push(objFornecedor);
        }
        $("#conteudoCotacao").val(JSON.stringify(arrFornecedores));
        //Checar -> Não está desabilitando os selects de itens indisponíveis ->
        disableEntradasCot();

        return true;
           
    }
    // Quando não possui itens de cotação/ Fornecedores selecionados --
    else{

        toast('Atenção!', 'Você não inseriu nenhuma cotação!', 'warning');
        return false;

    }
}

function disableEntradasCot(){
    $("input", $("#dvCotacoesFilhos")).each(function(){
        let _id = $(this).attr("id");

        $("#"+_id).prop("readonly", true);

    })
    $("textarea", $("#dvCotacoesFilhos")).each(function(){
        let _id = $(this).attr("id");

        $("#"+_id).prop("readonly", true);

    })
    $(".cotSwitchs").each(function(){
        let _id = $(this).attr("id");

        FLUIGC.switcher.disable("#"+_id);

    })
    $(".delCotacao").remove();

}
// Gera último JSON --- 

function parseItensFinal(){
    let fornecedor;
    if($("#dvShowMapaForn").children().length){// Cotação por fornecedor ---
        $(".selFornMapa").each(function(){
            let _selId = $(this).attr("id");
            console.log('_selId->'+_selId)
            if($("#"+_selId).is(":checked") ){
                console.log('Forn sel')
                fornecedor = $("#cnpjForn___"+_selId.split('___')[1]).text();
                console.log('fornecedor->'+fornecedor)
                var _cotacao = JSON.parse($("#conteudoCotacao").val());
                var _codcfo, _dataEntrega;
                var _arrItensCor = [];
                for(let i = 0; i < _cotacao.length; i++){
                    console.log('1 for')
                    if(_cotacao[i].cnpj == fornecedor){
                        _codcfo = retCodCfo(fornecedor);
                        _dataEntrega = _cotacao[i].entrega;
                        //var _arrFornSel = [retCodCfo(fornecedor), _cotacao[i].entrega ]; //Fornecedor---
                        for(let p = 0; p < _cotacao[i].cotacao.length; p++){
                            var _tempObj = {
                                "idProduto":_cotacao[i].cotacao[p].idProduto,
                                "quantidade": _cotacao[i].cotacao[p].quantidade,
                                "valor": _cotacao[i].cotacao[p].preco,
                                "rateios": parseRateiosFinal(_cotacao[i].cotacao[p].idProduto)
                            }
                            _arrItensCor.push(_tempObj) //Produtos ---
                        }        
                    }
                }
                //Fim primeiro for->
                var _objF = {
                    "codcfo": _codcfo,
                    "dataEntrega":  _dataEntrega,
                    "cotacao":_arrItensCor
                } 
                console.log(_objF) ;
                $("#conteudoFinal").val(JSON.stringify(_objF));
                return true;           
            }
        })
    }
    if($("#dvShowMapaItem").children().length){ // Cotação por item de compra ---
        $(".selItemMapa").each(function(){

        });    
    }
}
function retCodCfo(cnpj){
    let cst1 = DatasetFactory.createConstraint('CGCCFO', cnpj, cnpj, ConstraintType.MUST);
    let _ret = DatasetFactory.getDataset('dsClienteRM2', null, [cst1], null);
    if(!jQuery.isEmptyObject(_ret)){
        return _ret.values[0].CODCFO;
    }
}

function parseRateiosFinal(idProduto){
    var _arrRateios = [];
    var _rateios = JSON.parse($("#conteudoRateios").val());
    for(let r = 0; r < _rateios.length; r++){
        if(_rateios[r].idProduto == idProduto){
           // console.log(_rateios[r]);
            for(let cc = 0; cc < _rateios[r].rateios.length; cc++){
                var _temp = [[_rateios[r].rateios[cc].codUnidade, _rateios[r].rateios[cc].percentual]]
                _arrRateios.push(_temp)
                //console.log(`CC -> ${_rateios[r].rateios[cc].codUnidade} -- Percent -> ${_rateios[r].rateios[cc].percentual}`)    
            }    
        }    
    }
    return _arrRateios;
}