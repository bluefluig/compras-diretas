/* Mapa de Compras/Cotações  Jean Varlet 25/09/2020 -  */

/** @function parseCotacoesMapa() -  faz a leitura do JSON que contém os dados das cotações inseridas na etapa 5 do processo.
 *  @author {Jean Varlet}
 *  @param {Number} - atividade -
 */
function parseCotacoesMapa(atividade){
	let _obj;
	if($("#dvShowMapaForn").children().length){
		$("#dvShowMapaForn").children().remove()	
	}
    if($("#conteudoCotacao").val() != ''){
        _obj = JSON.parse($("#conteudoCotacao").val());
        if(_obj.length){
            for(let f = 0; f < _obj.length; f++){
                /* Jean Varlet - 19/11/2020 -  
                    Adicionar checagem se item disponível ou não na cotação do fornecedor.
                    Necessita encontrar o parametro do obj que trata isso.
                */
				var _ordem = _obj[f].ordem;
				
				var _fantasia = _obj[f].fantasia;
				var _cnpj = _obj[f].cnpj;
				var _situacao = 'ok';
				var _necessidade = $('.pegadata').val();
				var _previsao = _obj[f].entrega;
				var _anexo = _obj[f].proposta;
				var _valorTotal = _obj[f].valorTotal;
				var _propostas = _obj[f].cotacao;
                showMapaCotFornecedores(_ordem,
                    
					_fantasia, 
					_cnpj,
					_situacao,
					_necessidade, 
					_previsao,
					_anexo,
					_valorTotal,
					_propostas, 
					atividade
					)
            }
        }else{
            console.log('Erro! Não há itens de cotação no JSON');
        }

    }else{
        console.log('Erro ao ler os dados do JSON de cotações');
    }
}
/** @function showMapaCotFornecedores() - Exibe mapa de cotações por Fornecedores
 *  @author{Jean Varlet}
 *  @param {int} - ordem
 *  @param {string} - fantasia
 */
function showMapaCotFornecedores(ordem, fantasia, cnpj, situacao, necessidade, previsao, anexo, valorTotal, produtos, atividade){
    $("#dvShowMapaForn").fadeIn();
    $("#dvShowMapaItem").hide();
    let _statusCerts = '';
    if(cnpj.length == 18){
        _statusCerts = '<h4>'+statusCertidao('cnd', cnpj)+' CND  '+statusCertidao('fgts', cnpj)+' FGTS </h4>'
    }else{
        _statusCerts = '<h4 ><i class="fluigicon fluigicon-check-circle-on" style="color:green"></i>  Isento</h4>' 
    }

    $("#dvShowMapaForn").append(
        '<div class="row" style="border: 1px solid gray; border-radius: 7px; margin-top:15px; padding-bottom:7px;" >'+
        '   <div class="col-md-6 col-sm-12">'+
        '       <h3>Fornecedor</h3>'+
        '       <h4>'+fantasia+'</h4>'+
        '   </div>'+
        //'</div><!--row-->'+
		//'<div class="row">'+
		'	<div class="col-md-1"></div>'+
        '   <div class="col-md-3 col-sm-12">'+
        '       <h3>CNPJ</h3>'+
        '       <h4 id="cnpjForn___'+ordem+'">'+cnpj+'</h4>'+
        '   </div>'+
        '   <div class="col-md-2 col-sm-12">'+
        '       <h3>Selecionar?</h3>'+
		//'       <h4>'+situacao+'</h4>'+ // Possivelmente mostrar imagem para ok e não ok
		'		<input class="selFornMapa" id="selFornMapa___'+ordem+'"   type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Sim" '+
		'	       data-off-text="Não"  	data-animate="true"  checked="false"  data-size="large"></input> '+
        '   </div>'+
        '</div><!-- row-->'+
        
        // container para exibir loop de itens de compra do fornecedor-->
        '<div class="row" >'+
        '   <table id="tableMapaForn___'+ordem+'" class="table">'+
        '       <thead>'+
        '           <tr>'+
        //'               <td><h4>Selecionar</h4></td>'+
        '               <td><h4 style="color:#2E66B7;">Produto</h4></td>'+
        '               <td><h4 h4 style="color:#2E66B7;>QTD</h4></td>'+
        '               <td><h4 h4 style="color:#2E66B7;>Valor</h4></td>'+
        '           </tr>'+
        '       </thead>'+
        '       <tbody  id="itensForn___'+ordem+'">'+
        '       </tbody  >'+
        '   </table>'+
		'</div> <!--row itensForn-->'+
		'<div class="row"  style="margin-bottom:17px;">'+
		'	<div class="col-md-3"></div>'+
		'	<div class="col-md-9" style="border: 1px solid gray; border-radius: 7px; ">'+
		'		<div class="row">'+
		'			<div class="col-md-3">'+
        '				<h3>Certidões</h3>'+
                         _statusCerts +
		//'				<h4>'+statusCertidao('cnd', cnpj)+' CND  '+statusCertidao('fgts', cnpj)+' FGTS </h4>'+
		'			</div>'+
        '   		<div class="col-md-3 col-sm-12">'+
        '       		<h3>Necessidade</h3>'+
        '       		<h4>'+necessidade+'</h4>'+
        '   		</div>'+
        '   		<div class="col-md-3 col-sm-12">'+
        '       		<h3>Previsão</h3>'+
                		avaliaDataEntrega(necessidade, previsao, 'fornecedor')+
        '   		</div>'+
        '   		<div class="col-md-3 col-sm-12">'+
        '       		<h3>Valor Total</h3>'+
        '       		<h4 id="valorTotalForn___'+ordem+'"> '+valorTotal+'</h4>'+
        '   		</div>'+
        '   	</div><!--row-->'+
        '   </div><!--col-md-9-->'+
        '</div><!-- row-->'
		);
        if(produtos.length){
			var _totalizador = 0;
            for(let p = 0; p < produtos.length; p++){
                var _posicao = p +1;
                let precoProduto;
                if(produtos[p].disponivel ==="nao"){
                    precoProduto = 'R$ 0,00';
                }else{
                    precoProduto = produtos[p].preco;
                }
                $("#itensForn___"+ordem).append(
                    '<tr>'+
                    //'<td>'+
                    //'<input class="selFornMapa" id="selFornMapa___'+ordem+'___'+_posicao+'"   type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Sim" '+
					//'	         	data-off-text="Não"  	data-animate="true"  checked="false"  data-size="large"></input> '+
                    //'</td>'+
                    '<td>'+
                    '   <h4>'+produtos[p].titulo+'</h4>'+
                    '</td>'+
                    '<td>'+
                    '   <h4>'+produtos[p].quantidade+' de ' +produtos[p].qtdDesejada+' </h4>'+
                    '</td>'+
                    '<td>'+
                    '   <h4>'+precoProduto+'</h4>'+
                    '</td>'+
                    '</tr>'
				)
				var _calc = floatReais(precoProduto) * produtos[p].quantidade;
				_totalizador = _totalizador + _calc;
            }
		}
		$("h3", $("#dvShowMapaForn")).css("color","#2E66B7");
		loadSwitchs('selFornMapa');
		
		$("#valorTotalForn___"+ordem).text(reais(_totalizador))
		setTimeout(function(){
			$("#mapadePrecos").fadeIn();
			if(atividade > 5){
				checkSwitchForn();
			}
			
		},1000)
}

/** @function avaliaDataEntrega() - Compara data da necessidade com a data da previsão de entrega
 * e exibe o ícone ok ou não ok
 * @author {Jean Varlet}
 * @param {Date} - necessidade
 * @param {Date} - previsao
 * @param {string} - modo -> item/fornecedor
 */
function avaliaDataEntrega(necessidade, previsao, modo){
    let _necess = moment(necessidade, "DD/MM/YYYY");
    let _prev = moment(previsao, "DD/MM/YYYY");
    if(modo == 'fornecedor'){
        if(_necess.isAfter(_prev)){
            return `<h4 style="color:green;"> ${previsao} <i class="fluigicon fluigicon-period-verified icon-sm"></i> </h4>`;
        }
        if(_prev.isAfter(_necess)){
            return `<h4 style="color:red;"> ${previsao} <i class="fluigicon fluigicon-period-remove icon-sm"></i> </h4>`;
        }
        if(_prev.isSame(_necess)){
            return `<h4 style="color:orange;"> ${previsao} <i class="fluigicon fluigicon-period-info-sign icon-sm"></i> </h4>`;
        }
    }
}




/** function _build() - Ordena JSON de cotações pelo valor do item de compra e agrupa a lista
 * por item de compra. Para posterior exibição por itens de compras e preços decrescentes.
 * @author {David}
 * @param {JSON} 
 */
function _build(obj){
    
    //espalha (spread operator "..." ) todos os produtos conhecidos em um array
    const prd = [].concat(...obj.map( el => el.cotacao ));

    //qual propriedade os produtos possuem em comum? o ideal seria um id,
    //objeto Set é usado para remover elementos duplicados
    const prdsFinal = [...new Set( prd.map(e => e.titulo) )];


    var response = [];
    prdsFinal.forEach(p => {
    
        var ct = [];
    
        obj.forEach(emp => {
    
            var o = emp.cotacao.find( el => el.titulo.trim().toLowerCase() == p.trim().toLowerCase());
    
            if(undefined != o){
    
                //clona o objeto e remove a propriedade "cotacao"
                var cl = JSON.parse( JSON.stringify(emp) );
                delete cl.cotacao;
                ct.push({
                    "fornecedor" : cl,
                   
                    "preco":  o.preco
                });
            }
        });

        ct.sort(function(a,b){
            return parsePrice4Number(a.preco) - parsePrice4Number(b.preco);
        });

        response.push({
            "Produto": p,
            "fornecedores": ct
           
        });
    
    });



    
        function parsePrice4Number(price){
        
            var arr = price.split("");
        
            if( arr.includes(",") && arr.includes('.') ){        
                if( arr.indexOf(",") !== -1 && arr.indexOf(".") !== -1){
            
                    var icomma = arr.indexOf(",");
                    var ipoint = arr.indexOf(".");
            
                    var aux = arr[icomma];
                    arr[icomma] = arr[ipoint];
                    arr[ipoint] = aux;
            
                    var currency = arr.join("");
                    return Number(currency.replace(/[^0-9.-]+/g,""));
                }
        
            }else if( arr.includes(",") && !arr.includes(".") ) {
                return Number(arr.join("").replace(",", ".").replace(/[^0-9.-]+/g,""));
            }
            return price;
        }
        
    
    

    return response;
} 

/** @function parseCotByPrice() - Percore os itens do JSON de cotações utilizando _build() para 
 * reconstruir a listagem ordenando por preço e agrupando por item de cotação. Por fim, chama 
 * a function que realiza o append na view -> por Itens 
 * 
 */
function parseCotByPrice(){
    let _obj = JSON.parse($("#conteudoCotacao").val());
    let _byPrice = _build(_obj);
    let _produto, _fornecedor,  _ordem; 
    if(_byPrice.length){
        if($("#dvShowMapaItem").children().length){
            $("#dvShowMapaItem").children().remove();
        }
        for(let i = 0; i < _byPrice.length; i++){
			_produto = _byPrice[i].Produto;
		
            _ordem = i + 1;
            showCotsByPrice(_ordem, _produto, _byPrice[i].fornecedores )
        
        }    
    }else{
        toast('Erro!', 'Houve um erro ao tentar ordenar os dados para essa exibição.', 'danger');
    }

}
/** @function showCotsByPrice() - Recebe a chamada de parseCotByPrice() e appenda o
 * conteúdo ordenado por preço e agrupado por item de compra na view
 * @author {Jean Varlet}
 * @param {int} - ordem
 * @param {string} - produto
 * @param {array} - fornecedor
 */
function showCotsByPrice(ordem, produto, fornecedor){
    $("#dvShowMapaItem").append(

        '<div class="row">'+
    
        '        <div class="row" style="border: 1px solid gray; border-radius: 4px; ">'+
        '            <div class="col-md-12 col-sm-12">'+
        '                <h3 style="padding: 7px;">'+produto+'</h3>'+
        '            </div><!--col-md-12-->'+
        '        </div><!--row-->'+
        '        <div class="row">'+
        '            <div class="col-md-12 col-sm-12">'+
        '                <table id="tbl___'+ordem+'" class="table">'+
        '                    <thead>'+
        '                        <tr class="info">'+
        '                             <td><h4>Fornecedor</h4></td>'+
        '                             <td style="width:20%; text-align:center;"><h4>Certidões</h4></td>'+
        '                             <td><h4>Previsão</h4></td>'+
        '                             <td><h4>Preço</h4></td>'+
        '                             <td><h4>Selecionar</h4></td>'+
        '                        </tr>'+
        '                    </thead>'+
        '                    <tbody id="tbodyPreco___'+ordem+'">'+
        '                    </tbody>'+
        '                </table>'+
        '            </div><!--col-md-12-->'+
        '        </div><!--row-->'+
      
        '</div><!--row-->'
    )

    if(fornecedor.length){
        for(let f = 0; f < fornecedor.length; f++){
            var _preco = fornecedor[f].preco;
            var _fornecedor = fornecedor[f].fornecedor.fantasia;
            var _cnpj = fornecedor[f].fornecedor.cnpj;
			var _entrega = fornecedor[f].fornecedor.entrega;
            var _posx = f + 1;
            
            let _statusCerts = '';
            if(_cnpj.length == 18){
                _statusCerts = '			<div class="col-md-6 col-sm-6">'+
				'				<h4> '+statusCertidao('cnd', _cnpj)+' CND</h4>'+
				'			</div>'+
				'			<div class="col-md-6 col-sm-6">'+
				'				<h4> '+statusCertidao('fgts', _cnpj)+' FGTS</h4>'+
				'			</div>'
            }else{
                _statusCerts = '<div class="col-md-12 col-sm-12" style="text-align:center;">'+
				'				    <h4 > <i class="fluigicon fluigicon-check-circle-on" style="color:green"></i> Isento</h4>'+
				'			    </div>'
            }
            $("#tbodyPreco___"+ordem).append(
				'<tr>'+
				
                '    <td>'+
                '    <h4>'+_fornecedor+'</h4>'+
				'    </td>'+

				'	 <td>'+
                '		<div class="row">'+
                        _statusCerts+
				//'			<div class="col-md-6 col-sm-6">'+
				//'				<h4> '+statusCertidao('cnd', _cnpj)+' CND</h4>'+
				//'			</div>'+
				//'			<div class="col-md-6 col-sm-6">'+
				//'				<h4> '+statusCertidao('fgts', _cnpj)+' FGTS</h4>'+
				//'			</div>'+
				'		</div><!--row-->'+
				'		<input type="hidden" id="fornCnpj___'+ordem+'___'+_posx+'" value="'+_cnpj+'">'+
				'		<input type="hidden" id="codProd___'+ordem+'___'+_posx+'" value="'+findIdProd(produto)+'">'+
				'	 </td>'+

                '    <td>'+
                '    <h4>'+avaliaDataEntrega($(".pegadata").val(), _entrega, 'fornecedor')+'</h4>'+
				'    </td>'+
				
                '    <td>'+
                '    <h4>'+_preco+'</h4>'+
				'    </td>'+
				
                '    <td>'+
                '	<input class="selItemMapa" id="selItemMapa___'+ordem+'___'+_posx+'" name="selItemMapa'+ordem+'"   type="radio" data-on-color="success" data-off-color="danger" data-on-text="Sim" '+
				'	         	data-off-text="Não"  	data-animate="true"  checked="false"  data-size="large"></input> '+
                '    </td>'+
                '</tr>'
            )

		   //loadSwitchs('selItemMapa');
		//FLUIGC.switcher.init($("#selItemMapa___"+ordem+"___"+_posx));
	
        }
        
	}
	//dvGerarMovimentos
	 $('.selItemMapa').prop('checked', false);
	 setTimeout(function(){
		$("tbody", $("#dvShowMapaItem")).each(function(){
			let _posicao = $(this).attr("id");
			$(".selItemMapa", $("#"+_posicao)).each(function(){
				let _id = $(this).attr("id");
				 FLUIGC.switcher.init($("#"+_id));
				 FLUIGC.switcher.onChange("#"+_id, function(event, state) {
					if (state === true) {
						$("#dvGerarMovimentos").fadeIn();
						
					} else if (state !== true) {
						$("#dvGerarMovimentos").fadeOut();
						
					}
				});            
			})
		
		});
	 },100)
	
 
		
}
/** @function statusCertidao() - Avalia retorno da certidão do fornecedor e exibe o ícone correto
 * 	@author {Jean Varlet}
 *  @param {string} - certidao fgts/cnd
 * 	@param {string} - fornecedor - cnpj
 */
function statusCertidao(certidao, fornecedor){
    let _parsed = JSON.parse($("#conteudoCotacao").val());
    if(_parsed.length){
   
        for(var i = 0; i < _parsed.length; i++){
            if(_parsed[i].cnpj.toString().trim() == fornecedor.trim()){
                if(certidao == 'fgts'){
                    if(_parsed[i].fgts[0].situacao.trim() != 'Regular'){
                        return '<i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i>';    
                    }
                    else{
                        return '<i class="fluigicon fluigicon-check-circle-on" style="color:green"></i>';    
                    }            
                }else if(certidao == 'cnd'){
                     if(_parsed[i].cnd[0].situacao.trim() != 'SucessoComRessalvas' && _parsed[i].cnd[0].situacao.trim() !='NADA CONSTA'){
                        return '<i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i>';    
                    }
                    else{
                        return '<i class="fluigicon fluigicon-check-circle-on" style="color:green"></i>';    
                    }
                }        
            }    
        }

    }

}

/** @function findIdProd() - Pesquisa e retorna o idProduto no JSON de rateios
 * 	@author {Jean Varlet}
 * 	@param {string} - produto
 */
function findIdProd(produto){
    _parsed = JSON.parse($("#conteudoRateios").val());
    
    if(_parsed.length){
        for(var i = 0; i < _parsed.length; i++){
            if(_parsed[i].produto[0].toString().trim().toLowerCase() == produto.trim().toLowerCase()){
                return _parsed[i].idProduto;
            }    
        }
    }
}