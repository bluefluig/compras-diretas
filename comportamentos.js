/** @function fnDadosInteracao() - Preenche os dados ocultos de interação da etapa
 *  @author {Jean Varlet}
 *  @param {int} -> etapa do processo 
 */
function fnDadosInteracao(etapa, isMobile){
    let matricula = parent.WCMAPI.getUserCode();
	let nome = parent.WCMAPI.getUser();
	if(etapa == 0 || etapa == 4){

	}
    if(etapa == 5 ){// Assistente UABS
        if($("#dataAssUABS").val() != ''){
            $("#dataAssUABS").val(moment().format("DD/MM/YYYY"));
        }else{
            $("#data2AssUABS").val(moment().format("DD/MM/YYYY"));
        }
        $("#matriculaAssUABS").val(matricula);
        $("#nomeAssUABS").val(nome);  
    }
    if(etapa == 12){// Ajuste de Solicitação - usuário solicitante
        $("#dataAjusteSol").val(moment().format("DD/MM/YYYY"));
        $("#matriculaAjusteSol").val(matricula);
        $("#nomeAjusteSol").val(nome); 
    }
    if(etapa == 18){// Gerente Aprova Valor
        $("#dataGerenteAprov").val(moment().format("DD/MM/YYYY"));
        $("#matriculaGerenteAprov").val(matricula);
        $("#nomeGerenteAprov").val(nome);      
    }
    if(etapa == 25){ // Analista UABS --
        $("#dataAnalUABS").val(moment().format("DD/MM/YYYY")); 
        $("#matriculaGerenteAprov").val(matricula);
        $("#nomeAnalUABS").val(nome);       
    }
    if(etapa == 29){// Ajuste UABS
        $("#dataAjusUABS").val(moment().format("DD/MM/YYYY"));
        $("#matriculaAjusUABS").val(matricula);
        $("#nomeAjusUABS").val(nome);     
    }
    if(etapa == 32){// Análise DAF
        $("#dataDecDAF").val(moment().format("DD/MM/YYYY"));
        $("#matriculaDecDAF").val(matricula);
        $("#nomeDecDAF").val(nome);     
    }
    if(etapa == 38){// Envio AF
        $("#dataEnviaAF").val(moment().format("DD/MM/YYYY"));
        $("#matriculaEnviaAF").val(matricula);
        $("#nomeEnviaAF").val(nome);    
	}
	hideEnviarBtn(isMobile);
	showEtapaForm(etapa, modo);
	disableSelections(etapa);
    console.log('fnDadosInteracao(etapa) -> '+ etapa +' Dados -> '+ matricula + ' - Nome->'+ nome)
}

/** @function disableSelections(etapa) -> Desabilita os campos do tipo switch nas exibições das 
 * cotações em todas as etapas diferentes da 32 DAF
 * @author {Jean Varlet}
 * @parama {int} -> etapa
 */
function disableSelections(etapa){
	if(etapa != 5){
		setTimeout(function(){
			$(".selFornMapa", $("#dvShowMapaForn")).each(function(){
				FLUIGC.switcher.disable("#"+$(this).attr("id"));
			})
		
			$(".selItemMapa", $("#mapadePrecos")).each(function(){
				FLUIGC.switcher.disable("#"+$(this).attr("id"));
			})
		},200)		
	}
	
}

/**
* @function showEtapaForm(atividade, modo) -  Exibe a DIV correta para a etapa atual da solicitação.
* @param {int} - ATIVIDADE_ATUAL
* @param {string} - modo: VIEW, MOD, ADD
 */
function showEtapaForm(atividade, modo){
	if(modo == "MOD" || modo == "ADD"){
		showHeader(isMobile);
		
		hideProcessMenuBar();
		transformMenuBar(modo);
		loadComportamentos(modo, atividade);
		showNaturezas(atividade);
		if(atividade == "0" || atividade == "4"){//Iníco - Solicitação de compras diretas - etapa 1 ---
			
			showDiv('inicio');
			$("p", $("#tituloEtapa")).text('INÍCIO - SOLICITAR COMPRAS');
			cabecalhoSolicitacao(atividade);
			setTimeout(function(){
				hideDiv('itensSolicitacao')	;
			},300)
			
			
		}
		if(atividade == "5"){
			hideDiv('additens', 'dvDataEntrega');
			showDiv('assistenteUABS', 'dvAnalAssUABS', 'itensSolicitacao', 'inicio', 'dadosSolicitante', 'dv1ajusteUABS', 'dvCancelUABS');
			$("p", $("#tituloEtapa")).text('COMPRAS UABS');
			cabecalhoSolicitacao(atividade);
			
			
		}
		if(atividade == "12"){
			hideDiv('dvGerarMovimentos');
			showDiv('inicio','itensSolicitacao', 'dvDataEntrega', 'dadosSolicitante','additens','dvDataEntrega');
			
			$("p", $("#tituloEtapa")).text('AJUSTE DA SOLICITAÇÃO');
			cabecalhoSolicitacao(atividade);
		}
		
		if(atividade == "18"){
			hideDiv('dvGerarMovimentos');
			showDiv('gerenteValor', 'divBtnEnviar');
			$("p", $("#tituloEtapa")).text('GERENTE AVALIA VALOR EXCEDENTE DE COMPRAS DIRETAS');
			cabecalhoSolicitacao(atividade);
			textAprovGerente();
			$("#decisaoGerenteAprov").val('nao');
		}
		if(atividade == "25"){
			showDiv('dvInsereCotacoes','analistaUABS', 'dadosSolicitante', 'dvSwitchCancUABS', 'inicio',  'assistenteUABS',  'dvCancelUABS', 'mapadePrecos' );
			hideDiv('additens', 'dvDataEntrega', 'dvAjustAnUABS', 'dvGerarMovimentos');
			$("p", $("#tituloEtapa")).text('REVISÃO JURÍDICO');
			cabecalhoSolicitacao(atividade);
			
		}
		if(atividade == "29"){
			hideDiv('dvGerarMovimentos');
			$("p", $("#tituloEtapa")).text('AJUSTE UABS');
			cabecalhoSolicitacao(atividade)
		}
		if(atividade == "32"){
			showDiv('dvSwitchCancUABS','analiseDaf','dvSwitchCancUABS','inicio', 'dadosSolicitante', 'assistenteUABS', 'mapadePrecos', 'dvDAFAjuste');
			//Dúvida -> assistenteUABS, dvInsereCotacoes
			hideDiv('dvAjustAnUABS', 'additens', 'dvDataEntrega', 'dvInsereCotacoes', 'dvGerarMovimentos');
			$("p", $("#tituloEtapa")).text('HOMOLOGAÇÃO - DAF');
			cabecalhoSolicitacao(atividade)
		}
		if(atividade == "36"){
			hideDiv('dvGerarMovimentos');
			$("p", $("#tituloEtapa")).text('ANÁLISE DE SOLICITAÇÃO DE COMPRAS DIRETAS - AJU ');
			cabecalhoSolicitacao(atividade)
		}
		if(atividade == "38"){
			$("p", $("#tituloEtapa")).text('ENVIO DE AF DE COMPRAS DIRETAS - ');
			showDiv('inicio','dadosSolicitante','mapadePrecos','enviaAFForn');
			hideDiv('dvDataEntrega','dvInsereCotacoes', 'dvGerarMovimentos');
			cabecalhoSolicitacao(atividade)
		}
		if(atividade == "40"){
			$("p", $("#tituloEtapa")).text('AGUARDANDO ASSINATURAS - AF');
			showDiv('aguardaAssinaturas','inicio','dadosSolicitante','mapadePrecos');
			hideDiv('dvDataEntrega','dvInsereCotacoes', 'dvGerarMovimentos');
			cabecalhoSolicitacao(atividade)
		}
		
		
		
	}else if(modo == "VIEW"){//call viewMode(atividade)
		
	}
}



//TODO: Avaliar reescrita da function --
/** Adiciona comportamentos do formulário - Em uso apenas na etapa inicial e ajuste do form-
* @param {varchar} -> modo de carregamento atual do form
* @param {atividade} -> Atividade atual do processo
* @author {Jean Varlet}
*/
function loadComportamentos(modo, atividade){
	if(modo == "ADD" || modo == "MOD"){
		if(atividade == 0 || atividade == 4){
			retGerDir($("#matriculaSolicitante").val());
		}
		if(atividade == 0 || atividade == 4 || atividade == 12  ){
			$("span", $("#dataEntrega")).click(function(){
				let hoje, data;	
				console.log('Click-> .span dataEntrega')	
				$(".day").click(function(){
					console.log('Click-> .day')
					setTimeout(function(){
						hoje = moment();
						data = moment($("input", $("#dataEntrega")).val(), "DD/MM/YYYY");
						console.log('hoje->'+hoje.format("DD/MM/YYYY"));
						console.log('data->'+data.format("DD/MM/YYYY"));
						//moment().weekdayCalc('09/18/2020','09/22/2020',[1,2,3,4,5]); Jean Varlet - 18/08/2020 -- 
						if(data.isAfter(hoje)){
							if(moment().weekdayCalc(hoje.format("/MM/DD/YYYY"),data.format("/MM/DD/YYYY"),[1,2,3,4,5]) >= 7){
							//if(data.diff(hoje, 'days') >= 7){
								$("#solNoPrazo15").val('sim');
								showAlertPrazo('', 'hide');
								$("#dataEntrega").removeClass('has-error');
								$("#dvjustifica15dias").fadeOut();
								showAddItens('sim');
							}else{
								toast('Atenção!', "A data de entrega informada é menor que 7 dias úteis.", "warning");
								$("#solNoPrazo15").val('nao');
								showAlertPrazo('less15days', 'show');
								$("#dataEntrega").removeClass('has-error');
								$("#dvjustifica15dias").fadeIn();
								
								showAddItens('nao');
							}
						}else if(data.isSame(hoje, 'day')){
							toast('Atenção!', "Não é permitido solicitar para compras para o mesmo dia.", "warning");    
							showAlertPrazo('sameDay', 'show');
							$("#solNoPrazo15").val('nao');
							$("#dataEntrega").addClass('has-error');
							showAddItens('nao');
						}else{
							toast('Erro!', 'A data de entrega não pode ser anterior ao dia atual', 'warning');
							$("input", $("#dataEntrega")).val('');
							$("#dataEntrega").addClass('has-error');
							$("#dvjustifica15dias").fadeOut();
							showAlertPrazo('invalid', 'show');
							$("#solNoPrazo15").val('invalida');
					   }	
						 
					},500)
				});
		});
		checkCargoSol(matricula);
		$("#additens").click(function(){
			$("#orcamentoDisp").fadeIn();
			
			$("#dvAnexarProp").fadeIn();
		});
		loadSwitchs('switchs');
		loadSwitchs('evSwitchs');
		loadReais();
			$("#additens").click(function(){
			    let field = $(".reais:last", $("#itens"));
			    let id = field.attr("id");
			    setTimeout(function(){
			        field.blur(function(){
			            if($(this).val() != ''){
			                if($("#"+id).val().indexOf('R$') != -1){
			                    let val = $("#"+id).val().split('R$')[1];
			                
			                    $("#"+id).val(reais($("#"+id).val().split('R$')[1]));
			                }else{
			                    $(this).val(reais($(this).val()));
			                }    
			                 
			            }else{
			                 $(this).val(reais(0));
			            }
			        });
			        field.keypress(function(e){		
				    if(e.which == 13){	
					        $(this).val(reais($(this).val()));
					        setTimeout(function(){
					            $(this).blur();
					        },150);
				    	}		
				    });
			    },150);
			
			    field.click(function(){
			        if(field.val() != ''){
			            field.val('');
			        }
			    })
			    $("#dvAnexarProp").fadeIn();
			});
		}
		// Fim  - Etapas -> início e ajuste de solicitação

		if(atividade == 5 ){
			//Assistente UABS--	
			$(".item:visible").each(function(){
				let _id = $(this);
			   
				if($(".nomeCadastrar", $(this)).val() != ''){
					console.log('oba')
					$(this).css('background-color', 'yellow')
				}else{
					 $(this).css('background-color', 'white')
				}    
				
			});
			setTimeout(function(){
				$(".delete").remove();
			},2000)
			
			
		}
		if(atividade == 18){
			loadSwitchs('swAprovGerente');
			$("#dvGerarMovimentos").hide();
		}
		// Fim  - Etapas -> Assistente UABS
		if(atividade == 25){
			//Analista UABS
			loadSwitchs('swAnalUABS');	
			$("#addCotacao").prop("disabled", true);

			$("#navPorForn").click(function(){
				$("#dvShowMapaItem").hide();	
				$("#dvShowMapaForn").fadeIn();
				parseCotacoesMapa();
				disableSelections(atividade);
				checkSwitchForn();
			
			})
			$("#navPorItem").click(function(){
				$("#dvShowMapaForn").hide();	
				$("#dvShowMapaItem").fadeIn();
				parseCotByPrice();
				disableSelections(atividade);	
			})
			setTimeout(function(){
				$(".delete").remove();
				$("#dvGerarMovimentos").remove();
			},2000)
			
		}
		if(atividade == 32){
			//Análise DAF
			loadSwitchs('swDAF');
		
			$("#navPorForn").click(function(){
				$("#dvShowMapaItem").children().remove();	
				$("#dvShowMapaForn").fadeIn();
				parseCotacoesMapa();	
				checkSwitchForn();
			})
			$("#navPorItem").click(function(){
				$("#dvShowMapaForn").children().remove();	
				$("#dvShowMapaItem").fadeIn();
				parseCotByPrice();		
			})
			$("#continuidade").val('nao');
			$("#addCotacao").prop("disabled", true);
			setTimeout(function(){
				$(".delete").remove();
				$("h3:first",$("#analiseDaf")).hide();
				
			},2000)
			setTimeout(function(){
				$("#dvGerarMovimentos").remove();
			},3500)
			
		}
		if(atividade == 38){
			$("#gerarMovimentos").click(function(){
				callGerarMovs();
				
				$("#enviarAf").fadeIn();
			})
			$("#navPorForn").click(function(){
				$("#dvShowMapaItem").hide();	
				$("#dvShowMapaForn").fadeIn();
				parseCotacoesMapa();
				disableSelections(atividade);
				checkSwitchForn();
			
			})
			$("#navPorItem").click(function(){
				$("#dvShowMapaForn").hide();	
				$("#dvShowMapaItem").fadeIn();
				parseCotByPrice();
				disableSelections(atividade);	
			})
			setTimeout(function(){
				$(".delete").remove();
				checkSwitchForn();
				$("#dvGerarMovimentos").remove();
			},2000)
			
		}
		if(atividade == 40){
			$("#navPorForn").click(function(){
				$("#dvShowMapaItem").hide();	
				$("#dvShowMapaForn").fadeIn();
				parseCotacoesMapa();
				checkSwitchForn();
				disableSelections(atividade);
			
			})
			$("#navPorItem").click(function(){
				$("#dvShowMapaForn").hide();	
				$("#dvShowMapaItem").fadeIn();
				parseCotByPrice();
				disableSelections(atividade);	
			})
			setTimeout(function(){
				$(".delete").remove();
				checkSwitchForn();
				$("#dvGerarMovimentos").remove();
			},2000)
			
		}
	}
		FLUIGC.calendar('#dataEntrega' , {
        pickDate: true,
        pickTime: false,
        
        useCurrent: false
        
    	});		
	
}

function checkSwitchForn(){
    let _parsed = JSON.parse($("#conteudoFinal").val());
    let _fornecedor = retCnpjFornMov(_parsed.codcfo);
    $("h4[id^='cnpjForn___']").each(function(){
        if($(this).text() == _fornecedor){
            console.log($(this).text())
           var _pos = $(this).attr("id").split('___')[1];
           FLUIGC.switcher.enable("#selFornMapa___"+_pos); 
           FLUIGC.switcher.setTrue("#selFornMapa___"+_pos);
           FLUIGC.switcher.disable("#selFornMapa___"+_pos);   
        }
        
    })

}
function retCnpjFornMov(codcfo){
    let forn = DatasetFactory.createConstraint('CODCFO', codcfo, codcfo, ConstraintType.MUST);
    let _ret = DatasetFactory.getDataset('dsClienteRm2', null, [forn], null);
    //cnpjForn___1
    if(!jQuery.isEmptyObject(_ret)){
        return _ret.values[0].CGCCFO;    
    }
}

/** Carrega comportamento de conversão para valor financeiro para os campos que possuem
*  a class reais através da classe reais();
* @param {string}
* @returns {string}
* @author {Jean Varlet}
*/
function loadReais(){
	$(".reais").each(function(){
    let id = $(this).attr("id");
    
    $(this).click(function(){
        $(this).val('');
    });
    $(this).blur(function(){
        if($(this).val() != ''){
            if($("#"+id).val().indexOf('R$') != -1){
                console.log("id->"+id)
                let val = $("#"+id).val().split('R$')[1];
                
                $("#"+id).val(reais($("#"+id).val().split('R$')[1]));
            }else{
                $(this).val(reais($(this).val()));
            }
            
        }else{
            $(this).val(reais(0));
        }   
    });
	    $(this).keypress(function(e){		
	    if(e.which == 13){	
		        $(this).val(reais($(this).val()));
		        setTimeout(function(){
		            $(this).blur();
		        },150);
	    	}		
	    });
	});
	
}


/** @function  showAlertPrazo() - Auxiliar - recebe o tipo de situação para a data selecionada
 * para entrega das compras.
 * @author {Jean Varlet}
 * @param {string} - tipo -> sameDay/less15Days
 * @param {string} - modo -> show/hide
 */
function showAlertPrazo(tipo, modo){
	if(modo  == 'show'){
		if($("#dvAlertaPrazo").children().length){
			$("#dvAlertaPrazo").children().remove();
			
		}
		if(tipo == 'sameDay'){
			$("#justifica15dias").val('');
			$("#dvjustifica15dias").fadeOut();
			$("#dvAlertaPrazo").fadeIn();
			$("#dvAlertaPrazo").append('<div class="alert alert-warning" role="alert">Não é permitido selecionar a data de entrega para o mesmo dia da solicitação</div>')
		}else if(tipo == 'less15days'){
			$("#dvAlertaPrazo").fadeIn();
			$("#dvAlertaPrazo").append('<div class="alert alert-warning" role="alert">A solicitação não está dentro do prazo mínimo de 7 dias úteis.Preencha a justificativa abaixo para prosseguir.</div>')	
		}else{
			$("#justifica15dias").val('');
			$("#dvjustifica15dias").fadeOut();
			$("#dvAlertaPrazo").fadeIn();
			$("#dvAlertaPrazo").append('<div class="alert alert-warning" role="alert">Não é permitido realizar uma solicitação com data anterior a hoje.</div>')		
		}
	}
	if(modo == 'hide'){
		$("#dvAlertaPrazo").children().remove();
		$("#dvAlertaPrazo").fadeOut();	
		
	}
	
	
}
/** @function showAddItens() - Controla a exibição da área de justificativa e variáveis
 * auxiliares pra solicitações dentro do prazo de 7 dias úteis e fora do prazo
 * @author{Jean Varlet}
 * @param {string} prazo -> sim/nao
 */
function showAddItens(prazo){
    if(prazo == 'sim'){// Dentro do prazo de 7 dias úteis prévios
        $("#itensSolicitacao").fadeIn();
		$("#dvEvento").fadeIn();
		
     
    }else{
        $("#itensSolicitacao").fadeOut();
        $("#dvEvento").fadeOut();
           $("#justifica15dias").click(function(){
            if($(this).val() != ''){
				$("#itensSolicitacao").fadeIn(); 
				   
            }else{
                $(this).keypress(function(){
                    if($(this).val().length > 5){
						$("#itensSolicitacao").fadeIn();
						$("#dvEvento").fadeIn();
						
						   
                    }else{
						console.log('Alterando justificativa');
					}
                })    
            }
        })        
    }
}

function showNaturezas(etapa){
	if(etapa > 4){
		$(".dvSelNatureza").fadeIn();
	}
	$(".naturezaProd").each(function(){
		if($(this).val() == ''){
			let _id = $(this).attr("id");
			$("#"+_id).hide();
		}
	})
	$(".naturezaProdSel").each(function(){
		if($(this).val() == ''){
			let _id = $(this).attr("id");
			$("#"+_id).hide();
		}
	})

}