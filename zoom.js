/** Jean Varlet - Funções referentes ao tratamentos dos campos do tipo zoom e autocomplete */

function setSelectedZoomItem(selectedItem){
	let id = selectedItem.inputId;
	let name = selectedItem.fieldName;
	let cargo;
	if(id.split('___')[0] == 'selCNPJForn'){
		console.log(id.split('___')[0]);
		$("#razaoForn___"+id.split('___')[1]).val(selectedItem.NOME);
		$("#fantasiaForn___"+id.split('___')[1]).val(selectedItem.NOMEFANTASIA);

		setTimeout(function(){
			parseItensCot(id.split('___')[1]);	
		},500)
	}
	if(id.split('___')[0] == 'produtoRm'){
		console.log(id.split('___')[0]);
		$("#idProduto___"+id.split('___')[1]).val(selectedItem.IDPRD);
		$("#codProduto___"+id.split('___')[1]).val(selectedItem.CODIGOPRD);
		$("#tituloProd___"+id.split('___')[1]).val(selectedItem.NOMEFANTASIA);
		$("#especificacao___"+id.split('___')[1]).val(selectedItem.DESCRICAO);
		if(selectedItem.CUSTOMEDIO != 'null' &&
			 selectedItem.CUSTOMEDIO != null && 
			 selectedItem.CUSTOMEDIO !== ''){
			$("#valCustoAproxItem___"+id.split('___')[1]).val(reais(selectedItem.CUSTOMEDIO));	
			console.log('CUSTOMEDIO -> '+ selectedItem.CUSTOMEDIO)	
		}
		if(selectedItem.IDPRD == '559'){
			$("#nomeCadastrar___"+id.split('___')[1]).parent().show();
	
		}else{
			$("#nomeCadastrar___"+id.split('___')[1]).val('');
			$("#nomeCadastrar___"+id.split('___')[1]).parent().hide();	
		}
		console.log('selectedItem.CODNATUREZA -> '+selectedItem.CODNATUREZA);
		if(selectedItem.CODNATUREZA == null || selectedItem.CODNATUREZA == 'null' || selectedItem.CODNATUREZA == ''){// Não Encontrado
			//Se o código de natureza do produto selecionado não existir, exibe o campo para seleção manual
			$("#dv"+id.split('___')[1]).fadeIn();
			$("#naturezaProdSel___"+id.split('___')[1]).fadeOut();
			$("#naturezaProd___"+id.split('___')[1]).fadeIn();
			naturezaField(selectedItem.CODNATUREZA, id.split('___')[1])
		}else{//Encontrado
			$("#dv"+id.split('___')[1]).fadeIn();
			selNaturezaItem(selectedItem.CODNATUREZA, id.split('___')[1])
			$("#naturezaProdSel___"+id.split('___')[1]).fadeIn();
			$("#naturezaProd___"+id.split('___')[1]).fadeOut();
		}
		

		setTimeout(function(){
			parseItensCot(id.split('___')[1]);	
		},500)
	}
	if(id == 'analistaResponsavel'){
		if(selectedItem.chapa == selectedItem.GERENTE){
			cargo = 'gerente';
		}else if(selectedItem.chapa ==  selectedItem.DIRETOR){
			
			cargo = 'diretor';
			
		}else{
			cargo = 'colaborador';
		}
		
		if(selectedItem.CODSECAO == $("#codSecao").val()){
			$("#codSecaoResponsavel").val(selectedItem.CODSECAO);
			$("#dvDataEntrega").fadeIn();
			$("#dataEntrega").fadeIn();
		}else{
			$("#codSecaoResponsavel").val(selectedItem.CODSECAO);
			$("#dvDataEntrega").fadeIn();
			$("#dataEntrega").fadeIn();
		}
		setTimeout(function(){
			retGerDir($("#matriculaSolicitante").val());
		},200)
		
	}
}

function removedZoomItem(removedItem) {
    let id = removedItem.inputId;
    console.log('removedZoomItem()-> id-> '+id);
	if(id == 'analistaResponsavel'){
	$("#dvDataEntrega").fadeOut();		
	}
	
}

