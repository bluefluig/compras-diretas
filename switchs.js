/** Jean Varlet - 27/09/2020 - Arquivos responsável pelo controle e carregamentos dos switchs do form */

/** Chamada para inicialização dos switchs do form
* @param {vachar} --- classe -> classe do item
* @author {Jean Varlet}
*/
function loadSwitchs(classe){
	$("."+classe).each(function(){      
		let id = $(this).attr("id");
		console.log('loadSwitchs()->id-> '+ id);
		FLUIGC.switcher.init($("#"+id));
		$("#"+id).attr("data-on-color", 'success')
	    $("#"+id).attr("offColor", 'danger')
	    $("#"+id).attr("onText", 'Sim')
		$("#"+id).attr("offText", 'Não');
		/*
		if(id == 'custoAproxItem'){
			FLUIGC.switcher.setFalse($("#"+id));
		}
		*/
		if(id == 'estimativaTotal'){
			FLUIGC.switcher.setFalse($("#"+id));
		}
		if(id == 'orcamentodisp'){
			FLUIGC.switcher.setFalse($("#"+id));
		}
		if(id == 'swPropostaBaliz'){
			FLUIGC.switcher.setFalse($("#"+id));
		}
		if(id == 'eventoSW'){
			FLUIGC.switcher.setFalse($("#"+id));
		}
		if(id == 'aprovAnalUABS'){
			FLUIGC.switcher.setFalse($("#"+id));
		}
		if(id == 'cancelAnalUABS'){
			FLUIGC.switcher.setFalse($("#"+id));
		}
		//Jean Varlet - Etapa An;alise DAF
		if(id == 'aprovaDaf'){
			FLUIGC.switcher.setFalse($("#"+id));
			$("#dvDAFSWAjuste").fadeIn();
		}
		if(id == 'ajusteDaf'){
			FLUIGC.switcher.setTrue($("#"+id));
			
		}
		if(id == 'swAprovGerente'){
			FLUIGC.switcher.setFalse($("#"+id));
			
		}
		if(id.split('___')[0] == 'selFornMapa'){
			FLUIGC.switcher.setFalse($("#"+id));
			FLUIGC.switcher.onChange("#"+id, function(event, state) {
				if (state === true) {
				$(".selFornMapa").each(function(){
					let _idx = $(this).attr("id")
					if( _idx != id){
						FLUIGC.switcher.setFalse($("#"+_idx));
						$("#dvGerarMovimentos").fadeIn();
						$("#gerarMovimentos").prop('disable', false);
						
									
					}
				})	
					
				} else if (state !== true) {
					$("#gerarMovimentos").prop('disable', true);
				}
			});	
		}
		/*
		if(id.split('___')[0] == 'custoAproxItem'){
			console.log('Inicialização de switchs -> inside -> custoAproxItem '+id);
			FLUIGC.switcher.onChange("#"+id, function(event, state) {
				if (state === true) {
					let pos = $(this).attr("id").split('___')[1];
					console.log('estadoCustoAprox___1 -> pos -> '+ pos)
					$("#valCustoAproxItem___"+pos).prop("readonly", false);
					$("#estadoCustoAprox___"+pos).val('sim');
					
				} else if (state !== true) {
					let pos = $(this).attr("id").split('___')[1];
					$("#valCustoAproxItem___"+pos).prop("readonly", true);
					$("#valCustoAproxItem___"+pos).prop(reais(0));
					$("#estadoCustoAprox___"+pos).val('nao'); //estadoCustoAprox___1
		
					
				}
			});
		}
		*/
	
    });


	FLUIGC.switcher.onChange("#aprovaDaf", function(event, state) {
	    if (state === true) {
			$("#dvDAFSWAjuste").fadeOut();
			$("#dvDAFAjuste").fadeOut();
			$("#dvDAFCancela").fadeOut();
			$("#decisaoDecDAF").val('aprovado');
			
	    } else if (state !== true) {
	    	$("#dvDAFSWAjuste").fadeIn();
			$("#dvDAFAjuste").fadeIn();
			$("#dvDAFCancela").fadeOut();
			FLUIGC.switcher.setTrue('#ajusteDaf')
	    	$("#decisaoDecDAF").val('ajuste');
	    }
	});
	FLUIGC.switcher.onChange("#swAprovGerente", function(event, state) {
	    if (state === true) {
			
			$("#decisaoGerenteAprov").val('sim');
			
	    } else if (state !== true) {
	    	$("#decisaoGerenteAprov").val('nao');
	    }
	});
	FLUIGC.switcher.onChange("#ajusteDaf", function(event, state) {
	    if (state === true) {
			$("#dvDAFSWAjuste").fadeIn();
			$("#dvDAFAjuste").fadeIn();
			$("#dvDAFCancela").fadeOut();
			//FLUIGC.switcher.setTrue('#ajusteDaf')
	    	$("#decisaoDecDAF").val('ajuste');
			
	    } else if (state !== true) {
			$("#dvDAFSWAjuste").fadeIn();
			$("#dvDAFAjuste").hide();
			$("#dvDAFCancela").fadeIn();
			$("#decisaoDecDAF").val('cancelar');
	    }
	});

	FLUIGC.switcher.onChange("#swPropostaBaliz", function(event, state) {
	    if (state === true) {
			
			$("#dvAnexos").fadeIn();
			$("#anexarProposta").val('sim');
	    	
	    } else if (state !== true) {
	    	$("#dvAnexos").fadeOut();
			$("#anexarProposta").val('nao');
	    	
	    }
	});
	FLUIGC.switcher.onChange("#estimativaTotal", function(event, state) {
	    if (state === true) {
			$("#dvEstCusto").fadeIn();
			$("#informouValPrevisto").val('sim');
			calcEstCustoTotal();//Chamar também ao interagir com rateios---
	    	
	    } else if (state !== true) {
	    	let pos = $(this).attr("id").split('___')[1];
			$("#dvEstCusto").fadeOut();
			$("#informouValPrevisto").val('nao');
			$("#valorEstimativaGer").val('');
	    	
	    }
	});
	FLUIGC.switcher.onChange("#orcamentodisp", function(event, state) {
	    if (state === true) {
			
			$("#dvOrcDisp").fadeIn();
	    	$("#informouOrcPrevisto").val('sim');
	    } else if (state !== true) {
	    	
			$("#dvOrcDisp").fadeOut();
	    	$("#informouOrcPrevisto").val('nao');
	    	$("#valorOrcamentoGer").val('');
	    }
	});
	FLUIGC.switcher.onChange("#aprovUABS", function(event, state) {
	    if (state === true) {
			
			$("#dvInsereCotacoes").fadeIn();
			$("#dv1ajusteUABS").fadeOut();
			$("#dvCancelUABS").fadeOut();
			$("#decisaoAssUABS").val('aprovado');
			$("#divBtnEnviar").fadeOut();
			if($(".trCotacao:visible").length == 0){
				wdkAddChild("cotacoes");	
			}

	    } else if (state !== true) {
	    	
			$("#dvInsereCotacoes").fadeOut();
			$("#dvCancelUABS").fadeIn();
			$("#dv1ajusteUABS").fadeIn();
			$("#decisaoAssUABS").val('cancelar');
			$("#dvAjusteUABS").fadeOut();
			$("#divBtnEnviar").fadeIn();
	    	FLUIGC.switcher.setFalse('#ajusteUABS')
	    	
	    }
	});
	FLUIGC.switcher.onChange("#ajusteUABS", function(event, state) {
	    if (state === true) {
			
			$("#dvAjusteUABS").fadeIn();
			$("#dvCancelUABS").fadeOut();
			$("#decisaoAssUABS").val('ajuste');
			if($(".trCotacao:visible").length == 0){
				wdkAddChild("cotacoes");	
			}

	    } else if (state !== true) {
	    	$("#dvAjusteUABS").fadeOut();
			$("#dvCancelUABS").fadeIn();
			$("#decisaoAssUABS").val('cancelar');
			
	    	
	    }
	});
	FLUIGC.switcher.onChange("#eventoSW", function(event, state) {
	    if (state === true) {
			
			$("#dvSelEvento").fadeIn();
			$("#eventoSWh").val('sim');
		
	    } else if (state !== true) {
	    	$("#dvSelEvento").fadeOut();
			$("#eventoSWh").val('nao');
	    }
	});
	
	FLUIGC.switcher.onChange("#cancelAnalUABS", function(event, state) {
	    if (state === true) {
			$("#dvAjustAnUABS").fadeOut();
			$("#dvCancelAnUABS").fadeIn();
			$("#decisaoAnalUABS").val('cancelar');
		
	    } else if (state !== true) {
	    	$("#dvAjustAnUABS").fadeIn();
			$("#dvCancelAnUABS").fadeOut();
			$("#decisaoAnalUABS").val('ajustar');
	    }
	});
	FLUIGC.switcher.onChange("#aprovAnalUABS", function(event, state) {
	    if (state === true) {
			
			$("#dvSwitchCancUABS").fadeOut();
			$("#dvCancelAnUABS").fadeOut();
			$("#dvAjustAnUABS").fadeOut();
			$("#decisaoAnalUABS").val('sim');
			FLUIGC.switcher.setFalse("#cancelAnalUABS")
		
	    } else if (state !== true) {

	    	$("#dvSwitchCancUABS").fadeIn();
			$("#dvCancelAnUABS").fadeIn();
			$("#dvAjustAnUABS").fadeOut();
			$("#decisaoAnalUABS").val('cancelar');
	    }
	});
	
}

/** Carregamento personalizado dos switchs para a etapa de avaliação da UABS.
 * Considera se o orçamento disponível foi informado ou não.
 *  @author {Jean Varlet}
 *  @param {int} -> etapa - > etapa do processo
 *  @param {string} -> conteudoRateios - > sim/nao
 *  @param {string} -> orcamentodisp -> on/off 
 *  @param {string} -> valorEstimativa -> R$ X,XX
 *  @param {string} -> valorOrcamentoGer -> R$ X,XX
 */
function fnSwitchsUABS(etapa, conteudoRateios, orcamentodisp, valorEstimativa, valorOrcamentoGer  ){
	//Adicionar parse no JSON de conteudoRateios para avaliar se rateios informados
    loadSwitchs('switchsUABS');
	conteudoRateios = 'sim'
    if(conteudoRateios == 'sim'){
        FLUIGC.switcher.setTrue($("#projAcUABS"));        
    }else{
        FLUIGC.switcher.setFalse($("#projAcUABS"));    
    }
    if(orcamentodisp == 'on'){
		
        if(valorEstimativa != '' && valorEstimativa != null){
            if(valorOrcamentoGer != '' && valorOrcamentoGer != null){
                if(floatReais(valorOrcamentoGer) >= floatReais(valorEstimativa)){
                    FLUIGC.switcher.setTrue($("#saldoUABS"));     
                }else if(floatReais(valorOrcamentoGer) < floatReais(valorEstimativa)){
                    FLUIGC.switcher.setFalse($("#saldoUABS"));    
                }
            }    
        }
    }else{// Se saldo não foi informado ---
        
    }
	FLUIGC.switcher.setFalse($('#aprovUABS'));
	FLUIGC.switcher.setFalse($('#ajusteUABS'));
	
    FLUIGC.switcher.onChange("#aprovUABS", function(event, state) {
	
	    if (state === true) {
		    $("#dvAjusteUABS").fadeOut();
            $("#descAjustUABS").val('');
            $("#decisaoAssUABS").val('aprovado'); 	
	    	
	    } else if (state !== true) {
	    	$("#dv1ajusteUABS").fadeIn();
            $("#decisaoAssUABS").val('ajuste');  
	    	
	    }
	});
      
}

function fnSwitchsDAF(classe){
	$("."+classe).each(function(){      
		let id = $(this).attr("id");
		console.log('fnSwitchsDAF()->id-> '+ id);
		FLUIGC.switcher.init($("#"+id));
		$("#"+id).attr("data-on-color", 'success')
	    $("#"+id).attr("offColor", 'danger')
	    $("#"+id).attr("onText", 'Sim')
	    $("#"+id).attr("offText", 'Não');
		if(id == 'custoAproxItem'){
			FLUIGC.switcher.setFalse($("#"+id));
		}

		// Jean Varlet - 28/09/2020 - 
		FLUIGC.switcher.onChange("#"+id, function(event, state) {
			if (state === true) {
				console.log('TRUE -> '+ id);
			
				
			} else if (state !== true) {
				console.log('FALSE -> '+ id);
				
			}
		});
	});
}