var matricula, modo, atividade;
var tenant,rootFolder;
var srvUrl = parent.WCMAPI.serverURL;
function loadScript(){
	var matriculaUser = $("#matriculaLogado").val();
	var tenant,rootFolder;
    var srvUrl = parent.WCMAPI.serverURL;
	if(srvUrl.indexOf('fluighomolog') != -1){
		tenant = 1;
		rootFolder = 10791;
	}else{
		tenant = 'SEBRAEPE';
		rootFolder = 17991; // Checar folderId
	}
	if(isMobile){
		console.log('ATIVIDADE->' + ATIVIDADE_ATUAL);
		 matricula = parent.WCMAPI.getUserCode();
		 modo = MODO_ATUAL;
		 atividade = ATIVIDADE_ATUAL;
		 loadMobile(atividade, matricula, modo,rootFolder);	
	}else{
		$("document").ready(function(){
			console.log('ATIVIDADE->' + ATIVIDADE_ATUAL);
			matricula = parent.WCMAPI.getUserCode();
			modo = MODO_ATUAL;
			atividade = ATIVIDADE_ATUAL;	
			loadDesktop(atividade, matricula, modo, isMobile,rootFolder);
			$("main").css("margin-left", "15px");	
			if(ATIVIDADE_ATUAL != 0 && ATIVIDADE_ATUAL != 4){
				$("#enviarbtn").text('Enviar Solicitação');
			}
		});
	}
}

//TODO: SEM USO - 10/10/2020 - Jean Varlet
/** Valida a existência da adição de itens no início da solicitação
 * @author {Jean Varlet}
 * @param {int} -> etapa do processo
 */
function validate(etapa){
	if(etapa == "0" || etapa == "4"){
		if($(".item:visible").length){//Checa se existem filhos no formulário
			$(".PfSwitchs:visible").each(function(){
			    let pos = $(this).attr("id").split('___')[1];
			    if($(this).is(":checked")){
			        if($("#valCustoAproxItem___"+pos).val() == '' || $("#valCustoAproxItem___"+pos).val() == 'R$ 0,00'){
			            toast('Erro!', 'É necessário informar o valor de custo Aproximado no item '+pos + '. ', 'danger');
			            return false;
			        }
			    }
			    if($("#produtoRm___"+pos).val() == ''){
			        toast('Erro!', 'O item '+pos+ ' não foi informado', 'danger');
			        return false;
			    }
			    if($("#especificacao___"+pos).val() == ''){
			        toast('Erro!', 'O item '+pos+ ' não foi especificado', 'danger');
			        return false;
			    }
			    if($("#quantidade___"+pos).val() == '' || $("#quantidade___"+pos).val() == '0' ){
			        toast('Erro!', 'O item '+pos+ ' possui uma quantidade invalida', 'danger');
			        return false;
			    }
			});
			
		}else{
			toast('Erro!', 'Você precisa adicionar itens de compras nessa solicitação', 'danger');
			return false;
		}
		
	}
	
}


// Jean Varlet -> 09/09/2020 ---- Em teste - > Carregar dados ETAPA UABS -> id-> 5 
/**@function fnDadosSolicitacao() - Resposável por exibir os dados da solicitação nas etapas
 * do processo que sejam posteriores a etapa inicial.
 * @author {Jean Varlet}
 * @param {int} nrSolicitacao 
 * @param {int} etapa 
 */
function fnDadosSolicitacao(nrSolicitacao, etapa ){
    let cst1 = DatasetFactory.createConstraint('nrSolicitacao', nrSolicitacao, nrSolicitacao, ConstraintType.MUST);
    let retorno = DatasetFactory.getDataset('ds_compras_diretas_v1', null, [cst1], null);
    if(!jQuery.isEmptyObject(retorno)){
        console.log('ret ok 1')
        //if(retorno.values.length){
            var ret = retorno.values[0];
            console.log('ret ok 2')
            //1 
            fnAvalSels(etapa, ret.informouOrcPrevisto, ret.valorOrcamentoGer, ret.informouValPrevisto, ret.valorEstimativaGer);
            //2
            fnControlBtnsItens(etapa, ret.anexarProposta);
            //3
            fnSwitchsUABS(etapa, ret.conteudoRateios, ret.orcamentodisp, ret.valorEstimativaGer, ret.valorOrcamentoGer  )
			//fnSwitchsUABS(etapa,conteudoRateios, orcamentodisp, valorEstimativa,valorOrcamentoGer   )
            $("#pNomSol").text(ret.nomeSolicitante);
            if(ret.unidadeSolicitante != '' && ret.unidadeSolicitante != undefined && ret.unidadeSolicitante != null){
                 $("#pNomUnd").text(ret.unidadeSolicitante);        
            }else{
                 $("#pNomUnd").text('Não retornada');
            }
            if(ret.analistaResponsavel != '' && ret.analistaResponsavel != undefined && ret.analistaResponsavel != null){
                $("#dvAnalistaResp").fadeIn();
                $("#pNomResp").text(ret.analistaResponsavel);    
			}
			if(ret.eventoSWh == 'sim'){
				$("#dvVinculoEvnto").fadeIn();
				$("#pNomEvento").text(ret.selEventoZoom);
			}
			if($("#justifica15dias").val() != '' && $("#solNoPrazo15").val() == 'nao'){
				var _justificativa = $("#justifica15dias").val();
				$("#dvAlertSol").fadeIn();
				$("#dvAlertSol").append('<div class="alert alert-warning" role="alert">A solicitação foi realizada fora do prazo de 7 dias úteis.</div>');
				$("#dvAlertSol").append(
					'<div class="row">'+
					'	<div class="col-sm-12 col-md-12">'+
					'		<h4>Justificativa:<h4>'+
					'		<p style="color:#2E66B7;">'+_justificativa+'</p>'+
					'	</div>'+
					'</div><!--row-->'
				);		
			}else if( $("#solNoPrazo15").val() == 'sim'){
				$("#dvAlertSol").fadeIn();
				$("#dvAlertSol").append('<div class="alert alert-success" role="alert">A solicitação foi realizada dentro do prazo de 7 dias úteis.</div>')	
			}
            
             $("#pdtEntrega").text(ret.dataEntrega);
             $("#pDiasEntr").text(fnAvalDataEntrega(ret.dataSolicitacao, ret.dataEntrega));
        //}
    }else{
        toast('Erro!', 'Não foi possível retornar os dados dessa solicitação', 'danger');
    }
	$("p",$("#dvAnalAssUABS")).css("color", "#2E66B7;");
	$("#divBtnEnviar").show()
}
/** Auxiliar para fnDadosSolicitacao --- 

*/
function fnAvalSels(etapa, orcamento, valorOrcamento, estimativa, valorEstimativa){
	console.log('fnAvalSels()->' + etapa + ' - orcamento-> '+ orcamento + ' - valorOrcamento->'+valorOrcamento + ' - estimativa-> '+estimativa + ' - valorEstimativa-> '+valorEstimativa)
    if(orcamento == 'sim'){
        $("#orcDisponivelUABS").text(valorOrcamento);
    }else{
        $("#orcDisponivelUABS").text('Não informado');    
    }
    if(estimativa == 'sim'){
        $("#estCustoUABS").text(valorEstimativa);
    }else{
        $("#estCustoUABS").text('Não informada');    
    }
    
}
/** Bloqueia os btns do pai x filho de itens e das tables de rateios adcionados


*/
function fnControlBtnsItens(etapa, anexado){
    if(etapa != 0 && etapa != 4 && etapa != 11 && etapa != 5){
		
		$("#additens").prop("disabled", true);
        $(".btnAddProjAc").each(function(){
            $(this).prop("disabled", true);
		});
		
    }else if(etapa == 5){//Considera se o projeto e ação já foi adicionado na etapa inicial do processo
        if(anexado == 'sim'){
            $("#dvAnexos").fadeIn();
        }
        if(solNoPrazo15 == 'nao'){// Caso fora do prazo de 15 dias previos, exibe caixa informando situação
            $("#dvAlertaPrazo").fadeIn();
		}		
    }
}


/** Compara a data de entrega desejada e data da realização da solicitação
 * @author {Jean Varlet}
 * @param {Date} -> dataSolicitacao -> data da realizaçao da solicitação
 * @param {Date} -> dataEntrega -> data estipulada para a entrega da compra
 */

function fnAvalDataEntrega(dataSolicitacao, dataEntrega){
    let _hoje = moment();
    let _dtSol = moment(dataSolicitacao, "DD/MM/YYYY");    
    let _dtEnt = moment(dataEntrega, "DD/MM/YYYY");
    let saida;
    if(moment(_hoje).isAfter(dataEntrega)){
        saida = `Solicitação vencida em ${_dtEnt.diff(_hoje, 'days')} dias úteis`;
    }else{
        saida = `Em ${_dtEnt.diff(_hoje, 'days')} dias úteis`;
        
    }
    return saida;
}
/** @function fnListItensCompra - Exibe os itens de compras selecionados na primeira
 * etapa do processo nas etapas seguintes da solicitação.
 * @author {Jean Varlet}
 * @param {*} item 
 * @param {*} desc 
 * @param {*} custoInformado 
 * @param {*} custo 
 * @param {*} quantidade 
 * @param {*} projAcao 
 */
function fnListItensCompra(item, desc, custoInformado, custo, quantidade, projAcao){
	if($("#conteudoRateios").val() != ''){
		if(custoInformado != 'sim'){
			custo = 'Não informado.'
		}
		$("#dvListaItens").append(''+
		'<div id="dvItem___'+indexer+'">'+
		'	<div class="row">'+
		'		<div class="col-md-12 col-sm-12 form-group">'+
		'			<h3>Item de compra:</h3>'+
		'			<p>'+item+'</p>'+
		'		</div>>'+
		'		<div class="col-md-12 col-sm-12 form-group">'+
		'			<h3>Especificação do Item:</h3>'+
		'			<p>'+desc+'</p>'+
		'		</div>>'+
		'		<div class="col-md-8  form-group"></div>'+
		'		<div class="col-md-4 col-sm-12 form-group">'+
		'			<h3>Custo Aproximado:</h3>'+
		'			<p>'+custo+'</p>'+
		'		</div>>'+
		'		<div class="col-md-8  form-group"></div>'+
		'		<div class="col-md-4 col-sm-12 form-group">'+
		'			<h3>Quantidade:</h3>'+
		'			<p>'+quantidade+'</p>'+
		'		</div>>'+
		'	</div>>'+
		'	<div class="row" id="ratItens___'+indexer+'">'+
		'	</div>'+
		'</div>'
		)
	}
	
}

//TODO: Remove this function - unnused
/**
 * apenda todos os rateios na tabela
 * @param {Array} array de rateios
 * @author {David}
 * @returns {void}
 */
function showApportionment(arr) {

	const tableBody = document.querySelectorAll("#bodyRateiostbl");

	arr.forEach(rt => {

		const idTb = rt.ordem;
		const tr = document.createElement("tr");
		//desativa os botões de adição e exclusão de rateio
		let parentTable = tableBody[idTb];

		for(let i=0; i<6; i++) {
			parentTable = parentTable.parentElement;
		}

		parentTable.querySelectorAll(".fluigicon.fluigicon-trash.icon-md.btn.btn-danger").forEach(el => $(el).fadeOut());

		if(rt.rateios) {
			
			for(let rateio in rt.rateios) {
	
				const td = document.createElement("td");
				
				td.appendChild(document.createTextNode(rt.rateios[rateio]));
				tr.appendChild(td);
			}
	
			tableBody[idTb].appendChild(tr);

			$(parentTable).find("#addProjAcao").fadeOut();
		}
	
		//ativa o switch de custo aproximado
		FLUIGC.switcher.init(`#_custoAproxItem___${idTb}`);
		FLUIGC.switcher.destroy(`#custoAproxItem___${idTb}`)
	});
}

/** Modelo para controlar itens onde é necessário ter 2 modos
*	view e insert. 
 @params {int} - etapa 
 @params {varchar} - modo - insert/view
 */
function fnShowEvento(etapa, modo){
	if(etapa == 0 || etapa == 4){
		if(modo == 'insert'){
			
		}else if(modo == 'view'){
			
		}	
	}else{
		
	}	
	
}

function showAnexos(etapa){
	if(etapa > 4){
		if($("#anexado").val() != 'nao'){
			$("#dvShowAnexos").fadeIn();
			setTimeout(function(){
				fnLinkArquivos($("#pastaAnexos").val());
			},1000)	
		}
	}
}


/* Ordenar itens por comparação decrescente
[0, 20, 3, 4].sort(function compare(a, b) {
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
})
*/


// JEAN VARLET - 09/10/2020 ---- Assinaturas de AFS -----

/** @function retornaSolsAssinaturas() - Consulta se existem solicitações de assinaturas para o nrSolicitacao no param
 * @author {Jean Varlet}
 * @param {int} - nrSolicitacao
 * @param {int} - atividade
 */
function retornaSolsAssinaturas(atividade, nrSolicitacao){
    let cst1 = DatasetFactory.createConstraint('idSolComprasDiretas', nrSolicitacao, nrSolicitacao, ConstraintType.MUST);
    let ret = DatasetFactory.getDataset('ds_coleta_assinaturas', null, [cst1], null);
    if(!jQuery.isEmptyObject(ret) && atividade == "40"){
        if(ret.values.length){
            for(let i = 0; i < ret.values.length; i++){
				var _situacao = [];
				
				if(ret.values[i].dataEtapa1 == null || ret.values[i].dataEtapa1 == 'null' || ret.values[i].dataEtapa1 == ''){
					_situacao.push('false');		
				}else{
					_situacao.push('true');	
				}
				if(ret.values[i].dataEtapa2 == null || ret.values[i].dataEtapa2 == 'null' || ret.values[i].dataEtapa2 == ''){
					_situacao.push('false');		
				}else{
					_situacao.push('true');	
				}
				if(ret.values[i].dataEtapa3 == null || ret.values[i].dataEtapa3 == 'null' || ret.values[i].dataEtapa3 == ''){
					_situacao.push('false');		
				}else{
					_situacao.push('true');	
					
				}
				var _codigo = "";
				if(ret.values[i].nrSolicitacao == null || ret.values[i].nrSolicitacao == 'null' || ret.values[i].nrSolicitacao == ''){
					_codigo = '0000';
				}else{
					_codigo = ret.values[i].nrSolicitacao;
				}

				appendStatusAss(_codigo, ret.values[i].dataStartProcess, _situacao);
            }    
        }else{
            console.log('Nenhuma Solicitação de assinatura encontrada')
        }
    }
}
/** @function appendStatusAss() - Adiciona status das assinaturas retornadas por dadosSolAssinaturas()
 * @author {Jean Varlet}
 * @param {int} - codigo  -> codigo da solicitacão de assinatura de  -
 * @param {Date} - data de abertura da solicitação de assinaturas
 * @param {string} - texto da situação atual da solicitação
 */
function appendStatusAss(codigo,dtAbertura, situacao){
	let arrIcons = ['<i class="fluigicon fluigicon-enrollment-verified icon-md success" style="color:green"></i>', 
					'<i class="fluigicon fluigicon-test-refresh icon-md warning" style="color:orange"></i>'];
	let linkSolicitacao ='<a src="#">Acessar </a>';
	let status= '';
	if(findLastState(situacao) == 0){
		status = 'Aguardando Assinatura do Fornecedor';	
	}
	else if(findLastState(situacao) == 2){
		status = 'Aguardando Assinatura da Direção';		
	}
	else if(findLastState(situacao) == 1){
		status = 'Aguardando Assinatura Gerente';		
	}
    $("#dvAssinaturas").append(
        '<div class="row">'+
        '    <div class="col-md-2">'+
        '        <h3>Solicitação</h3>'+
		'        <h4>'+codigo+'</h4>'+
		'    </div>'+
        '    <div class="col-md-3">'+
        '        <h3>Data Abertura</h3>'+
        '        <h4>'+dtAbertura+'</h4>'+
        '    </div>'+
        '    <div class="col-md-5">'+
        '        <h3>Situação Atual</h3>'+
        '        <h4>'+status+'</h4>'+
        '    </div>'+
        '    <div class="col-md-2">'+
        '        <h3>Solicitação</h3>'+
        '        <h4>'+linkSolicitacao+'</h4>'+
        '    </div>'+
		'</div><!--row-->'+
		
		'<div class="row" id="detalhes___'+codigo+'" style="display:none;">'+
		'    <div class="col-md-4 .fs-txt-center" id="etapa1___'+codigo+'">'+
		'		<h3>Assinatura do Gerente</h3>'+
		'    </div>'+
		'    <div class="col-md-4 .fs-txt-center" id="etapa2___'+codigo+'">'+
        '		<h3>Assinatura do Fornecedor</h3>'+ 
		'    </div>'+
		'    <div class="col-md-4 .fs-txt-center" id="etapa3___'+codigo+'">'+
        '		<h3>Assinatura da Direção</h3>'+ 
		'    </div>'+
		'</div><!--row-->'+
		'<div class="row" >'+
		'    <div class="col-md-12 fs-txt-center">'+
        '		<i class="fluigicon fluigicon-chevron-down icon-md " id="expand___'+codigo+'" style="color:blue;"></i>'+
		'    </div>'+
		'</div><!--row-->'
	)
	
	$("#expand___"+codigo).click(function(){
		if($("#detalhes___"+codigo).is(":visible")){
			$(this).removeClass('fluigicon-chevron-up');
			$(this).addClass('fluigicon-chevron-down');
			$("#detalhes___"+codigo).fadeOut();
	
		}else{
			$(this).removeClass('fluigicon-chevron-down');
			$(this).addClass('fluigicon-chevron-up');
			$("#detalhes___"+codigo).fadeIn();
		}
	});

	if(situacao.length){
		for(let i = 0; i < situacao.length; i++){
			var pos = i + 1;
			if(situacao[i] === 'true'){
				$("#etapa"+pos+"___"+codigo).append(arrIcons[0]);
			}else{
				$("#etapa"+pos+"___"+codigo).append(arrIcons[1]);

			}

		}
	}
}
/** @function findLastState() - 
 * 
 */
function findLastState(arrTeste){
    if(arrTeste.length){
        
        for(let i = parseInt(arrTeste.length); i > 0; i--){
            var pos = i - 1;
            if(arrTeste[pos] === 'true'){
                console.log('Achei true em '+ pos )
                return  pos;
                
            }
        }
        
    }
}

/** @function textAprovGerente() - Exibe texto com o valor da cotação mais barata para a etapa 18 do processo
 * @author {Jean Varlet}
 */
function textAprovGerente(){

	$("#txtAprovGer").text(
		`Prezado Gerente, a cotação realizada na solicitação de compras: ${$("#nrSolicitacao").val()} retornou um valor maior que o orçamento máximo definido. 
		Solicitamos a sua aprovação para dar continuidade a solicitação de compras. `
	)
	$("#txtOrcinf").text(`Orçamento Informado: ${$("#valorOrcamentoGer").val()}`);
	$("#txtMenorCot").text(`R$ 0,00`);
}