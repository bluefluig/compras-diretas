/*Arquivo Utils.js --- Jean Varlet 23/03/2020 ---*/
var red, blue, purple, cyan, green, yellow, reset;
red   = '\u001b[31m';
blue  = '\u001b[34m';
purple = '\u001b[35m';
cyan = '\u001b[36m';
green = '\u001b[32m';
yellow = '\u001b[33m';
reset = '\u001b[0m';
/**Esconde o  Botão Enviar 
 * @param mode  isMobile Boolean 
 */
function hideEnviarBtn(mode){
	console.log('hideEnviarBtn(mode)->'+mode);
	if(mode){		
		return null;
	}else{
		if($("button:contains('Enviar')").length ){
			$("button:contains('Enviar')").hide();
			$("a[class='topBtn btn btn-primary']").hide();
			$("button[data-toggle='dropdown']").hide();
		}
		if(parent.$("button:contains('Enviar')").length){
			parent.$("button:contains('Enviar')").hide();
			parent.$("a[class='topBtn btn btn-primary']").hide();
			parent.$("button[data-toggle='dropdown']").hide();
		}		
		return true;
	}
}
/** @function enviarAction(atividade) -> Atualização 11/10/2020 - Adicionado parametro "atividade"
 * para chamada de validateAll().
 * @author {Jean Varlet}
 * @param {int} -> atividade
 */
function enviarAction(atividade){
	$("#enviarbtn").prop("disabled", true); // Desabilita button para evitar chamada dupla
	if(validateAll(atividade)){
		parent.document.getElementById("workflowActions").querySelector("button").click();	
	}else{
		$("#enviarbtn").prop("disabled", false);
	}
}


function transformMenuBar(mode){
    let distanceCalc = $(".fluig-style-guide").offset().top  * -1;
   // let retorno = 'margin-top:'+distanceCalc+'px';
    let retorno = 'margin-top:-203px';
    console.log('distancia ->'+ retorno)
    if(isMobile){
        return null;
    }else{
    	//parent.$("#workflowview-header").clone().appendTo("#showProcessArea");
       //$("#showProcessArea").append(parent.$("#workflowview-header").clone());// appenda barra original em uma div --
    	//parent.$("#workflowview-header").hide();
        $("form").attr("style", retorno); 
        
       // parent.$("#textActivity").hide(); // Texto com o nome do processo --
    }
}
function hideProcessMenuBar(){
	parent.$("#workflowview-header").fadeOut();
	parent.$("#breadcrumb").parent().fadeOut()
}
function showProcessMenuBar(){
	parent.$("#workflowview-header").fadeIn();
	parent.$("#breadcrumb").parent().fadeIn();
	
}
/**
 * @function floatReais Função para verificar se a string é nula | vazia | indefinida
 * 
 * @param {string} value string com 'R$' ser convertida. 
 * Exemplo:  R$ 200,00 -> 200.00
 * 
 * @return {number} number.
 */
function floatReais(value){
	//if(value.split('NaN').length == 1){
		return localeParseFloat(value.split("R$")[1].trim(), ",", ".");	
	//}
    
}

function reais(value) {
	if(value == ''){
		value = 0;
	}
	/*
	if(value.indexOf(',') != -1){
		value = value.replaceAll(',', '.');
	  }
	  */
    return parseFloat(value).toLocaleString('pt-BR', {
        minimumFractionDigits: 2,
        style: 'currency',
        currency: 'BRL'
    });
}

/**
 * @function localeParseFloat Função para substituir ',' por '.'
 * 
 * @param {string} value string a ser modificada.
 * 
 * @return {number} number.
 */
function localeParseFloat(str) {
    let out = [];

    str.split(",").map(function(x) {
        x = x.replace(".", "");
        out.push(x);
    });

    return parseFloat(out.join("."));
}
function fnTestaTipoValor(valor){
    let temp1, temp2;
    if(valor.split(',').length > 1){
        if(valor.split('R$').length > 1){
            temp1 = valor.split('R$')[1];
            temp2 = temp1.split(',')[0];
            if(parseInt(temp2) > 0){
                console.log(parseInt(temp2))
                return true
            }else{
                return false;
            }
        }else{
        	return false;
        }       
    }else{
    	return false;
    }
    
}

function invertData(data){
	 return data.split('/').reverse().join('-');
}

function fnCustomDelete(oElement){
    fnWdkRemoveChild(oElement);
   
};
function fnCustomAdd(field){
	wdkAddChild(field);
	if(field == 'itens'){
        $("#divBtnEnviar").fadeIn();
		let posicao = $('.ordemProduto:last').attr("id").split('___')[1];
		$(".ordemProduto:last").val(posicao);
		
		FLUIGC.switcher.init($("#custoAproxItem___"+posicao));
		$("#custoAproxItem___"+posicao).attr("data-on-color", 'success')
	    $("#custoAproxItem___"+posicao).attr("offColor", 'danger')
	    $("#custoAproxItem___"+posicao).attr("onText", 'Sim')
	    $("#custoAproxItem___"+posicao).attr("offText", 'Não');
		
		FLUIGC.switcher.setFalse($("#custoAproxItem___"+posicao));
		FLUIGC.switcher.onChange("#custoAproxItem___"+posicao, function(event, state) {
		    if (state === true) {
				let pos = $(this).attr("id").split('___')[1];
				$("#valCustoAproxItem___"+posicao).prop("readonly", false);
				$("#estadoCustoAprox___"+pos).val('sim');
				$(".valCustoAproxItem:visible").click(function(){
				    console.log('evento valor')
				    $(this).blur(function(){
				        console.log('evento blur')
				        if($(this).val() != '' && floatReais($(this).val()) > 0 ){
				            let pos =  $(this).attr("id").split('valCustoAproxItem___')[1];
				            
				            let linha = floatReais($("#valCustoAproxItem___"+pos).val()) * parseFloat($("#quantidade___"+pos).val());
				            if(linha > 0){
				                
				                $("#valorNecessario___"+pos[0]).val(reais(linha));
				            }else{
				                console.log('Valor inválido');
				            }
				        }
				    })
				});
				$(".quantidade:visible").click(function(){
				      
				    if($(this).val() != '' && $(this).val() > 0 ){
				        $(this).removeClass('has-error'); 
				        let pos =  $(this).attr("id").split('quantidade___')[1];
				
				        let linha = floatReais($("#valCustoAproxItem___"+pos).val()) * parseFloat($("#quantidade___"+pos).val());
				        if(linha > 0){
				
				            $("#valorNecessario___"+pos[0]).val(reais(linha));
				        }else{
				            $(this).addClass('has-error');
				            toast('Atenção!', 'Cheque a quantidade mínima para o item '+pos, 'warning');
				        }
				    }
				   
				});
				$(".quantidade:visible").blur(function(){
				      
				    if($(this).val() != '' && $(this).val() > 0 ){
				        $(this).removeClass('has-error'); 
				        let pos =  $(this).attr("id").split('quantidade___')[1];
				
				        let linha = floatReais($("#valCustoAproxItem___"+pos).val()) * parseFloat($("#quantidade___"+pos).val());
				        if(linha > 0){
				
				            $("#valorNecessario___"+pos[0]).val(reais(linha));
				        }else{
				            $(this).addClass('has-error');
				            toast('Atenção!', 'Cheque a quantidade mínima para o item '+pos, 'warning');
				        }
				    }
				   
				});
		    	
		    } else if (state !== true) {
		    	let pos = $(this).attr("id").split('___')[1];
				$("#valCustoAproxItem___"+posicao).prop("readonly", true);
				$("#valCustoAproxItem___"+posicao).val(reais(0));
				$("#valorNecessario___"+posicao).val(reais(0));
				$("#estadoCustoAprox___"+pos).val('nao');
		    	
		    }
		});
		
	}
	if(field == 'cotacoes'){
		$("#dvBtnChkCert").fadeIn();
	}
}

/**
 * @function toast Função genérica para gerar toast.
 * @param {String} titulo Titulo do toast.
 * @param {String} msg Mensagem para informação do toast.
 * @param {String} tipo Tipos: 'success', 'warning', 'info' e 'danger'.
 * @returns {component} Retorna o toast sem timeout.
 */
function toast(titulo, msg, tipo) {
    if($(".alert-"+tipo, $(".toaster")).length){
        $(".alert-"+tipo).remove();
    }
    FLUIGC.toast({ title: titulo, message: msg, type: tipo });
 }

function isEmpty(value) {
    if (value == null || value === '' || typeof value === 'undefined')
        return true;

    return false;
}
/** @function desabilitaZoom(...campos) - Desabilita campos do tipo Zoom
 * @author {Jean Varlet}
 * @param {string} - Id dos campos
 */
function desabilitaZoom(...campos) {
	campos.forEach(campo => {
		window[`${campo}`].clear();
		window[`${campo}`].disable(true);
	});
}
/** @function desabilitaSwitch(...campos) - Desabilita campos do tipo Switch
 * @author {Jean Varlet}
 * @param {string} - Id dos campos
 */
function desabilitaSwitch(...campos){
	campos.forEach(campo=>{
		FLUIGC.switcher.disable('#'+campo);
	})
}

/**
Adequa formato matrícula RM 
@param {string} matrícula Fluig
@returns {string} matrícla RM

 */
function toRM(matriculaFluig){
    let size = matriculaFluig.length;
    let lChars = size - 4;
    let mat = matriculaFluig.substr(lChars, 4);

    if(mat.length == 4){
        if(mat.substr(0,1) != "5"){
            return '0000'+mat;
        }else{
            console.log('Mensagem para estagiário');
        }
    }
}
/**
	Adequa formato matricula retornada pelo RM 
	@param {string} matricula RM 0000XXXX
	@returns {string} matricula Fluig XXXX
 */
function toFluig(matriculaRM){
    let size, lChars, mat;
    if(matriculaRM.length){
        size = matriculaRM.length;
        lChars = size - 4;
        mat = matriculaRM.substr(lChars, 4);
        return mat;
    }else{
        console.log('Matrícula vazia!')
    }
} 
//Em testes -> 
function toFluig2(matriculaRM){
    let size, lChars, mat;
    if(matriculaRM.length){
        size = matriculaRM.length;
        lChars = size - 4;
        mat = matriculaRM.substr(lChars, 4);
        let cst1 = DatasetFactory.createConstraint('login', 'PE00'+mat, 'PE00'+mat, ConstraintType.MUST);
        let consulta = DatasetFactory.getDataset('colleague', null, [cst1], null);
        if(!jQuery.isEmptyObject(consulta)){
            
            return consulta.values[0]['colleaguePK.colleagueId'];
        }else{
            console.log('Não encontrado')
        }
    }else{
        console.log('Matrícula vazia!')
    }
} 
/** 19/08/2020 - Loading modificado 
* @author {Jean Varlet}
* @param {int} etapa -> if empty will load on body
* @param {varchar} acao -> show or hide
* @param {varchar} msg -> free text - optional
*/ 
function showLoading(etapa, acao, msg){
    let arrayAreaMov = ["body", "#dvProjsActs1", "#dvProjsActs2", "#dvGerUgal"];// div onde o loading será exibido
    if(msg == ''){
        msg = 'Carregando...';   
	}
	if(etapa == ''){
		etapa =  0;
	}
    var _loading = FLUIGC.loading(''+etapa, {
        textMessage:  ' '+msg, 
        title: null,
        css: {
            padding:        0,
            margin:         0,
            width:          '30%',
            top:            '40%',
            left:           '35%',
            textAlign:      'center',
            color:          '#000',
            border:         '3px solid #aaa',
            backgroundColor:'#fff',
            cursor:         'wait'
        },
        overlayCSS:  { 
            backgroundColor: '#fff', 
            opacity:         0.6, 
            cursor:          'wait'
        }, 
        cursorReset: 'default',
        baseZ: 1000,
        centerX: true,
        centerY: true,
        bindEvents: true,
        fadeIn:  200,
        fadeOut:  400,
        timeout: 0,
        showOverlay: false, 
        onBlock: null,
        onUnblock: null,
        ignoreIfBlocked: false
    });
    if(acao == 'show'){
        _loading.show();
    }else{
        _loading.hide();
    }
}
/** 21/09/2020 - Avalia se o valor do campo possui R$
 * @author {Jean Varlet}
 * @param {varchar} - value
 */
function isMoneyFormat(value){
    if(value.indexOf('R$') != -1){
        return true;
    }else{
        return false;
    }
}
/** Checar se o Id gerado por gerarIdSolicitacao() já existe
 * @RETURNS {BOOLEAN}*/
function checkId(id){
	
	let codSolicitacao = DatasetFactory.createConstraint('codigoSolicitacao', id, id, ConstraintType.MUST );
	let solicitacoes = DatasetFactory.getDataset('ds_compras_diretas_v1', null, [codSolicitacao], null);
	
	if( !jQuery.isEmptyObject(solicitacoes) ){
		if(solicitacoes.values.length > 0){
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
	
}
function gerarIdSolicitacao(field){
	
	var idGerado =  Math.random().toString(36).slice(-10);
	if(checkId(idGerado)){
		$("#"+field).val(idGerado) ;
		return true;
	}else if(!checkId(idGerado)){
		console.log('gerarIdSolicitacao()-> false, gerando outro Id' );
		gerarIdSolicitacao();
	}   
}

function showParams(caller,properties, values){
    if(values.length){
       for(var i = 0; i < values.length; i++){
           console.log('Caller-> '+caller+' prop-> '+properties.split(',')[i]+' value-> '+values[i]);     
        }
    }
}


function validacgc(el, str){    		
	str = str.replace(/[^\d]+/g, '');
	if (str.length == 11) {
		validacpf(el, str)
	}  else	if (str.length == 14){
		validacnpj(el, str)
	} else if(str.length > 0 && str.length < 11) {
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		});
	}
}
function validacnpj(el, cnpj) {
	cnpj = cnpj.replace(/[^\d]+/g, '');
	if (cnpj == '') {
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		})
		return false
	}
	if (cnpj.length != 14){    		
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		});
		return false
	}
	// Elimina CNPJs invalidos conhecidos
	if (cnpj == "00000000000000" ||
		cnpj == "11111111111111" ||
		cnpj == "22222222222222" ||
		cnpj == "33333333333333" ||
		cnpj == "44444444444444" ||
		cnpj == "55555555555555" ||
		cnpj == "66666666666666" ||
		cnpj == "77777777777777" ||
		cnpj == "88888888888888" ||
		cnpj == "99999999999999")
	{
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		})
		return false
	}

	// Valida DVs
	tamanho = cnpj.length - 2
	numeros = cnpj.substring(0, tamanho);
	digitos = cnpj.substring(tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0))
	{
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		})
		return false
	}

	tamanho = tamanho + 1;
	numeros = cnpj.substring(0, tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1))
	{
		FLUIGC.toast({
			title: 'CPF/CNPJ Inválido!',
			message: '<br>Favor digitar CPF/CNPJ válido',
			type: 'danger'
		})
		return false
	}

	cnpj = cnpj.replace(/(\d{2})(\d)/, "$1.$2"); // Coloca um ponto
													// entre o SEGUNDO e
													// o TERCEIRO
													// dígitos
	// console.log(cnpj)
	cnpj = cnpj.replace(/(\d{3})(\d)/, "$1.$2"); // Coloca um ponto
													// entre o terceiro
													// e o quarto
													// dígitos
	// console.log(cnpj)
	cnpj = cnpj.replace(/(\d{3})(\d)/, "$1/$2"); // Coloca um ponto
													// entre o terceiro
													// e o quarto
													// dígitos
	// console.log(cnpj)
	// de novo (para o segundo bloco de números)
	cnpj = cnpj.replace(/(\d{3})(\d{1,2})$/, "$1-$2"); // Coloca um
														// hífen entre o
														// terceiro e o
														// quarto
														// dígitos
	// console.log('cnpj alterado:' + cnpj)
	$('#' + el).val(cnpj);    		
	return true;
}
/** @function NotificarEmail() - Função para envio de emails padrão 
 * @author {Jean Varlet}
 */
function notificaColabs(){
    showLoading('#loadingEnvio', 'show', 'Notificando Colaboradores...');
    let enviados = 0;
    let arrayEnvio = [];
    let cst1 = DatasetFactory.createConstraint('active', true, true, ConstraintType.MUST);
    let retorno = DatasetFactory.getDataset('colleague', null, [cst1], null);
        
        if(!jQuery.isEmptyObject(retorno)){
            for(var i = 0; i < retorno.values.length; i++){
                if(retorno.values[i].mail != '' && retorno.values[i].mail != null && retorno.values[i].mail.indexOf('@') != -1){
                                        //nome, matricula, email
                    arrayEnvio.push([retorno.values[i].colleagueName, retorno.values[i]['colleaguePK.colleagueId'], retorno.values[i].mail])
                    //enviaOfertasMail(retorno.values[i].colleagueName, retorno.values[i]['colleaguePK.colleagueId'], retorno.values[i].mail);    
                }
            
            }
            //Chama enviaOfertasMail()->
            enviaOfertasMail(arrayEnvio);
        }else{
            console.log('Erro ao retornar os dados de COLLEAGUE');
        }
    return true;
    
}
//nome, matricula, email
function NotificarEmail(dados){	
	let  dataInicio, dataFim, titulo, resultado, template, aplicacaoConteudo, tenantId, template1, template2, cst1, consulta;
	let srvURL = parent.WCMAPI.serverURL;
	if(srvURL.indexOf('fluighomolog') != -1){
		tenantId = 1;	
	}else{
		tenantId = 'SEBRAEPE';
	}
    var linkAcesso = srvURL+ "/portal/p/"+tenantId+"/pageworkflowview?processID=v2_Solicitacao_Capacitacao&idOferta="+$("#idSolicitacao").val();
       template = "<h2 style='text-align:center; magin-bottom:20px;' >Uma nova oferta de capacitação acaba de ser disponibilizada. </h2>"+
               "<h3 style='text-align:center; magin-bottom:10px; '><b>Título da Capacitação: </b>"+$("#titulo").val()+" </h3>"+
               "<h3 style='text-align:center; magin-bottom:10px;'><b style='text-align:center; color:#5b90c9; '>Carga horária: </b> "+$("#cargaHorariaTotal").val()+"</h3>"+
               "<h3 style='text-align:center; magin-bottom:10px; '><b style='text-align:center; color:#5b90c9;'>Vagas disponibilizadas: </b> "+$("#qtdvagas").val()+"</h3>"+
               "<h3 style='text-align:center; magin-bottom:10px; '><b style='text-align:center; color:#5b90c9;'>Data de Início: </b> "+$(".dataInicio:visible:first").val()+" </h3>"+
               "<h3 style='text-align:center; magin-bottom:10px; '><b style='text-align:center; color:#5b90c9;'>Data de Término: </b> "+$(".dataFinal:visible:last").val()+" </h3>"+ 
               "<h3 style='text-align:center; magin-bottom:10px; '><b style='text-align:center; color:#5b90c9;'>Prazo para inscrição: </b> <i style='color:red;'>"+ $("input", $("#prazoOferta")).val()+" </i></h3>"+ 
               "<h3 style='text-align:center; magin-bottom:10px; '><b style='text-align:center; color:#5b90c9;'>Horários: </b> das  "+$(".horaInicio:visible:first").val()+" às "+$(".horaFinal:visible:first").val()+" </h3>"+
               "<h3 style='text-align:center; magin-bottom:10px; '><b style='text-align:center; color:#5b90c9;'>Detalhes: </b> </h3>"+
               "<h3 style='text-align:center; magin-bottom:10px; '><p style='text-align: justify'>"+$("#descricao").val() +"</p></h3>"+
               "<br><br>"+
               "<hr>"+
               "<pstyle='text-align:center; magin-bottom:10px; '><a style='text-align:center;'  href="+linkAcesso+">Para mais detalhes, clique aqui</a> </p>"+
                "<hr>" ;
    let nome, matricula;
    let email = [] 
    if(dados.length){
		for(var d = 0; d < dados.length; d++){
			if(dados[d][2] != '' && dados[d][2] != undefined){
			email.push(dados[d][2]);
				console.log('Pushed address-> '+ dados[d][2])  
			}
			
		}
    }else{
        console.log('Erro ao recuperar os dados dos destinatários da oferta');
    }
  
	var parametros = {
	        param: [
	           // { "SERVER_URL": "http://fluighomolog.sebraepb.com.br:8080/" },
	            { "SERVER_URL": srvURL },
	            //{ "SOLICITACAO_NUM": WKNumProces },
	            { "SOLICITANTE": 'Colaborador' },
	            { 'MENSAGEM': '<br>'+
	            	template},
	            { 'TENANT_ID': tenantId } // company id
	        ]
	    }
	var destinatarios = email;
    console.log(destinatarios);
	var fields = ['2423', 'notificaOfertaCapacitacao', JSON.stringify(parametros), JSON.stringify(destinatarios)]
	var ds = DatasetFactory.getDataset('dsEnvioEmail', fields, null, null)
	ds;
	setTimeout(function(){
        showLoading('#loadingEnvio', 'hide', '');
		FLUIGC.toast({
	        title: 'Notificação!',
	        message: '<br>E-mail enviado - Colaboradores Notificados sobre essa oferta.' ,
	        type: 'success'
	
			});
		
	}, 3000)
		
	
}//notificaColabs()