/**
 * 
 * Jean Varlet - Consulta de certidões utilizando a API do sistema Gescon --- 13/10/2020 --
 * 
 */

//TODO: Adicionar condicional para apontar o problema exato da certidão - se pendência ou validade -
 /** @funciton consultaApiGescon(cnpj) - 
  * @author {Jean Varlet}
  * @param {string} - cpnj sem pontos e barras
  */
 function consultaApiGescon(cnpj, index){
     
    //Consulta RM-->
    let consulta;
    let coForn = $("#codcfo___"+index).val();
    let forn = DatasetFactory.createConstraint('CODCFO', coForn, coForn, ConstraintType.MUST);
    let rm =  DatasetFactory.getDataset('ds_consulta_certidoes_RM', null, [forn], null);   
    //Consulta Gescon -> 
    let fornecedor = DatasetFactory.createConstraint('CNPJ', cnpj, cnpj, ConstraintType.MUST);
    let ret = DatasetFactory.getDataset('ds_api_gescon_compras_diretas', null, [fornecedor], null);
    if(!jQuery.isEmptyObject(rm) && rm.values.length){
        consulta = rm.values[0];
    }
    else if(!jQuery.isEmptyObject(ret) && ret.values.length ){
        consulta = ret.values[0];
    }
    else{
        console.log('A consulta a API Gescon não retornou resultados');
        return null;
    }
    let _validadeCnd = '';    
    let _validadeFgts = ''; 
    if(moment(consulta.cndValidade).format("DD/MM/YYYY") == "Invalid date"){
        _validadeCnd = 'Data não retornada';       
    }else{
        _validadeCnd = moment(consulta.cndValidade).format("DD/MM/YYYY");
    }   
    
    if(moment(consulta.fgtsValidadeFim).format("DD/MM/YYYY")== "Invalid date"){
        _validadeFgts = 'Data não retornada';       
    }else{
        _validadeFgts = moment(consulta.fgtsValidadeFim).format("DD/MM/YYYY");
    }   
    if(consulta.cndSituacao != 'FalhaAoConsultarCertidao' && consulta.cndSituacao != 'null' ){
        if(checaValidadeCnd(consulta.cndSituacao, consulta.cndValidade)){

            //Appenda ícone de certidão ok 
            $("#certidaoCnd___"+index).append(
                '<div class="row">'+
                '   <div class="col-md-1 col-sm-12">'+
                '       <i class="fluigicon fluigicon-enrollment-verified btn-success icon-lg"></i>   '+
                '   </div>'+
                '   <div class="col-md-9 col-sm-12">'+
                '       <h3>Certidão Negativa de Débitos</h3>'+
                '       <h4 id="cndSit___'+index+'"><b style="color:#2E66B7;">Situação: </b> '+consulta.cndSituacao+' <i class="fluigicon fluigicon-check-circle-on" style="color:green"></i></h4>  '+
                '       <h4 id="cndVal___'+index+'"><b style="color:#2E66B7;">Validade: </b> '+_validadeCnd+' <i class="fluigicon fluigicon-verified icon-sm" style="color:green"></i></h4>  '+
                '   </div>'+
                '</div><!--row-->'
            )          
        }else{//CND com retorno irregular ---
            $("#certidaoCnd___"+index).append(
                '<div class="row">'+
                '   <div class="col-md-1 col-sm-12">'+
                '       <i class="fluigicon fluigicon-enrollment-verified btn-danger icon-lg"></i>   '+
                '   </div>'+
                '   <div class="col-md-9 col-sm-12">'+
                '       <h3>Certidão Negativa de Débitos</h3>'+
                '       <h4 id="cndSit___'+index+'"><b style="color:#2E66B7;">Situação: </b> '+consulta.cndSituacao+' <i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i></h4>  '+
                '       <h4 id="cndVal___'+index+'"><b style="color:#2E66B7;">Validade: </b> '+_validadeCnd+' <i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i></h4>  '+
                '   </div>'+
                '</div><!--row-->'
            ) 
        }
        if(checaValidadeFgts(consulta.fgtsSituacao, consulta.fgtsValidadeFim)){
            //Appenda ícone de certidão ok    
            $("#certidaoFgts___"+index).append(
                '<div class="row">'+
                '   <div class="col-md-1 col-sm-12">'+
                '       <i class="fluigicon fluigicon-enrollment-verified btn-success icon-lg"></i>   '+
                '   </div>'+
                '   <div class="col-md-9 col-sm-12">'+
                '       <h3>Certificado de Regularidade do FGTS</h3>'+
                '       <h4 id="fgtsSit___'+index+'"><b style="color:#2E66B7;">Situação: </b> '+consulta.fgtsSituacao+' <i class="fluigicon fluigicon-check-circle-on" style="color:green"></i></h4>  '+
                '       <h4 id="fgtsVal___'+index+'"><b style="color:#2E66B7;">Validade: </b> '+_validadeFgts+' <i class="fluigicon fluigicon-verified icon-sm" style="color:green"></i></h4>  '+
                '   </div>'+
                '</div><!--row-->'
            )     
        }else{// FGTS com retorno irregular ---
            $("#certidaoFgts___"+index).append(
                '<div class="row">'+
                '   <div class="col-md-1 col-sm-12">'+
                '       <i class="fluigicon fluigicon-enrollment-verified btn-danger icon-lg"></i>   '+
                '   </div>'+
                '   <div class="col-md-9 col-sm-12">'+
                '       <h3>Certificado de Regularidade do FGTS</h3>'+
                '       <h4 id="fgtsSit___'+index+'"><b style="color:#2E66B7;">Situação: </b> '+consulta.fgtsSituacao+' <i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i></h4>  '+
                '       <h4 id="fgtsVal___'+index+'"><b style="color:#2E66B7;">Validade: </b> '+_validadeFgts+' <i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i></h4>  '+
                '   </div>'+
                '</div><!--row-->'
            )  
        }
        showLoading('#entradaCot___'+index, 'hide', '');
    }else{//Falha ao consultar certidão
        //Apendar msg de falha de consulta e oferecer uploads---
        $("#certidaoFgts___"+index).append(
            '<div class="row">'+
            '   <div class="col-md-1 col-sm-12">'+
            '       <i class="fluigicon fluigicon-enrollment-verified btn-danger icon-lg"></i>   '+
            '   </div>'+
            '   <div class="col-md-9 col-sm-12">'+
            '       <h3>Certificado de Regularidade do FGTS</h3>'+
            '       <h4 id="fgtsSit___'+index+'"><b style="color:#2E66B7;">Situação: </b> Não foi possível consultar essa certidão. <i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i></h4>  '+
            '       <h4 id="fgtsVal___'+index+'"><b style="color:#2E66B7;">Validade: </b> Não retornada. <i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i></h4>  '+
            '   </div>'+
            '</div><!--row-->'
        ) 
        $("#certidaoCnd___"+index).append(
            '<div class="row">'+
            '   <div class="col-md-1 col-sm-12">'+
            '       <i class="fluigicon fluigicon-enrollment-verified btn-danger icon-lg"></i>   '+
            '   </div>'+
            '   <div class="col-md-9 col-sm-12">'+
            '       <h3>Certidão Negativa de Débitos</h3>'+
            '       <h4 id="cndSit___'+index+'"><b style="color:#2E66B7;">Situação: </b> Não foi possível consultar essa certidão. <i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i></h4>  '+
            '       <h4 id="cndVal___'+index+'"><b style="color:#2E66B7;">Validade: </b> Não retornada. <i class="fluigicon fluigicon-remove-sign icon-sm" style="color:red;"></i></h4>  '+
            '   </div>'+
            '</div><!--row-->'
        ) 
        $("#dvanexarCertidoes___"+index).fadeIn();
        showLoading('#entradaCot___'+index, 'hide', '');
    }   
        
    
 }

 /** @function checaValidadeCnd(situacao, validade, emissao, codigo) - aux de consultaApiGescon() - Avalia os dados
  * retornados através da consulta de certidões do fornecedor.
  * @author {Jean Varlet}
  * @param {string} - situacao - 
  * @param {Date} - validade - 
  * @param {Date} - emissao  -
  * @param {string} - codigo - 
  * @returns {boolean}
  */
 function checaValidadeCnd(situacao, validade){
    let _retorno = false; 
    if(situacao == "NADA CONSTA"){//Checa msg da situação da certidão
        _retorno = true;   
    }else if(situacao == "SucessoComRessalvas"){
        _retorno = true; 
    }
    if(validade != null && validade != 'null' && validade != ''){
        if(validade.indexOf('T') != -1){
            if(moment().isAfter(moment(validade.split('T')[0]))){
                _retorno = false;
            }
        }else{
            if(moment().isAfter(moment(validade))){
                _retorno = false;
            }
        }
        
    }
    
    return _retorno;
 }
 /** @function checaValidadeFgts(situacao, validade) -  
  * @author {Jean Varlet}
  * @param {string} - situacao
  * @param {Date} - validade
  * @returns {boolean} 
  */
 function checaValidadeFgts(situacao, validade){
     let _retorno = true;
     if(situacao !== 'Regular'){
        _retorno = false;   
     }
     if(validade.indexOf('T') != -1){
        if(moment().isAfter(moment(validade.split('T')[0]))){
            _retorno = false;
        }
     }else{
        if(moment().isAfter(moment(validade))){
            _retorno = false;
        }
     }
     
     return _retorno;

 }