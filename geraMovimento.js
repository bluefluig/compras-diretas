/** @function callGerarMovs() - Responsável por receber a chamada do botão e realizar a geração dos movimentos
 *  @author {Jean Varlet}
 */
function callGerarMovs(){
    if($("#dvDetalhesMov").children().length){
        $("#dvDetalhesMov").children().remove();    
    }
    $("#dvDetalhesMov").fadeIn();
    var _conteudo = JSON.parse($("#conteudoFinal").val());
    let _codcfo = _conteudo.codcfo;
    let dataEntrega = _conteudo.dataEntrega;
    let _ret = _conteudo.cotacao;
    let  xmlItensRateios = fnTitmmov( _ret ); 
    showLoading('#dvDetalhesMov', 'show', 'Gerando movimentos...');
    setTimeout(function(){
        if(geraMovimento(_codcfo, dataEntrega, xmlItensRateios)){
            showLoading('#dvDetalhesMov', 'hide', 'Gerando movimentos...');
        }
    },300)
    $("#dvDetalhesMov").append(
        '<h4 class="pMovimento" style="padding-top:20px;">Movimentos Gerados:  </h4>'
    );
 
}
/** @function fnTitmmov(produtos) - Recebe o JSON de produtos para geração de duas áreas do xml produtos e rateios
 *  @author {Jean Varlet}
 *  @param {JSON} - produtos
*/
function fnTitmmov( produtos ){
    let titmov = '';
    
    if(produtos.length){
        for(let p = 0; p < produtos.length; p++){
            var _IDPRD = produtos[p].idProduto;
            var _QUANTIDADE = produtos[p].quantidade;
            var _PRECOUNITARIO = floatReais(produtos[p].valor);
            var _NSEQITMMOV = p + 1;
            var _xmlItemMov = `<TITMMOV><IDMOV>-1</IDMOV>
                               <CODCOLIGADA>1</CODCOLIGADA>
                               <NSEQITMMOV>${_NSEQITMMOV}</NSEQITMMOV>
                               <IDPRD>${_IDPRD}</IDPRD>
                               <QUANTIDADE>${_QUANTIDADE}</QUANTIDADE>
                               <PRECOUNITARIO>${parseFloat(_PRECOUNITARIO).toFixed(2).toString().replace('.',',')}</PRECOUNITARIO>
                               <CODUND>UN</CODUND>
                               </TITMMOV>`;
            //titmov += _xmlItemMov;
            let rateios = '';
            if(produtos[p].rateios.length){
                for(let r = 0; r < produtos[p].rateios.length; r++){
                    var  _CODCCUSTO = produtos[p].rateios[r][0].toString().split(',')[0].trim();
                    var _RAT = produtos[p].rateios[r][0].toString().split(',')[1].trim();
                    var _valor = retornaValorRatMov(_PRECOUNITARIO, _QUANTIDADE, _RAT)
                    var _xmlRateio = `<TITMMOVRATCCU>
                                        <CODCOLIGADA>1</CODCOLIGADA>
                                        <IDMOV>-1</IDMOV>
                                        <NSEQITMMOV>${_NSEQITMMOV}</NSEQITMMOV>
                                        <CODCCUSTO>${_CODCCUSTO}</CODCCUSTO>
                                        <VALOR>${parseFloat(_valor).toFixed(2).toString().replace('.',',')}</VALOR>
                                        <IDMOVRATCCU>-1</IDMOVRATCCU>
                                    </TITMMOVRATCCU>`;
                     rateios += _xmlRateio;
                }
            }
            titmov = _xmlItemMov + rateios;
        }
    }
    var saida = titmov;
    console.log(saida);
    return saida;
    
}

function retornaValorRatMov(preco, qtd, rateio){
    console.log('preco '+ preco + ' qtd '+ qtd + ' rateio '+ rateio)
    let _total = parseFloat(preco * qtd);
    let _scale = _total/100;
    let _liq = _scale * rateio;

    return _liq;
}

function geraMovimento( CODCFO, DATAENTREGA,  xmlItensRateios ){
    let CODCOLIGADA = "1";
    let IDMOV = "-1"; // adicionar condicional - se novo movimento ou nao
    let CODTMV = "1.1.93";
    let CODFILIAL = "1";
    let CODCOLCFO = 1;
    let DATAEMISSAO = moment().format("YYYY-MM-DD"); 
    let DATASAIDA = moment().format("YYYY-MM-DD"); 
    let DATAEXTRA1 = moment().format("YYYY-MM-DD");
    let DATAMOVIMENTO = moment().format("YYYY-MM-DD");
    let VALORDESC = 0; 

    let CODLOC = "01";
    let CODCOLCXA = 1;
    let CODCXA = "041";
    let CODCPG = "007";

    //let CODTB1FLX = ""; // preenchido no dataset - 
    let CODTB2FLX = "02";
    let PERCENTUALDESC = 0;
    let SEGUNDONUMERO = 0;
    let HISTORICOCURTO = "Movimento gerado através de uma solicitação de compras diretas do Fluig";

    let CHAVEORIGEM = "Fluig - "+ $("#nrSolicitacao").val(); 
    let CODTB1FAT = "03.01.04";
    let NSEQITMMOV = "1";

   
    let IDPRD = ""; //Será retornado do JSON por item de compra
    let CODCCUSTO
    let QUANTIDADE = "";//Quantidade  por produto
    let PRECOUNITARIO = "";//Valor unitário por produto
    let CODUND = "UN"; 
    let IDMOVRATCCU = -1;

    
    
    var citens = new Array();
	citens.push(DatasetFactory.createConstraint("CODCOLIGADA", CODCOLIGADA, CODCOLIGADA, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("IDMOV", IDMOV, IDMOV, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODTMV", CODTMV, CODTMV, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCOLCFO", CODCOLCFO, CODCOLCFO, ConstraintType.MUST));		
    citens.push(DatasetFactory.createConstraint("CODCFO", CODCFO, CODCFO, ConstraintType.MUST));

    citens.push(DatasetFactory.createConstraint("DATAEMISSAO", DATAEMISSAO, DATAEMISSAO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("DATASAIDA", DATASAIDA, DATASAIDA, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("DATAMOVIMENTO", DATAMOVIMENTO, DATAMOVIMENTO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("DATAENTREGA", moment(DATAENTREGA, "DD/MM/YYYY").format("YYYY-MM-DD"), moment(DATAENTREGA, "DD/MM/YYYY").format("YYYY-MM-DD"), ConstraintType.MUST));

    citens.push(DatasetFactory.createConstraint("VALORDESC", VALORDESC, VALORDESC, ConstraintType.MUST)); //new
    citens.push(DatasetFactory.createConstraint("CODLOC", CODLOC, CODLOC, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCOLCXA", CODCOLCXA, CODCOLCXA, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCXA", CODCXA, CODCXA, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODCPG", CODCPG, CODCPG, ConstraintType.MUST));
   // citens.push(DatasetFactory.createConstraint("CODTB1FLX", CODTB1FLX, CODTB1FLX, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODTB2FLX", CODTB2FLX, CODTB2FLX, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("PERCENTUALDESC", PERCENTUALDESC, PERCENTUALDESC, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("SEGUNDONUMERO", SEGUNDONUMERO, SEGUNDONUMERO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("HISTORICOCURTO", HISTORICOCURTO, HISTORICOCURTO, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CODFILIAL", CODFILIAL, CODFILIAL, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("CHAVEORIGEM", CHAVEORIGEM, CHAVEORIGEM, ConstraintType.MUST));
    citens.push(DatasetFactory.createConstraint("xmlItensRateios", xmlItensRateios.trim(), xmlItensRateios.trim(), ConstraintType.MUST));
					
	console.log(citens);

	var dts = DatasetFactory.getDataset("ds_insere_mov_compras_diretas", null, citens, null); 				
	if(dts){
        console.log(dts.values[0]);
        if(dts.values[0].Result.toString().split(';')[0] == 1){
            $(".pMovimento").text('Movimento Gerado com sucesso. Código do movimento gerado: ' + dts.values[0].Result.split(';')[1]);
            toast('Sucesso!', 'Movimento Gerado com Sucesso', 'success');
            $("#dvDetalhesMov").append(
                '<div class="alert alert-success" role="alert">Movimento Gerado com Sucesso</div>'
            )
            $("#movimentosGerados").val(dts.values[0].Result.split(';')[1])
        }else{
            $(".pMovimento").text('Ocorreu um problema ao gerar o movimento. Mensagem retornada: ' + dts.values[0].Result);  
            toast('Erro!', 'Não foi possível Gerar o movimento.', 'danger');  
            $("#dvDetalhesMov").append(
                '<div class="alert alert-danger" role="alert">Não foi possível Gerar o movimento.</div>'
            )
        }
        
        return true;
    } 	
    
} 