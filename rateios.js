/** @function addRateioClkBtn() - Chama o modal para seleção e adição de itens de rateios. 
* Avalia se os dados do item de compra foram informados ou não.
* @author {Jean Varlet}
* @param {object} - html item
*/
function addRateioClkBtn(el){

	//nullish coalescing adicionado por contado bug encontrado na segunda etapa (id do paixfilho não está sendo atualizado)
	//TODO encontrar um método mais eficiente de retornar o índice da tabela
	let indexer = el.id.split('___')[1];
    let itemRat = 1; 
	let quantidade = 1; 
	let custo, titulo;
	let modo = 'none';

	if($("#bodyRateiostbl___"+indexer).children().length){//Checa se já existem itens adicionados ao datatable de rateios do item em questão
		modo = 'itens';
		quantidade = $("#bodyRateiostbl___"+indexer).children().length;
	}
	
	if($("#produtoRm___"+indexer).val() == ''){// Se o item de compra não possuir um título - Item ainda ainda não selecionado ----
		titulo = 'Título do Produto não informado';
	}else{
		titulo = $("#produtoRm___"+indexer).val();
		$("#produtoRm___"+indexer).prop("readonly", true);
		$("#especificacao___"+indexer).prop("readonly", true);
		$("#valCustoAproxItem___"+indexer).prop("readonly", true);
		$("#quantidade___"+indexer).prop("readonly", true);
		$("#obsItem___"+indexer).prop("readonly", true);
		FLUIGC.switcher.disable("#custoAproxItem___"+indexer);
		//if(FLUIGC.switcher.getState('#custoAproxItem___'+indexer)){
		if($("#estadoCustoAprox___"+indexer).val() == 'sim'){
			modalRat('sim', $("#valorNecessario___"+indexer).val(), quantidade, indexer,  titulo, 'full', modo) ;	
		}else{
			//Exibir msg sobre valor aprox. não informado!
			modalRat('nao', 'R$ 0,00', quantidade, indexer,  titulo, 'full', modo) ;
		}
		
	}
		
}

/** @function modalRat() -   Exibe modal para inserção de rateios para um determinado item de compra
*  	Caso o param modo == itens, varre os itens existentes no datatable do item
* 	@author {Jean Varlet}
* 	@param {varchar} titulo -> titulo do produto/item corrente;
* 	@param {string} size -> Tamanho do modal ( small  || large || full ).
* 	@param {int} indexer-> indexer pai x filho do item;
* 	@param {int} modo -> 1 = novo registro, 2 = edicao registro;
* 	@param {int} itemRat -> posicao de ordem do rateio na lista do produto;
* 	@param {array} botoes -> Array de botões a serem  inseridos.
*/
function modalRat(custoInformado, custo, quantidade, indexer, titulo, size, modo){
	let content ='';
	let itemRat;
    if(quantidade){//Avalia se a quantidade de itens de rateio é maior q zero-- Avaliar --
		if(custoInformado != 'sim'){
			custo = 'R$ 0,00'; 
			checkRatPercent();
		}
		for(var i = 0; i < quantidade ; i++){
			itemRat = i + 1;
			content += '<div class="itemRat" id="itemRat___'+indexer+'___'+itemRat+'">'+
		  
			'<h3 style:"color:blue;">Item de Rateio: '+itemRat+' </h3>'+
			'<hr>'+
				
		      '<div class="col-md-1 col-sm-12 " >'+
		            '<i class="fluigicon fluigicon-trash icon-md btn btn-danger delRateio" style="cursor:pointer; margin-top:120px;" id="delRateio___'+indexer+'___'+itemRat+'" onclick="delItemRateio(this);"></i>'+
		      '</div><!--lixeira-->'+
		      '<div class="col-md-1 col-sm-12 " >'+
		            '		<i class="fluigicon fluigicon-pencil icon-sm btn btn-warning editRateiotbl" style="cursor:pointer;margin-top:120px;display:none; " id="editRateio___'+indexer+'___'+itemRat+'"></i>'+
		      '</div><!--lixeira-->'+
		      '<div class="col-md-10 ">'+  
		        '<div class="row">'+
		            '<div class="col-md-6 col-sm-12 form-group">'+
		                '<h3>Unidade</h3>'+
		                '<input type="text" name="unidadeRateio" id="unidadeRateio___'+indexer+'___'+itemRat+'" class="form-control unidadeRateio itemRateioModal">'+
		            '</div>'+
		            '<div class="col-md-6 col-sm-12 form-group">'+
		                '<h3>Projeto</h3>'+
		                '<input type="text" name="projetoRateio" id="projetoRateio___'+indexer+'___'+itemRat+'" class="form-control projetoRateio itemRateioModal">'+
		            '</div>'+
		        '</div><!--row-->'+
		        '<div class="row">'+
		            '<div class="col-md-6 col-sm-12 form-group">'+
		                '<h3>Ação</h3>'+
		                '<input type="text" name="acaoRateio" id="acaoRateio___'+indexer+'___'+itemRat+'" class="form-control acaoRateio itemRateioModal">'+
		            '</div>'+
		            '<div class="col-md-6 col-sm-12 form-group">'+
		                '<h3>Saldo da Ação</h3>'+
		                '<input type="text" name="saldoAcaoRateio" id="saldoAcaoRateio___'+indexer+'___'+indexer+'" class="form-control itemRateioModal" readonly value="R$ 0,00">'+
		            '</div>'+
		        '</div><!--row-->'+
				'<div class="row">'+
					'<div class="col-md-3  form-group">'+
						'<h3>COD UNIDADE</h3>'+
						'<input type="text" name="codUnidade" id="codUnidade___'+indexer+'___'+itemRat+'" class="form-control codUnidade itemRateioModal" readonly>'+
					'</div>'+
					'<div class="col-md-3 col-sm-12 form-group">'+
						'<h3>Necessário</h3>'+
		                '<input type="text" name="valNecess" id="valNecess___'+indexer+'___'+itemRat+'" class="form-control valNecess itemRateioModal" value="'+custo+'" readonly>'+
					'</div>'+
		            '<div class="col-md-3 col-sm-12 form-group">'+
		                '<h3>Percentual Rateio</h3>'+
		                '<input type="number" name="percRateio" id="percRateio___'+indexer+'___'+itemRat+'" class="form-control percRateio itemRateioModal" min="0" max="100" value="0">'+
		            '</div>'+
		            '<div class="col-md-3 col-sm-12 form-group">'+
		                '<h3>Valor Rateio</h3>'+
		                '<input type="text" name="valRateio" id="valRateio___'+indexer+'___'+itemRat+'" class="form-control valRateio itemRateioModal" value="R$ 0,00">'+
		            '</div>'+
		        '</div><!--row-->'+
		       
		     '</div>'+
			'<hr>'+
			'</div><!--itemRat___-->'; 	
		}	
	}
 	let conteudo = ''+
	'<div id="dvRateios">'+
	   content+
	'</div><!--dvRateios-->'+
	 '<div class="row">'+
				'<div class="col-md-12 col-sm-12 form-group">'+					
				'	<button type="button" class="btn btn-warning form-control btnAddRat" style="margin-top:22px;" id="btnAddRat" name="btnAddRat" onclick="callModalFilhosRat();" >'+												
				//<button type="button" class="btn btn-warning form-control btnAddProjAc" style="margin-top:22px;" id="addProjAcao" name="addProjAcao" onclick="addRateioClkBtn(this);" >'+
				'	<i class="fluigicon fluigicon-money icon-sm" style="color:white; "></i><strong> &nbsp; Adicionar Projeto & Ação para esse item</strong> </button>	'+												
				'</div>'+											
	        '</div><!--row-->';

    FLUIGC.modal({
        title: titulo,
        content: conteudo,
        size: size,
        actions: [{
	        'label': 'Salvar',
	        'bind': 'data-open-modal',
    	},{
        'label': 'Fechar',
        'autoClose': true
   		 }]
    }, function (result, el, ev) { 
		//	console.log(result);
		//	console.log(el);
		//	console.log(ev);
		
	});
	
	$(".btn-primary", $(".modal-footer")).click(function(){//Click Save
		//checa se existe alguma row na tabela, caso sim, remove e adiciona as novas. 
		if(modo != 'itens' && $("#bodyRateiostbl___"+indexer).children().length){
			$("#bodyRateiostbl___"+indexer).children().remove();	
		}
	    $(".itemRat").each(function(){
	        let id = $(this).attr("id");
	        let pos = id.split('itemRat___')[1];
	        $('input', $('#'+id)).each(function(){
	            console.log('Checando itens -> '+ id);
	            if($(this).val() == ''){
	                $(this).addClass('has-error');  
	            }else{
	                $(this).removeClass('has-error'); 
	            }
	        })
		   addRowRateioTbl(pos, $("#unidadeRateio___"+pos).val(),
								$("#projetoRateio___"+pos).val(),
								$("#acaoRateio___"+pos).val(), 
								$("#percRateio___"+pos).val(), 
								$("#valRateio___"+pos).val(), 
								$("#codUnidade___"+pos).val() );
	    });
		//Adicionar checagem antes de adicionar outro item de rateio
		   setTimeout(function(){
				recalcTotalRateios(indexer);
				$(".delRateiotbl").click(function(){
					let _id = $(this).attr("id").split('delRateiotbl___')[1];
					$("#trRateiotbl___"+_id).remove();
					recalcTotalRateios(indexer);
					setTimeout(function(){
						if(custoInformado == 'sim'){
							if($("#valorNecessario___"+indexer).val() == $("#valorTotItemRat___"+indexer).val()){
								$("#addProjAcao___"+indexer).prop("disabled",false);	
							}
						}else{
							$("#addProjAcao___"+indexer).prop("disabled",false);	

						}
						
					},500)	
				});
				
				toast('Sucesso!', 'Rateios Adicionados.', 'success');
				$(".btn-default", $(".modal-footer")).click();
		   },500)
		setTimeout(function(){
			if($("#valorNecessario___"+indexer).val() == $("#valorTotItemRat___"+indexer).val()){
				$("#addProjAcao___"+indexer).prop("disabled",true);	
			}
		},1000)
				
	});
	
	//Ativa campos de seleção
	for(var a = 0; a < quantidade; a++){
		var _a = a+1;
		var _posicao = indexer+'___'+_a;

		fnAutoComplete(_posicao );
		
	}
	//TODO: Ajustar evento de calculo de valores entre valor e percentual 
	$(".percRateio").click(function(){
		let _itemRat = $(this).attr("id").split('___')[2];
		let _indexer = $(this).attr("id").split('___')[1];
		
		atualizaNecessario(_indexer, _itemRat, custoInformado, $('.valNecess:last').val());
		
	})
	$(".percRateio").blur(function(){
		if($(this).val() > 100){
			$(this).val('100');
		}
		let _itemRat = $(this).attr("id").split('___')[2];
		let _indexer = $(this).attr("id").split('___')[1];
		atualizaNecessario(_indexer, _itemRat, custoInformado, $('.valNecess:last').val());
		
	})
	$(".valRateio").click(function(){
		$(this).val('');
			
		
	})
	$(".valRateio").blur(function(){
		if($(this).val() != '' && $(this).val() != 'R$ 0,00'){
			$(this).val(reais($(this).val()));
		}else{
			$(this).val(reais(0));
		}
		if($(this).val() == 'R$ NaN'){
			$(this).val(reais(0))	;
		} 
	
		
	})	
}
/** 16/09/2020 - Percorre os valores adicionados e subtrai do valor necessário.
 * update 22/09/2020 - Jean Varlet - Added checkagem de saldo do centro de custo.
	@param {int} -> indexer -> item de compra
	@param {int} -> itemRat -> posição do item de rateio
	@param {varchar} -> custoInformado -> sim/nao
	@param {varchar} -> valorNecessario -> valor total necessário para o item de compra
	@author {Jean Varlet}
*/
function atualizaNecessario(indexer, itemRat, custoInformado, valorNecessario){
	console.log('atualizaNecessario()-> '+ custoInformado)
	let valorRateio = $("#valRateio___"+indexer+"___"+itemRat).val();
	let saldoCC = $("#saldoAcaoRateio___"+indexer+"___"+itemRat).val();
	valorNecessario = $("#valorNecessario___"+indexer).val(); // Jean Varlet - Para testes 07/09/2020 ----
	if(custoInformado == 'sim'){
		if(floatReais(saldoCC) >= floatReais(valorRateio)){
			if(floatReais(valorNecessario) >= floatReais(valorRateio) ){
				var stepPerc = floatReais(valorNecessario) / 100;
				var total = stepPerc * $("#percRateio___"+indexer+'___'+itemRat).val();
				$("#valRateio___"+indexer+'___'+itemRat).val(reais(total));		
			}else{
				toast('Atenção!', 'O valor informado é maior que o valor total necessário.', 'warning');
				$("#valRateio___"+indexer+'___'+itemRat).val(0);	
				$("#percRateio___"+indexer+'___'+itemRat).val(0);
			}
		}else{
			//Saldo do centro de custo inferior a necessidade!
			toast('Atenção!', 'Não há saldo suficiente nesse centro de custo', 'warning');
			$("#valRateio___"+indexer+'___'+itemRat).val(0);	
			$("#percRateio___"+indexer+'___'+itemRat).val(0);
		}
			
	}else{
		console.log('Custo não informado!');
		
		if(floatReais(saldoCC) > 0){
			$("#valRateio___"+indexer+'___'+itemRat).prop('readonly', true);
		}else{// Saldo do CC zerado
			$("#valRateio___"+indexer+'___'+itemRat).prop('readonly', true);
			toast('Atenção!', 'O Centro de custo selecionado está zerado.', 'warning');
		}

	}
}
/** New - preenche modal com itens já adicionadas - SEM USO ------>
* @author {Jean Varlet} -> 08/09/2020 - 
*/
function fillItensRateios(indexer, quantidade){
	var _qtdItens = $("tr",$("#bodyRateiostbl___"+indexer)).length;	
		for(var p = 0; p < _qtdItens; p++){
			if(p != 0){
				modalFilhosRateios(indexer, p, 'sim', '0,00',  'itens')	
			}
			
		}
		for(var i = 0; i <= _qtdItens; i++){
			if(i != 0){
				var  _unidade = $(".unidadeTbl", $("#trRateiotbl___"+indexer+"___"+i)).text();
		        var  _projeto = $(".projetoTbl", $("#trRateiotbl___"+indexer+"___"+i)).text();
		        var  _acao = $(".acaoTbl", $("#trRateiotbl___"+indexer+"___"+i)).text(); 
		        var  _percentual = $(".percentualTbl", $("#trRateiotbl___"+indexer+"___"+i)).text();
		        var  _valor = $(".valItemTblRat", $("#trRateiotbl___"+indexer+"___"+i)).text();
		        if(_unidade != '' && _unidade != null && _unidade != undefined){
					$("#unidadeRateio___"+indexer+'___'+i).val(_unidade);	
					$("#projetoRateio___"+indexer+'___'+i).val(_projeto);	
					$("#acaoRateio___"+indexer+'___'+i).val(_acao);	
					$("#percRateio___"+indexer+'___'+i).val(_percentual);	
					$("#valRateio___"+indexer+'___'+i).val(_valor);	
				}	
			}	
		}
		parseRateiosModal($("#valCustoAproxItem___"+indexer).val(), $("#quantidade___"+indexer).val())		
	
}
// SEM USO ---->
function parseRateiosModal(valorItem, qtd){
    let _total = 0;
    let valorNecessario = floatReais(valorItem) * qtd;
    
    $(".itemRat").each(function(){
        let id = $(this).attr("id").split('itemRat___')[1];
        console.log(id);
        
        if($("#valRateio___"+id).val() != ''){
            _total = _total + floatReais($("#valRateio___"+id).val());
            console.log('valorNecessario-> '+valorNecessario)
             console.log('_total-> '+_total)
            $("#valNecess___"+id).val(reais(valorNecessario - _total));              
        }
    })
}

// SEM USO ----->
/** Quando chamada modalFilhosRateios() modo == itens, varre os itens do datatable do item
* @param {int} qtd - quantidade de itens de rateio na tabela do Item/produto
* @param {int} indexer - posição do item/produto no DOM
* @author {Jean Varlet}
*/
function fillRateiosModal(indexer){
    $("tr",$("#dvTableRateios___"+indexer)).each(function(){
        let registros = $("tr",$("#dvTableRateios___"+indexer)).length;
        $(".dataTableRateios",$("#dvTableRateios___"+indexer)).each(function(){
        
              //addRowRateioTbl(posicao, unidade, projeto, acao, percentual, valor)		  
        })
    })
}
function recalcTotalRateios(item){
	console.log('recalcTotalRateios()->');
    let _total = 0;
	if($("tr", $("#bodyRateiostbl___"+item)).length){
		$("tr", $("#bodyRateiostbl___"+item)).each(function(){
		    console.log($(".valItemTblRat", $(this)).text());
            _total += floatReais($(".valItemTblRat", $(this)).text())		
			console.log('_total-> '+ _total);
		});
        $("#valorTotItemRat___"+item).val(reais(_total));	
	}
}
/** @function addRowRateioTbl() - Adiciona os itens de rateio selecionados no modal para o datatable de rateios do item
 *  @author {Jean Varlet}
 * @param {int} - posicao
 * @param {string} - unidade 
 * @param {string} - projeto
 * @param {string} - percentual
 * @param {string} - valor
 * @param {string} - codUnidade
 */
function addRowRateioTbl(posicao, unidade, projeto, acao, percentual, valor, codUnidade){
    console.log('addRowRateioTbl()-> posicao ->' +posicao+  ' unidade-> '+unidade + ' projeto-> '+ projeto + ' acao-> '+acao)
	if(percentual == 0 || percentual == '0'){
		percentual = '100';
		$("#addProjAcao___"+posicao.split('___')[0]).prop('disabled', true);
	}
	
	$("#bodyRateiostbl___"+posicao.split('___')[0]).append(''+
        '<tr id="trRateiotbl___'+posicao+'">'+
        '    <td>'+
        '      <i class="fluigicon fluigicon-trash icon-sm btn btn-danger delRateiotbl" style="cursor:pointer;" id="delRateiotbl___'+posicao+'" ></i>'+
        '    </td>'+
        '    <td class="unidadeTbl dataTableRateios">'+
                unidade+
        '    </td>'+
        '    <td class="projetoTbl dataTableRateios">'+
                projeto+
        '    </td>'+
        '    <td class="acaoTbl dataTableRateios">'+
                acao+
        '    </td>'+
        '    <td class="percentualTbl dataTableRateios" id="percentualTbl___'+posicao+'">'+
                percentual+
        '    </td>'+
        '    <td class="valItemTblRat dataTableRateios">'+
                valor+
        '    </td>'+
		'    <td class="codUnidadeTbl dataTableRateios" style="display:none;">'+
				codUnidade+
        '    </td>'+
        '</tr>'
	)
	
	if(percentual == '100'){
		$("#addProjAcao___"+posicao.split('___')[0]).prop('disabled', true);
	}
	
}
/** Jean Varlet - 16/09/2020 - Avalia a chamada para adicionar novos filhos para inserção de novos rateios no modal.
 * @author {Jean Varlet}
	UTILIZA -> checkRateio() -> para checar o preenchimento do item de rateio atual.
	UTILIZA -> checkTotalRateioAdd() -> para avaliar o valor total de rateios já adicionados.
	UTILIZA -> modalFilhosRateios() -> após a checagem atrav;es de checkRateio() e checkTotalRateioAdd 
			   adiciona um novo filho para adição de um novo registro de rateio.
	 */
function callModalFilhosRat(){
	let indexer = $(".itemRat:last").attr("id");
	let next = parseInt(indexer.split('___')[2]) + 1;
	let custoInformado = 'nao';
	let custo = 0;
	let quantidade = 0;
	let faltante = 0;
	if(checkRateio($(".itemRat:last").attr("id").split('___')[1],$(".itemRat:last").attr("id").split('___')[2] )){//Checa o preenchimento
		if($(".valNecess:last").val() != '' && $(".valNecess:last").val() != 'R$ 0,00'){//Avaliação de custo - Se informado ---
			custoInformado = 'sim';
			if(floatReais($(".valNecess:last").val()) > floatReais(checkTotalRateioAdd())){//Compara o valor necessário com o valor já selecionado
				
				custo = checkTotalRateioAdd();
				faltante = reais(floatReais($(".valNecess:last").val()) - floatReais(checkTotalRateioAdd())) ;	
				modalFilhosRateios(indexer.split('___')[1], next, custoInformado, custo,  faltante);	
					
			}else if(floatReais($(".valNecess:last").val()) == floatReais(checkTotalRateioAdd())){//Valor necessário atingido.
				
				modalFilhosRateios(indexer.split('___')[1], next, custoInformado, custo,  'R$ 0,00');
					
			}else if(floatReais($(".valNecess:last").val()) < floatReais(checkTotalRateioAdd())){ // Valor ultrapassado
			
				toast('Atenção!', 'O valor total dos rateios não pode ser maior que o valor necessário.', 'warning');	
			}
		}else{
			
			modalFilhosRateios(indexer.split('___')[1], next, custoInformado, custo,  faltante);
				
		}
	}

}


/** 16/09/2020 - Percorre os valores de estimativa de custo dos items de compra
	adicionados e retorna o valor total para o campo de estimativa de custo.
	@author {Jean Varlet}
	
 */
function calcEstCustoTotal(){
    let total = 0;
    $(".valorNecessario").each(function(){
        let id = $(this).attr("id");
        if($("#"+id).val() != ''){
            total = total + floatReais($("#"+id).val());    
        }
    })
   
    $("#valorEstimativaGer").val(reais(total));
   
}
/** @function modalFilhosRateios() - Adiciona campos para inserção de registro de rateio
*   @author {Jean Varlet}
* 	@param {int} - indexer
* 	@param {int} - itemRat
* 	@param {boolean} - custoInformado sim/nao
* 	@param {varchar} - custo/valor/preço do item - formato R$ 0,00 
*/
function modalFilhosRateios(indexer, itemRat, custoInformado, custo,  faltante){
	let appendar = 'nao';
	let posicao = indexer+"___"+itemRat;
	let necessario = 0;
	if(custoInformado == 'sim'){
		if(faltante == 'R$ 0,00'){
			$("#btnAddRat").prop("disabled", true);
			appendar = 'nao';
			toast('Sucesso', 'Os Rateios foram inseridos com sucesso.', 'success');
		}else{
			appendar = 'sim';		
		}
	}else{// Custo não informado - Considerar apenas o rateio 
		appendar = 'sim';
		
	}
	
	if(appendar == 'sim'){
		$('.modal-body').css("max-height", window.innerHeight / 1.7 + 'px');
		$("#dvRateios").append(''+
		'<div class="itemRat" id="itemRat___'+indexer+'___'+itemRat+'">'+
		  
		'<h3 style:"color:blue;">Item de Rateio: '+itemRat+' </h3>'+
		'<hr>'+
			
	      '<div class="col-md-1 col-sm-12 " >'+
	            '<i class="fluigicon fluigicon-trash icon-md btn btn-danger delRateio" style="cursor:pointer; margin-top:120px;" id="delRateio___'+indexer+'___'+itemRat+'" onclick="delItemRateio(this);"></i>'+
	      '</div><!--lixeira-->'+
	      '<div class="col-md-1 col-sm-12 " >'+
	            '		<i class="fluigicon fluigicon-pencil icon-sm btn btn-warning editRateiotbl" style="cursor:pointer;margin-top:120px;display:none; " id="editRateio___'+indexer+'___'+itemRat+'"></i>'+
	      '</div><!--lixeira-->'+
	      '<div class="col-md-10 ">'+  
	        '<div class="row">'+
	            '<div class="col-md-6 col-sm-12 form-group">'+
	                '<h3>Unidade</h3>'+
	                '<input type="text" name="unidadeRateio" id="unidadeRateio___'+indexer+'___'+itemRat+'" class="form-control unidadeRateio itemRateioModal">'+
	            '</div>'+
	            '<div class="col-md-6 col-sm-12 form-group">'+
	                '<h3>Projeto</h3>'+
	                '<input type="text" name="projetoRateio" id="projetoRateio___'+indexer+'___'+itemRat+'" class="form-control projetoRateio itemRateioModal">'+
	            '</div>'+
	        '</div><!--row-->'+
	        '<div class="row">'+
	            '<div class="col-md-6 col-sm-12 form-group">'+
	                '<h3>Ação</h3>'+
	                '<input type="text" name="acaoRateio" id="acaoRateio___'+indexer+'___'+itemRat+'" class="form-control acaoRateio itemRateioModal">'+
	            '</div>'+
	            '<div class="col-md-6 col-sm-12 form-group">'+
	                '<h3>Saldo da Ação</h3>'+
	                '<input type="text" name="saldoAcaoRateio" id="saldoAcaoRateio___'+indexer+'___'+itemRat+'" class="form-control itemRateioModal" readonly value="R$ 0,00">'+
	            '</div>'+
	        '</div><!--row-->'+
	        '<div class="row">'+
				'<div class="col-md-3  form-group">'+
					'<h3>COD UNIDADE</h3>'+
	                '<input type="text" name="codUnidade" id="codUnidade___'+indexer+'___'+itemRat+'" class="form-control codUnidade itemRateioModal" readonly>'+
				'</div>'+
				'<div class="col-md-3  form-group">'+
					'<h3>Necessário</h3>'+
	                '<input type="text" name="valNecess" id="valNecess___'+indexer+'___'+itemRat+'" class="form-control valNecess itemRateioModal" readonly>'+
				'</div>'+
	            '<div class="col-md-3 col-sm-12 form-group">'+
	                '<h3>Percentual Rateio</h3>'+
	                '<input type="number" name="percRateio" id="percRateio___'+indexer+'___'+itemRat+'" class="form-control percRateio itemRateioModal" min="0" max="100" value="0">'+
	            '</div>'+
	            '<div class="col-md-3 col-sm-12 form-group">'+
	                '<h3>Valor Rateio</h3>'+
	                '<input type="text" name="valRateio" id="valRateio___'+indexer+'___'+itemRat+'" class="form-control valRateio itemRateioModal" value="R$ 0,00">'+
	            '</div>'+
	        '</div><!--row-->'+
	       
	     '</div>'+
			'<hr>'+
		'</div><!--itemRat___-->'
		);	
		
	}
	
	if(custoInformado == 'sim'){
		if(itemRat == 1){
			$("#valNecess___"+indexer+'___'+itemRat).val(custo);	
		}else if(itemRat > 1 ){
			var _itemRatAnt = parseInt(itemRat) - 1 ;
			$("#valNecess___"+indexer+'___'+itemRat).val($("#valNecess___"+indexer+'___'+_itemRatAnt).val());
		}
		else{
			if($('.valNecess:last').val() != ''){
				$("#valNecess___"+indexer+'___'+itemRat).val($('.valNecess:last').val());	
			}
				
		}
		
	}else{
		checkRatPercent();

	}
	
	$(".percRateio").click(function(){
		let _itemRat = $(this).attr("id").split('___')[2];
		let _indexer = $(this).attr("id").split('___')[1];
		atualizaNecessario(_indexer, _itemRat, custoInformado, $('.valNecess:last').val());
		
	})
	$(".percRateio").blur(function(){
		let _itemRat = $(this).attr("id").split('___')[2];
		let _indexer = $(this).attr("id").split('___')[1];
		atualizaNecessario(_indexer, _itemRat, custoInformado, $('.valNecess:last').val());
		
	})
	$(".valRateio").click(function(){
		$(this).val('');
	})
	$(".valRateio").blur(function(){
		if($(this).val() != '' && $(this).val() != 'R$ 0,00'){
			$(this).val(reais($(this).val()));
		}
		setTimeout(function(){
			fnAddValFinRat();	
		},200)
	
	
	})
	fnAutoComplete(posicao);
	
	
	return true;
	
}

/** 16/09/2020 - Remove item de rateio ao clicar no BTN lixeira
	@param {object} - Item/Linha de rateio -
	@author {Jean Varlet}
	
 */
function delItemRateio(el){
    let pos = el.id.split('delRateio___')['1'];
    $("#itemRat___"+pos).remove();
}

/** 16/09/2020 - Ajusta valor inserido para rateios para 
	adequar escala de percentual para apenas 2 dígitos-
	@param {varchar} - inserido - > valor inserido pelo usuário
	@param {varchar} - necessario - > valor total necessário para o item de compra.
	@author {Jean Varlet}

 */
function fnAddValFinRat(){
	let _valNecess = $(".valNecess:last").val();		
	let _valInsert = $(".valRateio:last").val();
	let _valPercent = floatReais($(".valNecess:last").val()) / 100;
	let _percentValorIns = floatReais($(".valRateio:last").val()) / _valPercent;
    $(".percRateio:last").val(_percentValorIns.toFixed(0));
    setTimeout(function(){
        $(".percRateio:last").click()    
    },200)
    
    
	
}

/**
			----- 	INICIALIZA CAMPOS DO TIPO SELECT FLUIG  ----- 

 */


/** @function fnAutoComplete() - Listagem de Unidades no modal de seleção de rateios
 *  @author {Jean Varlet}
 *  @param {int} - posição do elemento
 */
function fnAutoComplete(posicao){
	var autoCompleteProj, autocompleteAcao;
	var autoCompletUnd = FLUIGC.autocomplete('#unidadeRateio___'+posicao, {
	    highlight: true,
	    minLength: 0,
	    hint: true,
	    searchTimeout: 100,
	    type: 'tagAutocomplete',
	    name: 'dsUnidade',
	    tagClass: 'tag-warning',
	    maxTags: 1,
	    allowDuplicates: false,
	    displayKey: 'UNIDADE',
	    source: {
	        url: "/api/public/ecm/dataset/search?datasetId=dsUnidade&searchField=UNIDADE&",
	        limit: 10,
	        offset: 100,
	        limitKey: 'limit',
	        patternKey: 'searchValue',
	        root: 'content'
	    },
	    onMaxTags: function(item, tag) {
	       // exibirMensagem('Só é permitido selecionar uma única Condições de Pagamento.', 'warning');
	    },
	    tagMaxWidth: 220
	}, function(err, data) {
	    // something with err or data.
	    if(err) {
	        console.log('Erro ao listar unidades');
	    }else{
			console.log('Listou Unidades');
		}
	}); 
	autoCompletUnd.on('fluig.autocomplete.itemAdded', function(result){
		//console.log('CodUnidade -> '+ result.item.CODUND );
		carregaProj(result.item.CODUND, posicao );
	}).on('fluig.autocomplete.itemRemoved', function(result){
		if (autoCompleteProj){
			autoCompleteProj.destroy();	
			$('#projetoRateio___'+posicao).val('')
		}		
	});
	function carregaProj(Und, posicao){
		console.log('Inside -> carregaProj()-> Und-> '+ Und);
		let urlRest = "/api/public/ecm/dataset/search?datasetId=dsProjeto&searchField=PROJETO&";
		let urlUnd = 'likeField=CODUND&likeValue=';		
		if (Und !== ''){
			urlRest = urlRest + urlUnd + Und + '&'
		}
	    autoCompleteProj = FLUIGC.autocomplete('#projetoRateio___'+posicao, {
			highlight: true,
			minLength: 0,
			hint: true,
			searchTimeout: 100,
			type: 'tagAutocomplete',
			name: 'dsProjeto',
			tagClass: 'tag-warning',
			maxTags: 1,
			allowDuplicates: false,
			displayKey: 'PROJETO',
			source: {
				url: urlRest,
				limit: 10,
				offset: 0,
				limitKey: 'limit',
				patternKey: 'searchValue',
				root: 'content'
			},
			onMaxTags: function(item, tag) {
			   
			},
			tagMaxWidth: 220
		}, function(err, data) {
			
			if(err) {
				console.log('Erro ao retornoar Projeto')
			}else{
				console.log('Projeto retornado')
			}
		});
		autoCompleteProj.on('fluig.autocomplete.itemAdded', function (result){
			console.log(result.item.CODPROJETO)
			carregaAcao(result.item.CODPROJETO, result.item.CODUND, posicao)
		}).on('fluig.autocomplete.itemRemoved', function (result){
			if(autocompleteAcao) {
				autocompleteAcao.destroy();
			}
			
			$('#modalRatProjetoCod').val('')
			$('#modalRatAcaoCod').val('')
		})
	}// fim -> carregaProj()
	function carregaAcao(Proj, Und, posicao){
		let urlRest = "/api/public/ecm/dataset/search?datasetId=dsAcao&searchField=ACAO&";
		let urlUnd = 'likeField=CODUND&likeValue=';
		let urlProj = 'likeField=CODPROJETO&likeValue=';
		
		if (Proj !== ''){
			urlRest = urlRest + urlUnd + Und + '&' + urlProj + Proj + '&'
		}
		
		autocompleteAcao = FLUIGC.autocomplete('#acaoRateio___'+posicao, {
			source: {
				url: urlRest,
				patternKey: "searchValue",
				limit: 100,
				root: "content"
			},
			highlight: true,
			displayKey: 'ACAO',
			tagClass: 'tag-warning',
			type: 'tagAutocomplete',
			minLength: 0,
			searchTimeout: 100,
			maxTags: 1,
			tagMaxWidth: 500
		}, function (err, data){
			if (err) {
				
			} else {
				
			}
		})
		
		autocompleteAcao.on('fluig.autocomplete.itemAdded', function (result){
			console.log(result.item.CODUNIDADE)
			$("#codUnidade___"+posicao).val(result.item.CODUNIDADE);
			fAnalisaSaldoOrcamento(posicao, result.item.CODUNIDADE, parseInt($("input", $("#dataEntrega")).val().split('/')[1]), parseInt($("input", $("#dataEntrega")).val().split('/')[2]))
		}).on('fluig.autocomplete.itemRemoved', function (result){
			$('#modalRatAcaoCod').val('')
			$('#modalSaldoAcao').val('0,00')
		})
	}
	
}
/** @function fAnalisaSaldoOrcamento() - consulta o saldo de orçamento disponível para a unidade, projeto e ação
 * selecionados no modal de inserção de rateios. 
 * @author {Marcos Silva}
 * @param {string} posicao 
 * @param {string} cc 
 * @param {string} mes 
 * @param {string} ano 
 */
function fAnalisaSaldoOrcamento (posicao,cc,mes,ano){
	console.log('fnAnalisaSaldoOrcamento() -> posicao -> '+posicao)
	if (cc != "") {

		let c1 = DatasetFactory.createConstraint("CODCCUSTO", cc, cc, ConstraintType.MUST);
		let c2 = DatasetFactory.createConstraint("ANO", ano, ano, ConstraintType.MUST);
		let c3 = DatasetFactory.createConstraint("MES", mes, mes, ConstraintType.MUST);
		let constraints   = new Array(c1,c2,c3);
		let dataset         = DatasetFactory.getDataset("dsSaldoOrcamento", null, constraints, null);
		let row             = dataset.values[0];

		if (row == null){ 
			var erro = "Não existe informação de orçamento para o Centro de Custo (Projeto/Ação) " + cc + "!";
			//alert (erro);
			toast('Erro!', erro, 'danger');
		}
		else {
			var vlrsaldo = row["SALDODISPONIVEL"];
			vlrsaldo = parseFloat( vlrsaldo );
			$("#saldoAcaoRateio___"+posicao).val(reais(vlrsaldo));
		}	
	}
}

// Etapa 5 - parse Rateios ---
function parseItensRateios(){
    let _obj;
    if($("#conteudoRateios").val() != ''){
        _obj = JSON.parse($("#conteudoRateios").val());
        if(_obj.length){
            for(var i = 0; i < _obj.length; i++){
				var posicao = i +1;
				console.log('parseItensRateios()->posicao -> '+ posicao)
                if($("tr",$("#bodyRateiostbl___"+posicao)).length){
                    $("tr",$("#bodyRateiostbl___"+posicao)).remove();    
				}
				$("#addProjAcao___"+posicao).prop("disabled", true);
                console.log('produto ->'+ _obj[i].produto);
                if(_obj[i].rateios.length){
                    for(var r = 0; r < _obj[i].rateios.length; r++ ){
                        console.log('rateios  ->'+ _obj[i].rateios[r].unidade);
                        appendRateios(_obj[i].rateios[r].produto, _obj[i].rateios[r].unidade, _obj[i].rateios[r].projeto, _obj[i].rateios[r].acao, _obj[i].rateios[r].percentual, _obj[i].rateios[r].valor );    
                    }
                }
            }
        }
    }

}//parseItensRateios()

function appendRateios(indexer, unidade,projeto, acao, percentual, valor){
    console.log('appendRateios()->indexer->'+indexer)
    $("#bodyRateiostbl___"+indexer).append('<tr>'+
    '    <td></td>'+
    '    <td>'+unidade+'</td>'+    
    '    <td>'+projeto+'</td>'+
    '    <td>'+acao+'</td>'+ 
    '    <td>'+percentual+'</td>'+ 
    '    <td>'+valor+'</td>'+  
    '</tr>'    );    
}



/** @function checkRatPercent() -- Soma o percentual total selecionado de centros de custos e retorna - 100% para o novo
 * campo
 * @author {Jean Varlet} 
 */
function checkRatPercent(){
    let _total = 0;
    $(".percRateio").each(function(){
        let _id = $(this).attr("id");
   
        if($("#"+_id).val() <= 100){
            console.log('valor-> '+ $("#"+_id).val())
            _total += parseInt($(this).val()); 
        }
    });
   var newVal =  100 -  _total;
   $(".percRateio:last").val(newVal);   
   $(".valNecess:last").val('R$ 0,00');   
}