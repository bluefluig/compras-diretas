/** Jean Varlet - Arquivo responsável pela criação de eventos e vinculação a solicitação */

/**@function openEventoModal() - carrega um modal Fluig para inserção de novos eventos para 
 * posterior vinculação.
 * @author{Jean Varlet}
 */
function openEventoModal(){
	var tpl = $('.tpl-cadastra-evento').html();
	var html = Mustache.render(tpl, null);
	
	modal = FLUIGC.modal({
		title: 'Cadastrar Eventos',
		content: html,
		id: 'fluig-modal',
		size: 'full',
		actions: [{
			'label': 'Cadastrar',
			'classType' : 'btn-primary save-tipo',
			'autoClose': true
		},{
			'label': 'Fechar',
			'autoClose': true
		}]
	}, function(err, data) {
        setTimeout(function(){
            idCadEvento();
        },500)
		$(".save-tipo").click(function(){
			saveEvento();
		});
	});
}

/**@function saveEvento() - Realiza a escrita do evento a ser gravado no modal da function openEventoModal()
 * @author {Jean Varlet}
 */
function saveEvento(){
	var parentId = getDatasetTipoId();
	var titulo = $("#tituloEventoModal").val();
	if(parentId == null || parentId == undefined || parentId == ""){
		FLUIGC.toast({
			title: '',
			message: "O cadastro do Tipo não foi configurado no Fluig. Contate o administrador do sistema.",
			type: 'danger'
		});
		return false;
	}
	
	parent.WCMAPI.Create({
	url: '/api/public/2.0/cards/create',
	data:
		JSON.stringify({
			"documentDescription": titulo,
			"parentDocumentId": parentId,
			"version": 1000,
			"formData": [
				{
					"name": "codigoEvento",
					"value": $("#codEventoModal").val()
					},
				{
					"name": "tituloEvento",
					"value": $("#tituloEventoModal").val()
				},
				{
					"name": "ativo",
					"value": $("#ativoTipoModal").val()
				},
				
				{
					"name": "cadastrante",
					"value": $("#colabCadEvento").val()
				},
				
				{
					"name": "dataCadastro",
					"value": $("#dataCadEvento").val()
				},
				
				{
					"name": "pegahora",
					"value": $("#horaCadEvento").val()
				}
				
			]
		}),
	success: function (data, status, xhr) {
		FLUIGC.toast({
	        title: '',
	        message: "O tipo foi cadastrada.",
	        type: 'success'
		});
	},
	error: function(xhr, status, error) {
		FLUIGC.toast({
			title: '',
			message: "Ocorreu um erro ao cadastrar o Tipo. Se o problema persistir, cadastre o Evento  pelo GED.",
			type: 'danger'
		});
	}
});
}

function getDatasetTipoId(){
	var DATASET = "ds_cadastro_eventos";
	var datasetName = DatasetFactory.createConstraint("datasetName", DATASET, DATASET, ConstraintType.MUST);
	var document = DatasetFactory.getDataset("document", null, [datasetName], null);
	return document.values[0]["documentPK.documentId"];
}

function idCadEvento(){
    $("#codEventoModal").prop("readonly", true);
    let _retorno = DatasetFactory.getDataset('ds_cadastro_eventos', null, null, null);
    let lastId = 0;
    if(!jQuery.isEmptyObject(_retorno)){
        for(var i = 0; i < _retorno.values.length; i++){
            lastId = _retorno.values[i].codigoEvento; 
            console.log('lastId-> '+ lastId)   
        }
        
            lastId = parseInt(lastId) + 1;
        
        $("#codEventoModal").val(lastId);
    }
}