/** @function retGerDir(matricula) - Retorna as matrículas de Gerente e Diretor do solicitante Responsável
 *  @author {Jean Varlet} 
 *  @param {int} - matrícula fluig
 */
function retGerDir(matricula){
    matricula = toRM(matricula);
    let cst1 = DatasetFactory.createConstraint('CHAPA', matricula, matricula, ConstraintType.MUST);
    let retorno = DatasetFactory.getDataset('ds_retornaPessoa', null, [cst1], null);
    if(!jQuery.isEmptyObject(retorno)){
        if(retorno.values.length){
            $("#gerenteUnd").val(retorno.values[0].GERENTE);
            $("#diretorUnd").val(retorno.values[0].DIRETOR);
            $("#nomediretorUnd").val(retorno.values[0].DiretorNome);
        }
    }
}
/** Avalia se o solicitante possui cargo de analista ou superior. 
*   Caso não, exibi div para seleção do analista responsável pela
*   solicitação.
* @params {int} - matricula colab logado
*/
function checkCargoSol(matricula){
    // "ASSIST-I", "ASSIST-II", "0179"-Assist1, "0165"-Trainee, "104" - Estagiario, 0101-Assistente
    let _resultado = false;
    matricula = toRM(matricula);
    let arrayAssist = [ "101", "ASSIST-I", "ASSIST-II","0101",  "0179", "0165", "104", "0182"];
    let cst1 = DatasetFactory.createConstraint('CHAPA', matricula, matricula, ConstraintType.MUST);
    let retorno = DatasetFactory.getDataset('ds_cargos_compras_diretas', null, [cst1], null);
    console.log('checkCargoSol() -> cargo->'+ retorno.values[0].CODFUNCAO);
    if(!jQuery.isEmptyObject(retorno)){
        if(retorno.values.length){
            
            for(let i = 0; i < arrayAssist.length; i++){
                if(arrayAssist[i].toString().trim() ==  retorno.values[0].CODFUNCAO.toString().trim()){  
                    _resultado = true;                
                }
            }// End For
            
            if(_resultado){
                console.log('condicional ->  encontrado !')  
                $("#itensSolicitacao").fadeIn();
                $("#solResponsavel").val('sim');
                retCodSecao(matricula, 0);
            }else{
                console.log('condicional -> não encontrado !')  
                toast('Atenção!', 'Você deve selecionar um analista responsável para essa solcitação.', 'warning');
                $("#selecionaAnalista").fadeIn();
                $("#dataEntrega").fadeOut();
                $("#itensSolicitacao").fadeOut();
                $("#solResponsavel").val('nao');
                retCodSecao(matricula, 1);  
            }
                
        }else{
			 toast('Atenção!', 'Você deve selecionar um analista responsável para essa solcitação.', 'warning');
             $("#selecionaAnalista").fadeIn();
		     $("#dataEntrega").fadeOut();
		     $("#itensSolicitacao").fadeOut();
             $("#solResponsavel").val('nao');
	
		}
    }else{
		toast('Erro!', 'O usuário não possui cadastro dentro do TOTVS RM!', 'danger');
    } 
}
//unidadeSolicitante
/**Retorna o CODSECAO do usuário logado e chama modal para selecao do analista responsável
@param {varchar} matricula
@param {int} 0 -> Analista ou superior / 1 - Assistente ou similares

 */
function retCodSecao(matricula, cargo){
    matricula = toRM(matricula);
    let cst1 = DatasetFactory.createConstraint('CHAPA', matricula, matricula, ConstraintType.MUST);
    let retorno = DatasetFactory.getDataset('ds_retornaPessoa', null, [cst1], null);
    if(!jQuery.isEmptyObject(retorno)){
        if(retorno.values.length){
			if(cargo == 1){
				//modalSelAnalista(retorno.values[0].CODSECAO);
				$("#codSecao").val(retorno.values[0].CODSECAO);	
			}else if(cargo == 0){//Analista ou superior
				$("#codSecao").val(retorno.values[0].CODSECAO);
				$("#checkCargoSol").val(retorno.values[0].Unidade);
				$("#unidadeSolicitante").val(retorno.values[0].Unidade);
			}            
            
        }else{
            toast('ERRO!', 'O Solicitante não possui cadastro no RM! Selecione o Analista responsável manualmente', 'warning');
            modalSelAnalista('');
        }
    }else{
		toast('Erro!', 'Colaborador não encontrado na base de dados do TOTVS RM.', 'danger');
	}
}

// Sem uso --
function modalSelAnalista(CODSECAO){
	let campoCodSecao, valorCodSecao;
	if(CODSECAO != ''){
		campoCodSecao = 'CODSECAO';
		valorCodSecao = CODSECAO;
	}else{
		campoCodSecao = '';
		valorCodSecao = '';
	}
	
    var param  = {"datasetId" : "ds_retornaPessoa", "limit" : "0", 
                            "filterFields" : [campoCodSecao, valorCodSecao], 
                            "searchField" : "NOME", "searchValue" : '' };        
    var thisModal = FLUIGC.modal({
       title: 'Lista de Grupos',
       content: '<div id="postEmb"></div>',
       id: 'fluig-modal',
       actions: [{
           'label': 'Fechar',
           'autoClose': true
       }]
    }, function(err, data) {            
        var thisTable = FLUIGC.datatable('#postEmb', {
            dataRequest: {
                url: '/api/public/ecm/dataset/search',
                options: {
                    contentType:"application/json",
                    dataType: 'json',
                    method: 'POST',
                    data: JSON.stringify(param),
                    crossDomain: true,
                    cache: false
                },
                root: 'content'
            },
            renderContent: ['Unidade', 'chapa', 'NOME'], 
            header: [{'title': 'Unidade', 'size': 'col-sm-4'},
                     {'title': 'Chapa', 'size': 'col-sm-2'},
                     {'title': 'Nome', 'size': 'col-sm-6'}
                    ],
            multiSelect: false,
            search: {
                enabled: true,
                searchAreaStyle: 'col-md-9',
                onSearch: function(response) {

                }
            },
            scroll: {
                target: '#postEmb',
                enabled: true                    
           },
            tableStyle: 'table-striped'
        }).on('dblclick', function(ev) {
            var index = thisTable.selectedRows()[0];
            var selected = thisTable.getRow(index);
            $("#matriculaResponsavel").val(selected.chapa);
            $("#nomeResponsavel").val(selected.NOME); 
            $("#nomeResponsavelSol").val(selected.NOME); 
            $("#itensSolicitacao").fadeIn();   
            thisModal.remove();                        
        });
    });
    $(".modal-body").css("max-height" , window.innerHeight/2 + 'px');

}
/**Retorna o nome de Exibição do usuário logado na plataforma 
@param {null}
@returns {varchar}
*/
function nomeExibicaoColab(){
    let name;
	let request_data = {
	       url: parent.WCMAPI.serverURL+'/api/public/2.0/users/getCurrent',      
	        method: 'GET',
		}; 
	    $.ajax({
			url: request_data.url,
			crossDomain: true,
			type: 'GET',
			async : false,
			contentType: "application/json",		
			dataType : 'application/json', 
			//data :({limit:'100'}),
			success: function(data){
                   console.log(data);
                    console.log('success')
                   name = data.content.fullName;
                   return name;
                       
            },
			error: function(data) {
				var obj = JSON.parse(data.responseText)
                name = obj.content.fullName;

			}
		});
 	return name;
	   
}

