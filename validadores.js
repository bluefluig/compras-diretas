/**Avalia se valor informado está dentro dos limites do valor necessário
* @author{Jean Varlet} 
* @param {int} - indexer 
*/
function avalValNecessRats(indexer){
    let _necessario = floatReais($("#valorNecessario___"+indexer).val());
    let _total = 0;
    $(".valRateio:visible").each(function(){
        if(floatReais($(this).val()) > 0){
            _total += floatReais($(this).val());
        }    
    })
    if(_necessario == _total){
        console.log('Bateu valor total')
    }
    if(_necessario > _total){
        console.log('Adicionar novo input');
    }
    if(_necessario < _total){
        console.log('Ultrapassou valor necessario - corrigir last input');
    }

}//avalValNecessRats(1)

/** 16/09/2020 - Auxiliar de callModalFilhosRat()
*	Checa o preenchimento dos campos do filho de rateio atual antes de adicionar um novo
*	@author{Jean Varlet}
*	@param {int} -> indexer -> posição do item de compra no formulário
*	@param {int} -> itemRat -> posição do item de rateio no modal
*/
function checkRateio(indexer, itemRat){
	let _retorno = true;
    $(".itemRateioModal",$("#itemRat___"+indexer+"___"+itemRat)).each(function(){
        let id = $(this).attr("id");
		
        if($("#"+id).val() == ''){
            $("#"+id).parent().addClass('has-error');
            toast('Erro!', 'O campo: <b>'+ $("#"+id).parent().text() + '</b> do item de rateio: <b>'+ itemRat+'</b> necessita ser preenchido.', 'danger');
            _retorno = false;
        }else{
            $("#"+id).parent().removeClass('has-error');
            $("#"+id).prop("readonly", true);//checar se é o melhor momento para bloquear---- 
			 
        }
    })
	if(_retorno){
		$("#editRateio___"+indexer+"___"+itemRat).show();
		$("#editItem___"+indexer).show();
		$("#editItem___"+indexer).click(function(){
			$("#produtoRm___"+indexer).prop("readonly", false);	
			$("#especificacao___"+indexer).prop("readonly", false);	
			$("#valCustoAproxItem___"+indexer).prop("readonly", false);	
			$("#quantidade___"+indexer).prop("readonly", false);
			FLUIGC.switcher.disable("#custoAproxItem___"+indexer);	
		});
		
	}
	return _retorno;
}
/** 16/09/2020 - Auxiliar de callModalFilhosRat()
*	Percorrer os filhos de rateios adicionados no modal e retorna a soma
*	@author{Jean Varlet }
 */
function checkTotalRateioAdd(){
    let total = 0;
    $(".valRateio").each(function(){
        
        if($(this).val() != ''){
            total += floatReais($(this).val());    
        }

        
    })
    return reais(total);

}//checkTotalRateioAdd()

/** 16/09/2020 - Valida todas as regras antes de realizar o envio da solicitação.
*	Acionada pelo botão enviar para todas as etapas do processo -
*	@author{Jean Varlet}
*	@param{int} - etapa 
*	@todo - Adicionar param para avaliar etapa atual e realizar somente checagens pertinentes a 
*	etapa.
*/
function validateAll(etapa){
	$("#enviarbtn").prop("disabled", true);
	if(etapa == 0 || etapa == 4 || etapa == 12){
		if(saveProdutos()){
			if($("#informouValPrevisto").val() == 'sim' && $("#informouOrcPrevisto").val() == 'sim'){
				if($("#valorEstimativaGer").val() != ''){
					if($("#valorOrcamentoGer").val() != ''){
						if(floatReais($("#valorEstimativaGer").val()) > floatReais($("#valorOrcamentoGer").val())){
							toast('Atenção!', 'O Orçamento informado é menor que a estimativa de custo', 'warning');
							$("#enviarbtn").prop("disabled",false);
							return false;	
						}else{
							toast('Atenção!', 'O orçamento disponível não pode ser menor que a estimativa de custo', 'warning');
							$("#enviarbtn").prop("disabled",false);
							return true;
						}
					}else{
						toast('Atenção', 'Você selecionou informar o orçamento disponível para essa solicitação, mas não informou o valor.', 'warning');
						$("#enviarbtn").prop("disabled",false);
						return false;	
					}
				}else{
					toast('Atenção', 'Você selecionou informar uma estimativa geral de custo, porém não informou o valor.', 'warning');
					$("#enviarbtn").prop("disabled",false);
					return false;
				}	
			}else{
				return true;
			}
				
		}else{
			$("#enviarbtn").prop("disabled",false);
			return false;
				
		}
		if(fnValidateProjAction()){

		}else{
			$("#enviarbtn").prop("disabled",false);	
		}
	}
	if(etapa == 5){// Assistente UABS --
		//valInputCotacoes();
		fnAvaliaOrcGer();
		//if(parseItensFinal()){
		//	console.log('Validou parseItensFinal()')
			return true;	
		//}else{
		//	$("#enviarbtn").prop("disabled", false);
		//	return false;	
		//}
	
		
	}
	if(etapa == 18){
		return true;
	}
	if(etapa == 25){
		if($("#decisaoAnalUABS").val() == 'ajustar'){
			if($("#justJustifUABS").val() == ''){
				$("#enviarbtn").prop("disabled", false);
				return false;
			}
		}
		else if($("#decisaoAnalUABS").val() == 'cancelar'){
			if($("#justCancelUABS").val() == ''){
				$("#enviarbtn").prop("disabled", false);
				return false;
			}
		}else{
			$("#enviarbtn").prop("disabled", false);
			return true;
		}
	}
	if(etapa == 32){
		//parseItensFinal()
			return true;		
		
		
	}
	if(etapa == 38){
		
	}

	setTimeout(function(){
		$("#enviarbtn").prop("disabled", false);
	},2000)
}

/** 21/09/2020 - Percorrer os itens de compra da solicitação checando se há 
 * Projetos e ações adicionados.
 * @author{Jean Varlet}
 */
function fnValidateProjAction(){
    let counter = 0;
	if($(".item:visible").length){//Checa se pelo menos 1 filho foi adicionado (1 item de compra)
		for(let i = 0; i < $(".item:visible").length; i++){//Percorre todos os itens de compra
			var pos = i + 1;
			if($("tr", $("#bodyRateiostbl___"+pos)).length){
				console.log('O item '+pos + ' possui rateios.')
                
			}else{
				//Caso não encontre item de rateio para o item de compra atual->
				console.log('O item '+pos + ' não possui rateios.')
				$(".item:eq("+pos+")").addClass('danger');
                counter += 1;
			}

		}

	}else{
		toast('Atenção!', 'Para iniciar uma solicitação é necessário adicionar pelo menos 1 item de compra', 'warning');
		counter += 1;
	}
    if(counter > 0){
        return false;
    }else{
        return true;
    }
} //fnValidateProjAction()
/** 24/09/2020 - Avalia se alguma das cotações inseridas possui um valor maior
 * que o valor esperado. Quando o valor esperado é informado no início da sol.
 * @author {Jean Varlet}
 * 
 */
function fnAvaliaOrcGer(){
    let valorOrcamento, saida ;
    if($("#orcamentodisp").is(":checked") && $("#inputCotacoes").val() == 'sim'){
        saida = 'nao';
        valorOrcamento = floatReais($("#valorOrcamentoGer").val());
        $(".valTotCot").each(function(){
            if(floatReais($(this).val()) > valorOrcamento){
                saida = 'sim';        
            
            } 
        })
    }else{
        saida = 'nao';
    }
    $("#solAprovGerenteValor").val(saida)//Saída decisão
}
/** 24/09/2020 - Valida se existe algum item de cotação no DOM 
 * @author{Jean Varlet}
 */
function valInputCotacoes(){
	if($(".entradaCotacao").length){
		$("#inputCotacoes").val('sim');
	}else{
		$("#inputCotacoes").val('nao');
	}
	
}
/** @function valEmailCotacoes() - Checa se todos os fornecedores possuem e-mail preenchido
 * @author {Jean Varlet}
 */
function valEmailCotacoes(){
	let _retorno = true;
	$(".emailForn").each(function(){
		let _id = $(this).attr("id");
		if($("#"+_id).val() == '' || $("#"+_id).val() == "Email Não cadastrado"){
			$("#"+_id).parent().addClass('has-error');
			_retorno = false;
		}else{
			$("#"+_id).parent().removeClass('has-error');
		}
	})
	return _retorno;
}

/** 24/09/2020 - Avalia data selecionada na inserção de uma cotação em
 * relação a data prevista no início da solicitação.
 * @author {Jean Varlet}
 * @param {Date} - dataProposta -> Data selecionada na proposta
 * @param {Date} - dataEntrega -> Data informada no início da solicitação
 */
function fnAvaliaDataEntrega(dataEntrega, dataProposta ){
	let _dtEntrega = moment(dataEntrega, "DD/MM/YYYY");
	let _dtProposta = moment(dataProposta, "DD/MM/YYYY");
	if(_dtProposta.isAfter(_dtEntrega)){

	}
	if(_dtProposta.isSame(_dtEntrega)){

	}
	
}

/** @function validaCotacao() - responsável por executar todas as validações para a última cotação inputada
 *  @author {Jean Varlet}
 *  @returns {Boolean} - true/false -
 */
function validaCotacao(){
	let _valid = true;
	// Checa se existem fornecedores adicionados/escolhidos --
	if($("#dvCotacoesFilhos").children().length){
		//Valida a seleção do fornecedor
		if(validaForncedorSel()){
			//Valida a seleção disponibilidade/preenchimento dos itens de cotação 
			if(validaInputCot()){
				
		   }else{
				_valid = false;
		   }
		}else{
			_valid = false;
		}
	}
    return _valid;

}
/** @function validaForncedorSel() - Valida os dados dos fornecedor selecionado na etapa de input de
 * cotações
 * @author {Jean Varlet}
 * @returns {Boolean}
 */
function validaForncedorSel(){
	let _retorno = true;
	$(".fornecedor",$(".entradaCotacao:last")).each(function(){
        let _id = $(this).attr("id");
        if($("#"+_id).val() == ''){
			_retorno = false;
            toast('Atenção!', 'Cheque os dados do fornecedor selecionado.', 'warning');
        }else{
             if(_id.indexOf('emailForn___') != -1 && $("#"+_id).val().indexOf('@') == -1 ){
				toast('Atenção!', 'O email inserido é inválido.', 'warning');
				_retorno = false;
            }
		}
		if(!validaDataEntrega(_id)){
			_retorno = false;	
		}
		if(!validaValorTotalCot(_id)){
			_retorno = false;
		}
	});
	return _retorno; 	
}
/** @function validaDataEntrega(indexer) - valida a data de entrega inserida no input de cotações
 * 	@author {Jean Varlet}
 *  @param {int} - indexer 
 */
function validaDataEntrega(indexer){
	let _pos = indexer.split('___')[1];
	let _retorno = true;
	if($("input",$("#EntPrevisCot___"+_pos)).val() != ''){
		if(!moment($("input",$("#EntPrevisCot___"+_pos)).val(), "DD/MM/YYYY").isAfter(moment())){
			toast('Atenção', 'A data de entrega não pode ser inferior a hoje.', 'warning');
			_retorno = false;
		}
	}else{
		toast('Atenção!', 'Não esqueça de preencher a data de entrega da cotação.', 'warning');
		_retorno = false;
	}
	return _retorno;
}

/** @function validaValorTotalCot() - Checa o preenchimento do campo valor total do input de cotação
 * 	@author {Jean Varlet}
 */
function validaValorTotalCot(indexer){
	let _pos = indexer.split('___')[1];
	let _retorno = true;
	if($("#valTotCot___"+_pos).val() == '' || $("#valTotCot___"+_pos).val().indexOf('NaN') != -1 ){
		toast('Erro!', 'O valor total da cotação é inválido.', 'danger');
		_retorno = false;
	}
	return _retorno;
}
/** @function validaInputCot() - Valida o preenchimento das cotações por fornecedor na etapa de 
 * input
 * 	@author {Jean Varlet}
 *  @returns {Boolean} - true/false
 */
function validaInputCot(){
    let _retorno = true;
	let _index = $(".itensCompCot:last").attr("id").split('___')[1];
	let counter = $("tr", $("#itensCompCot___"+_index)).length;
	//Percorre os itens de cotação para o fornecedor da vez -- 
	for(let i = 1; i <= counter; i++){
		let _pos = _index+'___'+i;

		if($("#dvDisponivel___"+_pos).is(":visible")){
			if($("#qtdItemCot___"+_pos).val() == 0 || $("#qtdItemCot___"+_pos).val() == ''){
				_retorno == false;
			}
			if($("#valItemCot___"+_pos).val() == 0 || $("#valItemCot___"+_pos).val() == 'R$ 0,00' || $("#qtdItemCot___"+_pos).val() == ''
			|| $("#qtdItemCot___"+_pos).val() == 0){
				_retorno == false;
			}        
		}
		//Div de seleção do motivo da insdisponibilidade do item de cotação - 
		else if($("#dvInDisponivel___"+_pos).is(":visible")){
			if($("#motivoIndCot___"+_pos).val() == 'selecione'){
				_retorno = false;
			}    
		}
	}
  
    return _retorno;
}