/** Jean Varlet - funções para carregamento das versões MOBILE e DESKTOP do formulário */

/**Inicia scripts referentes a visão para desktop
@param {int} - ATIVIDADE_ATUAL
@param {string} - matricula Colaborador 
@param {string} - ViewMode - ADD, MOD, VIEW
 */
function loadDesktop(atividade, matricula, modo, isMobile,rootFolder){
	console.log('Modo Desktop->');
	if(modo == "ADD" || modo == "MOD"){
		//Atribui ação de envio para o botão Enviar personalizado -> 
		$("#enviarbtn").click(function(){
			enviarAction(atividade);
		});
		fnDadosInteracao(atividade, isMobile);	
		if(atividade == "0" || atividade == "4"){
			
			callCreateId().then(createFolderStructure(rootFolder));	
			
			$("#anexarArquivos").click(function(){
    			createModalOfAttachments(atividade);
    		});
			
			$("#cadastrarEvento").click(function(){
				openEventoModal()
			})
		}
		if(atividade == "5") {//Analise UABS --
			
			fnDadosSolicitacao($("#nrSolicitacao").val(), atividade );

			correctIds();
			
			FLUIGC.switcher.disable('#projAcUABS')
			FLUIGC.switcher.disable('#saldoUABS')
			setTimeout(function(){
				$(".fluigicon-trash", $("#itensSolicitacao")).hide();
				
			},500)
			setTimeout(function(){
				parseItensRateios();
			},1000)
			
			$("#addCotacao").click(function(){
				if(validaCotacao()){
					addItemCotacao();
					$("#dvFinCotacoes").fadeIn();
				}
				
			});
			$("#gerarMovimentos").click(function(){
				parseItensFinal();
				setTimeout(function(){
					callGerarMovs();
				},500)
				
			})
		}
		if(atividade == "12"){//Primeira etapa de ajustes de solcitação
			
			$("#conteudoAjusUABS").text($("#descAjustUABS").val());
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );

			setTimeout(function(){
				$(".fluigicon-trash", $("#dvCotPaiFilho")).hide();
			},500)
			
		}
		if(atividade == "18"){
			
		}
		if(atividade == "25"){
			
			parseItensCot2();
			
			correctIds();
			
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
			setTimeout(function(){
				$(".fluigicon-trash", $("#itensSolicitacao")).hide();
				
			},500)
			setTimeout(function(){
				parseItensRateios();
			},1000)
			
			$("#decisaoAnalUABS").val('ajustar');
			parseCotacoesMapa(atividade);
		

		}
		if(atividade == "32"){ //DAF - 
			
			
			$("#addCotacao").prop("disabled", true);
			parseItensCot2();
			//Cotações ----
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
			setTimeout(function(){
				parseItensRateios();
			},1000)
			setTimeout(function(){
				$(".fluigicon-trash", $("#itensSolicitacao")).hide();
				//$(".fluigicon-trash", $("#dvCotPaiFilho")).hide();
			},500)
			parseCotacoesMapa(atividade);
			$("#navPorForn").click(function(){
				$("#dvShowMapaItem").hide();	
				$("#dvShowMapaForn").fadeIn();
				parseCotacoesMapa(atividade);
				disableSelections(atividade);
			
			})
			$("#navPorItem").click(function(){
				$("#dvShowMapaForn").hide();	
				$("#dvShowMapaItem").fadeIn();
				parseCotByPrice();
				disableSelections(atividade);	
			})
			cabecalhoSolicitacao(atividade);
			
		}
		if(atividade == 38){
			
			
			$("#inicio").fadeIn();
			$("#dvDataEntrega").fadeOut();
			$("#dadosSolicitante").fadeIn();
			$("#dvInsereCotacoes").hide();
			$("#mapadePrecos").show();
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
			setTimeout(function(){
				parseItensRateios();
			},1000)
			parseItensCot2();
			parseCotacoesMapa(atividade);
			$("#navPorForn").click(function(){
				$("#dvShowMapaItem").hide();	
				$("#dvShowMapaForn").fadeIn();
				parseCotacoesMapa(atividade);
				disableSelections(atividade);
			
			})
			$("#navPorItem").click(function(){
				$("#dvShowMapaForn").hide();	
				$("#dvShowMapaItem").fadeIn();
				parseCotByPrice();
				disableSelections(atividade);	
			})
			$("#enviaAFForn").fadeIn();
			$("#gerarMovimentos").click(function(){
				callGerarMovs();
				
				$("#enviarAf").fadeIn();
			})

			$("#enviarbtn").click(function(){
				parent.document.getElementById("workflowActions").querySelector("button").click();
			})
		}
		if(atividade == 40){
			
			$("#aguardaAssinaturas").show();
			$("#inicio").fadeIn();
			$("#dvDataEntrega").fadeOut();
			$("#dadosSolicitante").fadeIn();
			$("#dvInsereCotacoes").hide();
			$("#mapadePrecos").show();
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
			setTimeout(function(){
				parseItensRateios();
			},1000)
			parseItensCot2();
			parseCotacoesMapa(atividade);
			$("#navPorForn").click(function(){
				$("#dvShowMapaItem").hide();	
				$("#dvShowMapaForn").fadeIn();
				parseCotacoesMapa(atividade);
				disableSelections(atividade);
			
			})
			$("#navPorItem").click(function(){
				$("#dvShowMapaForn").hide();	
				$("#dvShowMapaItem").fadeIn();
				parseCotByPrice();
				disableSelections(atividade);	
			})
		}
		loadComportamentos(modo, atividade);
		retornaSolsAssinaturas(atividade, $("#nrSolicitacao").val());

	}else if(modo == "VIEW"){
		
		fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
		setTimeout(function(){
			parseItensRateios();
		},1000)
		setTimeout(function(){
			$(".fluigicon-trash").hide();
			//$(".fluigicon-trash", $("#dvCotPaiFilho")).hide();
		},500)
		
					
		
	}
	showNrSolicitacao(atividade);
	showAnexos(atividade);
	
}


/** Inicia scripts referentes a visão para mobile
@param {int} - ATIVIDADE_ATUAL
@param {string} - matricula Colaborador 
@param {string} - ViewMode - ADD, MOD, VIEW
 */
function loadMobile(atividade, matricula, modo){
	console.log('Modo Mobile->');
	if(modo == "ADD" || modo == "MOD"){
		//Atribui ação de envio para o botão Enviar personalizado -> 
		
		fnDadosInteracao(atividade, isMobile);	
		if(atividade == "0" || atividade == "4"){
			
			callCreateId().then(createFolderStructure(rootFolder))	
			
			$("#cadastrarEvento").click(function(){
				openEventoModal()
			})
		}
		if(atividade == "5") {//Analise UABS --
			
			fnDadosSolicitacao($("#nrSolicitacao").val(), atividade );

			correctIds();
			
			FLUIGC.switcher.disable('#projAcUABS')
			FLUIGC.switcher.disable('#saldoUABS')
			setTimeout(function(){
				$(".fluigicon-trash", $("#itensSolicitacao")).hide();
				
			},500)
			setTimeout(function(){
				parseItensRateios();
			},1000)
			
			$("#addCotacao").click(function(){
				if(validaCotacao()){
					addItemCotacao();	
				}
				
			})
			
		}
		if(atividade == "12"){//Primeira etapa de ajustes de solcitação
			
			$("#conteudoAjusUABS").text($("#descAjustUABS").val());
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );

			setTimeout(function(){
				$(".fluigicon-trash", $("#dvCotPaiFilho")).hide();
			},500)
			
		}
		if(atividade == "18"){
			cabecalhoSolicitacao(atividade);
		}
		if(atividade == "25"){
			
			parseItensCot2();
			
			correctIds();
			
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
			setTimeout(function(){
				$(".fluigicon-trash", $("#itensSolicitacao")).hide();
				
			},500)
			setTimeout(function(){
				parseItensRateios();
			},1000)
			
			$("#decisaoAnalUABS").val('ajustar');
			parseCotacoesMapa(atividade);
		

		}
		if(atividade == "32"){ //DAF - 
		
			parseItensCot2();
			
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
			
			setTimeout(function(){
				$(".fluigicon-trash", $("#itensSolicitacao")).hide();
				//$(".fluigicon-trash", $("#dvCotPaiFilho")).hide();
			},500)

			setTimeout(function(){
				parseItensRateios();
			},1000)
			
			setTimeout(function(){
				parseCotacoesMapa(atividade);
			},3000)
			cabecalhoSolicitacao(atividade);
			
		}
		if(atividade == 38){//Envia AF Fornecedor --
			
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
			setTimeout(function(){
				parseItensRateios();
			},1000)
			parseItensCot2();
			parseCotacoesMapa(atividade);
			cabecalhoSolicitacao(atividade);

		}
		if(atividade == 40){
			
			fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
			setTimeout(function(){
				parseItensRateios();
			},1000)
			parseItensCot2();
			parseCotacoesMapa(atividade);
			cabecalhoSolicitacao(atividade);
		
		}
		loadComportamentos(modo, atividade);
		retornaSolsAssinaturas($("#nrSolicitacao").val());

	}else if(modo == "VIEW"){
		
		fnDadosSolicitacao($("#nrSolicitacao").val(), 5 );
		setTimeout(function(){
			parseItensRateios();
		},1000)
		setTimeout(function(){
			$(".fluigicon-trash").hide();
			//$(".fluigicon-trash", $("#dvCotPaiFilho")).hide();
		},500)
		
					
		
	}
	showNrSolicitacao(atividade);
	showAnexos(atividade);
	
	
}