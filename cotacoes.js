/**

			----- Jean Varlet - Cotações -----

 */

/**16/09/2020 - Retorna Fornecedores do TOTVS RM
 * @author {Jean Varlet}
 * @param {varchar} -> CNPJ -> CNPJ Formatado  
 */
function retornaFornecedor(cnpj){
   
    console.log(cnpj)
	let cst1 = DatasetFactory.createConstraint("CGCCFO", cnpj, cnpj, ConstraintType.MUST);
    
	let _retorno = DatasetFactory.getDataset("dsClienteRM2", null, [cst1], null);	
	if(!jQuery.isEmptyObject(_retorno)){
		console.log(_retorno);
		
	}else{
		console.log('Fornecedor não encontrado');
	}		
}

/** 16/09/2020 - Percorre os itens de compra (JSON) existentes no campo #conteudoRateios
* @author {Jean Varlet}
* @param {int} indexer -> posição de ordem do item de compra.
*/
function parseItensCot(indexer){
	let _obj;
	if($("#conteudoRateios").val() != ''){
		_obj = JSON.parse($("#conteudoRateios").val());
		if(_obj.length){
			for(var i = 0; i < _obj.length; i++){
				var _item = _obj[i];

				fnAppendItemCompra(indexer, i, _item.produto, _item.especificacao, _item.quantidade, _item.idProduto);
			}
		}	

	}else{
		console.log('Erro! O JSON está vazio!')
	}
	setTimeout(function(){
		loadSwitchsCots('cotSwitchs');
	},300)
	setTimeout(function(){
		$(".valItemCot").click(function(){
			$(this).val('');
			
		})
		fnInciaCamposCot(indexer);
	
	},500)	
	
}
/** 22/09/2020 - Inicializar os swichts da área de cotações para os itens de compra
 * @param {varchar} - classe 
 * @author {Jean Varlet}
 */
function loadSwitchsCots(classe){
	let id;
	$("."+classe, $('.itensCompCot:last')).each(function(){
		 id = $(this).attr("id");
		console.log('oadSwitchsCots()-> classe-> ' + classe + ' id-> '+id);
		FLUIGC.switcher.init($("#"+id));
		$("#"+id).attr("data-on-color", 'success')
	    $("#"+id).attr("offColor", 'danger')
	    $("#"+id).attr("onText", 'Sim')
	    $("#"+id).attr("offText", 'Não');
		
		FLUIGC.switcher.setFalse($("#"+id));
		
		FLUIGC.switcher.onChange("#"+id, function(event, state) {
			if (state === true) {
				let pos = $(this).attr("id").split('___');
				$("#dvInDisponivel___"+pos[1]+"___"+pos[2]).fadeOut();
				$("#dvDisponivel___"+pos[1]+"___"+pos[2]).fadeIn();
				
			} else if (state !== true) {
				let pos = $(this).attr("id").split('___');
				$("#dvInDisponivel___"+pos[1]+"___"+pos[2]).fadeIn();
				$("#dvDisponivel___"+pos[1]+"___"+pos[2]).fadeOut();

				
			}
		});
	});
}
/**Inicia campos de valor, quantidade e valor total para inserção de contações
 * @param{int} -> posição do item pai x filho
 * @author{Jean Varlet}
 */
function fnInciaCamposCot(index){
	$(".qtdItemCot").click(function(){
		let _i = $(this).attr("id").split('___')[1];
		if($("#valItemCot___"+_i).val() == ''){
			$("#valItemCot___"+_i).val(reais(0));	
		}
		console.log(_i)
		recalcCot(_i);
	})
	$(".qtdItemCot").blur(function(){
		let _i = $(this).attr("id").split('___')[1];
		if($("#valItemCot___"+_i).val() == ''){
			$("#valItemCot___"+_i).val(reais(0));	
		}
		console.log(_i)
		recalcCot(_i);
	})
	$(".valItemCot").blur(function(){
		let _i = $(this).attr("id").split('___')[1];
		if(!isMoneyFormat($(this).val())){
			$(this).val(reais($(this).val()));
			   
		}
		console.log(_i)
		recalcCot(_i);
	})

	FLUIGC.calendar('#EntPrevisCot___'+index , {
        pickDate: true,
        pickTime: false,
        
        useCurrent: false
        
    	});	
}
function recalcCot(indexer){
    let _totalizador = 0;
    $(".qtdItemCot", $("#itensCompCot___"+indexer)).each(function(){
		let _idItem = $(this).attr("id").split('qtdItemCot___')[1];
		if($("#itemDispCot___"+_idItem).is(":checked")){
			let _temp = parseInt($("#qtdItemCot___"+_idItem).val()) * floatReais($("#valItemCot___"+_idItem).val());
        	_totalizador += _temp;
		}
        
    })
    $("#valTotCot___"+indexer).val(reais(_totalizador))    
}

/** 16/09/2020 - Appenda itens de compra na table itensCompCot
	@param {int} indexer -> ordem de input de cotações--
	@param {varchar} titulo -> titulo de input de cotações--
	@param {varchar} especificacao -> especificacao de input de cotações--
	@param {int} quantidade -> quantidade de input de cotações--
	@param {int} idprod -> 

 */
function fnAppendItemCompra(indexer, pos, titulo, especificacao, quantidade, idprod){
	pos = pos +1;
	$("#itensCompCot___"+indexer).append(
		'<tr>'+
		'	<td>'+
		'		<div class="row">'+
		'			<div class="col-md-1 col-sm-2 form-group">'+
		'				<h4 id="idprod___'+indexer+'___'+pos+'">'+idprod+'</h4>'+
		'			</div>'+
		'			<div class="col-md-7 col-sm-10 form-group">'+
		'				<h4 id="tituloItem___'+indexer+'___'+pos+'">'+titulo+'</h4>'+
		'			</div>'+
		'			<div class="col-md-2 col-sm-2 form-group" style="text-align:center;">'+
		'				<h4 id="qtdItem___'+indexer+'___'+pos+'">'+quantidade+'</h4>'+
		'			</div>'+
		'			<div class="col-md-2">'+
		'				<input class="cotSwitchs" id="itemDispCot___'+indexer+'___'+pos+'"   type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Sim" '+
		'				data-off-text="Não"  	data-animate="true"  checked="false"  data-size="large">'+
		'			</div>'+
		'		</div><!--row-->'+
		'		<div class="row">'+
		'			<div class="col-md-1"></div>'+
		'			<div class="col-md-6 col-sm-8 form-group">'+
		'				<h4>Especificação do Item de Compra</h4>'+
		'				<h4>'+especificacao+'</h4>'+
		'			</div>'+
		'			<div class="col-md-5">'+
		'				<div class="row">'+
		'					<div class="col-md-12 form-group">'+
		'				<div class="row" id="dvDisponivel___'+indexer+'___'+pos+'" style="display:none;">'+
		'					<div class="col-md-2  form-group">'+
		
		'					</div>'+
		'					<div class="col-md-4 col-sm-12 form-group">'+
		'						<h4>Quantidade:</h4>'+
		'						<input type="number" id="qtdItemCot___'+indexer+'___'+pos+'"  class="form-control qtdItemCot" min="0" max="99999" value="'+quantidade+'">'+
		'					</div>'+
		'					<div class="col-md-6 col-sm-12 form-group">'+
		'						<h4>Valor Unitário</h4>'+
		'						<input type="text"  id="valItemCot___'+indexer+'___'+pos+'" class="form-control valItemCot" value="R$ 0,00">'+
		'					</div>'+
		'				</div><!--row-->'+
		'				<div class="row" id="dvInDisponivel___'+indexer+'___'+pos+'">'+
		'					<div class="col-md-2 form-group"></div>'+
		'					<div class="col-md-10 col-sm-12 form-group">'+
		'						<select name="motivoIndCot" id="motivoIndCot___'+indexer+'___'+pos+'" class="form-control">'+
		'							<option value="selecione">Selecione o motivo</option>'+
		'							<option value="indisponivel">Produto Indisponível</option>'+
		'							<option value="especificacao">Especificação não atendida</option>'+
		'							<option value="problemasEspecificacao">Problemas na Especificação</option>'+
		'						</select>'+
		'					</div>'+
		
		'				</div><!--row-->'+
		'					</div>'+
		'				</div><!--row-->'+

		'			</div>'+
		'		</div><!--row-->'+
		
		'	</td>'+
		'</tr>'
	
	)

}
/** 23/09/2020 - Adiciona itens de cotação na div #dvCotacoesFilhos 
 * @author {Jean Varlet}
*/
function addItemCotacao(){
	let index = 1;
	if($(".entradaCotacao").length){
		index = parseInt($(".entradaCotacao").length) + 1;
	}
	$("#dvCotacoesFilhos").append(
	'<div class="entradaCotacao" id="entradaCot___'+index+'">'	+
	'	<div class="row">'+
	
	'		<div class="col-md-1"></div>'+
	'		<div class="col-md-10"><hr></div>'+
	'		<div class="col-md-1"></div>'+
	
	'	</div><!--row-->'+
	'	<div class="row">'+
	
	'		<div class="col-md-1"></div>'+
	
	'		<div class="col-md-3">'+
	'			<h3>Proposta: '+index+'</h3>'+
	'		</div>'+
	'		<div class="col-md-6"></div>'+
	'		<div class="col-md-1">'+
	'      		<i class="fluigicon fluigicon-trash icon-sm btn btn-danger delCotacao" style="cursor:pointer;" id="delCotacao___'+index+'" ></i>'+
	'		</div>'+
	//'		<div class="col-md-1">'+
	//'			<i class="fluigicon fluigicon-pencil icon-sm btn btn-warning editcotacao" style="cursor:pointer;margin-top:120px;display:none; " id="editCotacao___'+index+'"></i>'+
	//'		</div>'+
	
	'		<div class="col-md-1"></div>'+
	
	'	</div><!--row-->'+
	'	<div class="row">'+
	
	'		<div class="col-md-1"></div>'+
	'		<div class="col-md-10"><hr></div>'+
	'		<div class="col-md-1"></div>'+
	
	'	</div><!--row-->'+
	'	<div class="row" >'+
	'		<div class="col-md-1"></div>'+
	'		<div class="col-md-10 col-sm-12 ">'+
	'			<h4>informe o CNPJ do Fornecedor</h4>'+
	' 			<input type="text" id="selCNPJForn___'+index+'"  class="form-control basicAutoSelect fornecedor"> '+
	
	'		</div>'+
	'		<div class="col-md-1"></div>'+
	'	</div><!--row-->'+
	'	<div class="row">'+
	'		<div class="col-md-1"></div>'+
	'		<div class="col-md-10 col-sm-12">'+
	'			<h4> Razão Social</h4>'+
	'			<input type="text" id="razaoForn___'+index+'"  class="form-control fornecedor" readonly>'+
	'		</div>'+
	'		<div class="col-md-1"></div>'+
	'	</div><!--row-->'+
	'	<div class="row">'+
	'		<div class="col-md-1"></div>'+
	'		<div class="col-md-10 col-sm-12">'+
	'			<h4> Nome Fantasia</h4>'+
	'			<input type="text" id="fantasiaForn___'+index+'"  class="form-control fornecedor" readonly>'+
	'		</div>'+
	'		<div class="col-md-1"></div>'+
	'	</div><!--row-->'+
	'	<div class="row">'+
	'		<div class="col-md-1"></div>'+
	'		<div class="col-md-5 col-sm-12 ">'+
	'			<h4>E-mail do Fornecedor:</h4>'+
	'			<input type="text" id="emailForn___'+index+'"  class="form-control emailForn fornecedor" >'+	
	'		</div>'+
	'		<div class="col-md-5 col-sm-12 ">'+
	'			<h4>CODCFO</h4>'+
	'			<input type="text" id="codcfo___'+index+'"  class="form-control fornecedor" readonly>'+	
	'		</div>'+
	'		<div class="col-md-1"></div>'+
	'	</div><!--row-->'+
	
	'</div><!--entradaCotacao-->'+
	
	'<div class="row" id="dvCotacao___'+index+'">'+
	'	<div class="col-md-1"></div>'+
	'	<div class="col-md-10">'+

		'<div class="row" style="margin-top:12px;" > '+
		//'	<div class="col-md-1"></div>'+
		'	<div class="table col-md-12 col-sm-12" class="table-responsive">'+
		'		<table class="table table-bordered table-striped">'+
		'			<thead>'+
		'				<tr>'+
		'					<td>'+
		'						<div class="row">'+
		'							<div class="col-md-1"></div>'+
		'							<div class="col-md-7 col-sm-8 form-group">'+
		'								<h4>Item de Compra <i class="fluigicon fluigicon-shopping-cart icon-sm info"></i></h4>'+
		'							</div>'+
		'							<div class="col-md-2 col-sm-2 form-group">'+
		'								<h4>Quantidade</h4>'+
		'							</div>'+
		'							<div class="col-md-2">'+
		'								<h4>Disponível?</h4>'+
		'							</div>'+
		'						</div><!--row-->'+
		'					</td>'+
		'					'+
		'				</tr>'+
		'			'+
		'			</thead>'+
		'			<tbody id="itensCompCot___'+index+'"  class="itensCompCot">'+
		'			'+
		'			'+
		'			</tbody>'+
		'		'+
		'		</table>'+
		'	'+
		'	</div><!-- table -->'+
		//'	<div class="col-md-1"></div>'+
		'</div><!-- row -->'+

	'	</div>'+
	'	<div class="col-md-1"></div>'+
	'</div><!--row-->'+	

	
	'<div class="row" style="margin-top:12px;" id="totalCot___'+index+'">  ' +
	'	<div class="col-md-1"></div>'+
	' 	<div class="col-md-5 col-sm-12 form-group" id="anexosCot">  ' +
	' 		<h4>Anexar Cotação</h4>  ' +
	' 		<i id="anexarPropCot___'+index+'" class="anexarPropCot fluigicon fluigicon-paperclip icon-md btn btn-warning"></i>	  ' +	
	' 	</div>		  ' +
	' 	<div class="col-md-2 col-sm-12 form-group" id="dtEntCot">  ' +
	' 		<h4>Entrega Prevista</h4>  ' +
	' 		<div class="input-group date calendar entregaPrev"  id="EntPrevisCot___'+index+'">  ' +
	' 			<input type="text" class="form-control pegadata " name="EntPrevisCot___'+index+'"   mask="00/00/0000" readonly>  ' +
	' 			<span class="input-group-addon fs-cursor-pointer" style="background-color:#45b6da;">  ' +
	' 				<span class="fluigicon fluigicon-calendar icon-sm clickDataEntCot" style="color:white;"></span>  ' +
	' 			</span>  ' +
	' 		  ' +
	' 		</div><!-- input-group date calendar -->	  ' +		
	' 	</div>	<!--col-md-2-->	  ' +
	' 	<div class="col-md-3 col-sm-12 form-group" >  ' +
	' 		<h4>Valor Total</h4>  ' +
	' 		<input type="text" 	id="valTotCot___'+index+'" class="form-control valTotCot" readonly>	  ' +		
	' 	</div>	<!--col-md-3-->	  ' +
	'	<div class="col-md-1"></div>'+
	' </div> <!--row--> ' +
	' <div class="row" id="dvObsCot___'+index+'">  ' +
	'	<div class="col-md-1"></div>'+
	' 	<div class="col-md-10 col-sm-12 form-group">  ' +
	' 		<h4>Observações</h4>  ' +
	' 		<textarea id="obscotacoes___'+index+'"  class="form-control obscotacoes"></textarea>  ' +
	' 	</div>	<!--col-md-10-->		  ' +
	'	<div class="col-md-1"></div>'+
	' </div> <!--row-->  '+
	'<div class="row showAnexos" id="showAnexos___'+index+'" style="display:none;">'+
	'	<div class="col-md-1"></div>'+
	'	<div class="col-md-10">'+
	'		<h3>Proposta Anexada</h3>'+
	'	</div>'+
	'	<div class="col-md-1"></div>'+
	'</div><!--row-->'+
	//Certidões ---------------
	'<div id="certCotacao___'+index+'">  ' +
	' <div class="row" >  ' +
	'	<div class="col-md-1"></div>'+
	' 	<div class="col-md-10 col-sm-12">  ' +
	' 		<h3 style="color:#2E66B7;"><i class="fluigicon fluigicon-file-approval icon-md"></i>  Certidões</h3>  ' +
	'		<hr>'+	
	' 	</div>  ' +
	'	<div class="col-md-1"></div>'+
	' </div> <!--row--> ' +
	//' </div>  ' +
	' <div class="row">  ' +
	'	<div class="col-md-1"></div>'+
	' 	<div class="col-md-5 col-sm-12 certidaoCnd form-group" id="certidaoCnd___'+index+'">  ' +
	//' 		<h4> <i class="fluigicon fluigicon-question-sign icon-md warning"></i> Nome certidão</h4>  ' +
	' 	</div>	  ' +
	' 	<div class="col-md-5 col-sm-12 certidaoFgts form-group" id="certidaoFgts___'+index+'">  ' +
	//' 		<h4> <i class="fluigicon fluigicon-question-sign icon-md warning"></i> Nome certidão</h4>  ' +
	' 	</div>	  ' +
	
	'	<div class="col-md-1"></div>'+
	' </div>	<!--row-->	  ' +
	'<div class="row" id="dvanexarCertidoes___'+index+'" style="display:none;">'+
	'	<div class="col-md-1"></div>'+
	'	<div class="col-md-10">'+
	'		<h4>Anexar Certidões para esse fornecedor</h4>'+
	' 		<i id="anexarCerts___'+index+'" class="anexarCerts fluigicon fluigicon-paperclip icon-md btn btn-warning"></i>	  ' +
	'	</div>'+
	'	<div class="col-md-1"></div>'+
	'</div>'+	
	' </div> <!--certCotacao___ -->  '
	// Certidões ------------ 

	)
	$("#anexarPropCot___"+index).click(function(){
		createModalOfAttachments('cotacao');
	})
	$("#anexarCerts___"+index).click(function(){
		createModalOfAttachments('certidoes');
	})
	fnAutoCompleteFornecedor(index);
	$(".delCotacao").click(function(){
		let _id = $(this).attr("id");
		let _pos = _id.split('___')[1];
		$("#entradaCot___"+_pos).remove();
		$("#itensCompCot___"+_pos).remove();
		$("#certCotacao___"+_pos).remove();
		$("#dvCotacao___"+_pos).remove();
		$("#totalCot___"+_pos).remove();
		$("#dvObsCot___"+_pos).remove();
		$("#showAnexos___"+_pos).remove();
	})
	$(".editCotacao").click(function(){
		let _id = $(this).attr("id");
		let _pos = _id.split('___')[1];
		toast('Liberado.', 'Entrada de cotação '+ _pos + ' liberada para edição', 'warning');
		//$("#editCotacao___"+_pos).remove();
	})
	$("#finalizarCotacao").click(function(){
		if(validaCotacao()){
			fnFinalizarCotacao();
		}
		else{
			toast('Atenção!', 'Há um item com pendência de preenchimento!', 'warning');
		}
		
	})
}
//TODO: Checar se necessário manter essa function --
/**16/09/2020 - Totalizador para itens de cotações - Sem uso
 * @author{Jean Varlet}
 * @param{int}
 */
function totalizadorCot(indexer){
	//tbody id -> itensCompCot___  -> dvDisponivel___ dvInDisponivel___
	if($("#dvDisponivel___"+indexer).is(":visible")){

	}
}
//TODO: Checar se necessário manter essa function ---
/**16/09/2020 - Appenda valor total de uma cotação --
 * @author{Jean Varlet}
 */
function appendValTotalCot(){

}

/** @function fnAutoCompleteFornecedor(indexer) - Realiza a consulta na base do RM por cnpj para retornar dados do fornecedor
 * @author {Jean Varlet}
 * @parama {int} - posicao
 */
function fnAutoCompleteFornecedor(indexer){//dsClienteRM2
	var autoCompleteForn = FLUIGC.autocomplete('#selCNPJForn___'+indexer, {
		highlight: true,
		minLength: 0,
		hint: true,
		searchTimeout: 100,
		type: 'tagAutocomplete',
		name: 'dsClienteRM2',
		tagClass: 'tag-warning',
		maxTags: 1,
		allowDuplicates: false,
		displayKey: 'CGCCFO',
		source: {
			url: '/api/public/ecm/dataset/search?datasetId=dsClienteRM2&searchField=CGCCFO&',
			limit: 10,
			offset: 0,
			limitKey: 'limit',
			patternKey: 'searchValue',
			root: 'content'
		},
		onMaxTags: function(item, tag) {
		},
		tagMaxWidth: 800
	}, function(err, data) {
		// something with err or data.
		if(err) {
			
		} // if
	}); // var lookup

	autoCompleteForn.on('fluig.autocomplete.itemAdded', function (result){
		console.log(result.item.NOMEFANTASIA)
		console.log(result.item.NOME)
		$("#razaoForn___"+indexer).val(result.item.NOME);
		$("#fantasiaForn___"+indexer).val(result.item.NOMEFANTASIA);
		$("#codcfo___"+indexer).val(result.item.CODCFO);
		if(result.item.EMAIL == null || result.item.EMAIL == 'null' || result.item.EMAIL == ''){
			$("#emailForn___"+indexer).val('Email Não cadastrado');	
		}else{
			$("#emailForn___"+indexer).val(result.item.EMAIL);
		}
		$("#emailForn___"+indexer).prop("readonly", false);	
		$("#emailForn___"+indexer).click(function(){
			$(this).val('');
		});
		$("#emailForn___"+indexer).blur(function(){
			if($(this).val() == ''){
				$(this).val('Email Não Cadastrado');	
			}
		});
		console.log('Tamanho cgc->'+result.item.CGCCFO.length)
		setTimeout(function(){
			parseItensCot(indexer);
		},300)
		if(result.item.CGCCFO.length == 18){
			showLoading('#entradaCot___'+indexer, 'show', 'Checando certidões para esse fornecedor...');
			setTimeout(function(){
				consultaApiGescon(result.item.CGCCFO.toString().replaceAll('.','').replace('/','').replace('-',''), indexer);
			},500)
		}else{
			setTimeout(function(){
				$("#certCotacao___"+indexer).hide();
			},500)
		}
		
	})
	
}

function showCertidoesArea(indexer){

}

// Jean Varlet - Exibe cotações realizadas -->

function parseItensCot2(){
	let _obj;
	if($("#conteudoCotacao").val() != ''){
        if($("#dvCotacoesFilhos").children().length){
            $("#dvCotacoesFilhos").children().remove();    
        }
		_obj = JSON.parse($("#conteudoCotacao").val());
		if(_obj.length){
			for(var i = 0; i < _obj.length; i++){
				var _proposta = _obj[i];
                var _cotacoes = _proposta.cotacao;
                
                
				fnAppendCotacoes(_proposta.ordem, _proposta.cnpj, _proposta.razao,  _cotacoes,  _proposta.valorTotal, _proposta.entrega)
			}
		}	

	}else{
		console.log('Erro! O JSON está vazio!')
	}
	//setTimeout(function(){
		//loadSwitchsCots('cotSwitchs');
	//},300)
	//setTimeout(function(){
		//$(".valItemCot").click(function(){
		//	$(this).val('');
			
		//})
		//fnInciaCamposCot(indexer);
	
	//},500)
	
}

function fnAppendCotacoes(ordem, cnpj, fantasia, produtos,  valorTotal, dataEntrega){
    $("#dvCotacoesFilhos").append(
		'<div id="cotacao___'+ordem+'">'+
		'	<div class="row">'+
		'		<div class="col-md-1"></div>'+
		'		<div class="col-md-10">'+
		'		<h3>Cotação: '+ordem+'</h3>'+
		'		</div>'+
		'		<div class="col-md-1"></div>'+
		'	</div><!--row-->'+
        '    <div class="row">'+
        '        <div class="col-md-1"></div>'+
        '        <div class="col-md-5 col-sm-12">'+
        '           <h3>Fornecedor</h3> '+
        '           <h4 style="color:#2E66B7;">'+fantasia+'</h4>'+
        '        </div>'+
        '        <div class="col-md-5 col-sm-12">'+
        '           <h3>CNPJ</h3> '+
        '           <h4 class="cnpjForn" id="cnpjForn___'+ordem+'" style="color:#2E66B7;">'+cnpj+'</h4>'+
        '        </div>'+
        '        <div class="col-md-1"></div>'+
        '    </div><!--row-->'+
        '    <div class="row">'+
        '        <div class="col-md-1"></div>'+
        '        <div class="col-md-5 col-sm-12">'+
        '           <h3>Data de Entrega</h3> '+
        '           <h4 style="color:#2E66B7;">'+dataEntrega+'</h4>'+
        '        </div>'+
        '        <div class="col-md-5 col-sm-12">'+
        '           <h3>Valor Total</h3> '+
        '           <h4 style="color:#2E66B7;">'+valorTotal+'</h4>'+
        '        </div>'+
        '        <div class="col-md-1"></div>'+
		'    </div><!--row-->'+
		//Proposta Anexada ----
		'	<div class="row">'+
		'		<div class="col-md-1"></div>'+
		'		<div class="col-md-10 col-sm-12">'+
		'			<h3>Proposta Anexada</h3>'+
		'			<a id ="linkProposta___'+ordem+'" href="#"><i class="fluigicon fluigicon-file-pdf icon-md"></i>  baixar</a>'+
		'		</div>'+
		'		<div class="col-md-1"></div>'+
		'	</div><!--row-->'+
		//Proposta Anexada ----
		//Exibe certidões ---
		'	<div class="row">'+
		'		<div class="col-md-1"></div>'+
		'		<div class="col-md-10 col-sm-12">'+
		'			<h3>Certidões</h3>'+
		'		</div>'+
		'		<div class="col-md-1"></div>'+
		'	</div><!--row-->'+
        // Título dos produtos cotados -->
        '    <div class="row">'+
        '        <div class="col-md-1"></div>'+
        '        <div class="col-md-10 col-sm-12" >'+
        '            <h3><i class="fluigicon fluigicon-enrollment-verified icon-md"></i> Itens Cotados</h3>'+
        '        </div>'+
        '        <div class="col-md-1"></div>'+
        '    </div><!--row-->'+
        //Produtos cotados -->LOOP
        '    <div id="loopItens___'+ordem+'"></div><!--loopItens___-->'+
        
        '</div><!--ordem-->'

    )  

    //Loop itens da cotação->

    if(produtos.length){
        console.log('produtos -> '+ produtos.length);
        for(var i = 0; i < produtos.length; i++){
			var _disponivel = produtos[i].disponivel;
			
            var _quantidade = produtos[i].quantidade;
			var _valor = produtos[i].preco; 
			
            var _nomeProd = produtos[i].titulo; 
            var _qtdDesejada = produtos[i].qtdDesejada; 
            $("#loopItens___"+ordem).append(

                '    <div class="row" id="proposta___'+ordem+'">'+
                '        <div class="col-md-1"></div>'+
                '        <div class="col-md-4 col-sm-12">'+
                '           <h3>Item</h3> '+
                '           <h4 style="color:#2E66B7;">'+_nomeProd+'</h4>'+
                '        </div>'+
                '        <div class="col-md-2 col-sm-12">'+
                '           <h3>Quantidade</h3> '+
                '           <h4 style="color:#2E66B7;">'+_quantidade+' de '+_qtdDesejada+ '</h4>'+
                '        </div>'+
                '        <div class="col-md-2 col-sm-12">'+
                '           <h3>Valor</h3> '+
                '           <h4 style="color:#2E66B7;">'+_valor+'</h4>'+
                '        </div>  '+
                '        <div class="col-md-2 col-sm-12">'+
                '           <h3>Total</h3> '+
                '           <h4 style="color:#2E66B7;">'+reais(parseInt(_quantidade) * floatReais(_valor))+'</h4>'+
                '        </div>  '+
                '        <div class="col-md-1"></div> '+
				'    </div><!--row-->'+
				'<div class ="row">'+
				'	<div class="col-md-1"></div>'+
				'	<div class="col-md-10"><hr></div>'+
				'	<div class="col-md-1"></div>'+
				'</div><!--row-->'
                
			)
			if(_disponivel == 'nao'){
				$("#proposta___"+ordem).css('background-color:red');		
			}
        }
    }  
}

/** @fnfinalizarCotacao() - Finaliza a inserção de cotações na etapa de compras - 5 
 * @author {Jean Varlet}
 * 
 */
function fnFinalizarCotacao(){
	console.log('finalizarCotacao()-> ')
	showLoading('', 'show', 'Salvando cotações...');
	if(fnBlockCotacoes('finalizar')){
		console.log('após fnBlockCotacoes()')
		if(saveCotacoes()){
			console.log('após saveCotacoes()')
			setTimeout(function(){
				parseCotacoesMapa(5);
				showLoading('', 'hide', '');
				$("#divBtnEnviar").fadeIn();
				setTimeout(function(){
					$("#navPorForn").click(function(){
						$("#dvShowMapaItem").hide();	
						$("#dvShowMapaForn").fadeIn();
						parseCotacoesMapa(5);
						//disableSelections(atividade);
					
					})
					$("#navPorItem").click(function(){
						$("#dvShowMapaForn").hide();	
						$("#dvShowMapaItem").fadeIn();
						parseCotByPrice(5);
						//disableSelections(atividade);	
					})
				},500)
				$("#inputCotacoes").val('sim');
			},1000)
		}
	}else{
		showLoading('', 'hide', '');
		toast('Erro!', 'Ocorreu um erro ao finalizar a inserção das cotações.', 'danger');
		$("#addCotacao").prop("disabled", false);
	}
}

function fnBlockCotacoes(acao){
	if(acao == 'adicionar'){
		$("#dvCotacoesFilhos").children().each(function(){
			//Percorrer campos e bloquear - considerar autocompletes e switchs
		})
	}else if(acao == 'finalizar'){
		$("#addCotacao").prop("disabled", true);
		$("#dvCotacoesFilhos").children().each(function(){
			//Percorrer campos e bloquear - considerar autocompletes e switchs
		})
	}
	
	return true;
}