

/**Exibe os dados do cabeçalho da solicitação
@param {none} --
 */
function cabecalhoSolicitacao(atividade){
	if(atividade == 0 || atividade == 4 ){
		$("#dataSolicitacao").val(moment().format("DD/MM/YYYY"));
		$("#nomeSolicitante").val(parent.WCMAPI.getUser());
		$("#matriculaSolicitante").val(parent.WCMAPI.getUserCode());
	}
	$("#headerDtSolicitacao").html('<i >'+ moment().format("DD/MM/YYYY")+'</i>');
	
	$("#nomeColabEtapa").html(nomeExibicaoColab());
	
	
	if($("#codigoSolicitacao").val() == ''){
		$("#headerCodSolicitacao").html('O código será preenchido posteriormente');
	}else{
		$("#headerCodSolicitacao").html($("#codigoSolicitacao").val());
	}
}

/** Corrige os ids dos itens de rateio na tabela de exibição do item
 * de compra.
 * @author {David}
 */
function correctIds(){

	let ds = Array.from(document.querySelectorAll("#dvTableRateios"));
	let addRateio = Array.from(document.querySelectorAll("#addProjAcao"));
	ds = ds.slice(1, ds.length);
	addRateio = addRateio.slice(1, addRateio.length);
	for(let i = 1; i<=ds.length; i++){
		const actual = ds[i - 1].querySelectorAll("[id]");
		actual.forEach(el => {
			el.id = `${el.id}___${i}`;
			el.name = `${el.name}___${i}`;
		});
		addRateio[i - 1].id = `${addRateio[i - 1].id}___${i}`;
		addRateio[i - 1].name = `${addRateio[i - 1].name}___${i}`;
	}
}

/**Auxiliar de showEtapaForm() - Exibe div
* @param {string} - id
* @returns {boolean}
 */
function showDiv(...id){
    if(id.length){
        for(var i = 0; i < id.length; i++){
            $("#"+id[i]).fadeIn();
        }
    }
	
}
/**Auxiliar de showEtapaForm() - Exibe div
* @param {string} - id
* @returns {boolean}
 */
function hideDiv(...id){
    if(id.length){
        for(var i = 0; i < id.length; i++){
            $("#"+id[i]).fadeOut();
        }
    }
	
}
/** Mostra o header personalizado correto
* @param {boolean} - isMobile
 */
function showHeader(isMobile){
	let div;
	if(isMobile){
		div = 'headerMobile';
	}else{
		div = 'headerDesktop';
	}
	showDiv(div);
}
/** @function callCreateId -> Chamada para gerarIdSolicitacao() via Promise
 * @author {Jean Varlet}
 */
function callCreateId(){
    return new Promise((resolve, reject) =>{
        resolve(gerarIdSolicitacao('codigoSolicitacao'));
        reject('Erro ao executar gerar codigoSolicitacao()')
    });
}
/** Preenche o número da solicitação no cabeçalho padrão do SEBRAE PE
 * @param {int} -> etapa do processo
 * @author{Jean Varlet}
 */
function showNrSolicitacao(etapa){
	if(etapa > 4){
		if($("#nrSolicitacao").val() != ''){
			$("#spcodSolicitacao").text($("#nrSolicitacao").val());
		}else{
			console.log('showNrSolicitacao()-> Não retornou nrSolicitacao')
		}
	}
}