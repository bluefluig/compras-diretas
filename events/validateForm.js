function validateForm(form){
	
	var atividade = getValue("WKNumState");
	var msg = "";
	
	if(atividade == 0 || atividade == 4){
		if(form.getValue("solResponsavel") == "nao"){
			if(form.getValue("matriculaResponsavel") == ""){
				msg += "É necessário informar o analista responsável por essa solicitação";
			}
		}
		if(form.getValue("anexarProposta") == "sim"){
			if(form.getValue("anexado") == "nao"){
				msg += "Não esqueça de anexar a proposta balizadora";
			}
		}
		if(form.getValue("solNoPrazo15") == "invalida"){
			msg += "É necessário informar uma data de entrega válida";
		}
		if(form.getValue("eventoSWh") == "sim"){
			if(form.getValue("selEventoZoom") == ""){
			msg += "É necessário informar o evento referente a essa solicitação de compra.";
			}
		}
		if(form.getValue("solNoPrazo15") == "nao"){
			if(form.getValue("justifica15dias") == ""){
				msg += "Prazo menor que 15 dias! Necessário justificar o motivo";
			}
		}
	}
	if(atividade == 5){
		if(form.getValue("decisaoAssUABS") == "ajuste"){
			if(form.getValue("descAjustUABS") == ""){
				msg += "É necessário descrever os ajustes necessários.";
			}
		}
		if(form.getValue("decisaoAssUABS") == "cancelar"){
			if(form.getValue("descCancelUABS") == ""){
				msg += "É necessário Justificar o motivo do cancelamento.";
			}
		}
	}
	
	if(msg != ""){
		throw msg;	
	}
		
}