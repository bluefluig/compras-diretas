function displayFields(form,customHTML){
	
	form.setShowDisabledFields(true);	
	
	var modoVisualizacao = form.getFormMode();
	var atividade = getValue("WKNumState");	
	
	var numProces = getValue("WKNumProces");
	var numproc = parseInt(getValue("WKNumProces"));
	
	var habilitar = false; // Informe True para Habilitar ou False para Desabilitar os campos
	
    var mapaForm = new java.util.HashMap();
    mapaForm = form.getCardData();
    var it = mapaForm.keySet().iterator();     
    while (it.hasNext()) { // Laço de repetição para habilitar/desabilitar os campos
        var key = it.next();
        form.setEnabled(key, habilitar);
    }
    
    customHTML.append("<script>");
	customHTML.append("var ATIVIDADE_ATUAL = "+ atividade + ";" );
	customHTML.append("var MODO_ATUAL = '"+ modoVisualizacao + "';" );
	customHTML.append("</script>");		
    customHTML.append("<script>");
    customHTML.append("\n   var WKAtv     =  " + atividade + ";");
    customHTML.append("\n   var WKProc    =  " + numProces + ";");
    customHTML.append("\n   var isMobile  =  " + form.getMobile() + ";");
    customHTML.append("\n </script>"); 
    
    
	//form.setEnabled('temaCapacitacaoh', true);
	form.setEnabled('codigoSolicitacao', true);
	form.setEnabled('nrSolicitacao', true);// Id Solicitação padrão Fluig ---
	form.setEnabled('conteudoRateios', true);
	form.setEnabled('dataEntrega', true);
	form.setEnabled("solNoPrazo15", true);
	//Vinculação a um evento 15/09/2020 -- Jean Varlet
	form.setEnabled("eventoSW", true);
	form.setEnabled("eventoSWh", true);
	form.setEnabled("selEventoZoom", true);
	form.setEnabled("cadastrarEvento", true);
	form.setEnabled("conteudoCotacao", true);//JSON das cotações 
	form.setEnabled("pastaAnexos", true);
	form.setEnabled("nomeCadastrar", true);

	//Jean Varlet - 23/09/2020 - 
	form.setEnabled('informouValPrevisto', true);//sim/nao
	form.setEnabled('informouOrcPrevisto', true);//sim/nao
	form.setEnabled("anexarProposta", true);
	form.setEnabled("anexado", true);
	form.setEnabled("itensCompCot", true);
	form.setEnabled('rootFolder', true);//Pasta principal de anexos da solicitacao

	//Jean Varlet --- 25/09/2020 - Campos para produtos do RM
	form.setEnabled("idProduto",true);
	form.setEnabled("codProduto",true);
	form.setEnabled("tituloProd",true);

	form.setEnabled('dataSolicitacao', true);

	//Switchs
	form.setEnabled('swPropostaBaliz', true); 
	form.setEnabled('custoAproxItem', true);
	form.setEnabled('estimativaTotal', true);
	form.setEnabled('orcamentodisp', true);
	form.setEnabled('projAcUABS',true); 
	form.setEnabled('saldoUABS',true);    	
	form.setEnabled('aprovUABS',true);    	
	form.setEnabled('ajusteUABS',true); 

	form.setEnabled('aprovAnalUABS', true);
	form.setEnabled('justCancelUABS', true);
	form.setEnabled('aprovaDaf', true);
	form.setEnabled('ajusteDaf', true);

	//Jean Varlet -- Etapa envia AF

	form.setEnabled('matriculaEnviaAF', true);
	form.setEnabled('nomeEnviaAF', true);
	form.setEnabled('dataEnviaAF', true);

	form.setEnabled('matriculaAjusUABS', true);
	form.setEnabled('nomeAjusUABS', true);

	form.setEnabled('conteudoFinal', true);
	form.setEnabled('unidadeSolicitante', true);
	form.setEnabled('nomeResponsavel', true);
	form.setEnabled('solResponsavel', true);
	form.setEnabled('matriculaResponsavel', true);
	form.setEnabled('gerenteUnd', true);
	form.setEnabled('diretorUnd', true);
	form.setEnabled('cargoSolicitante', true);
	form.setEnabled('codSecao', true);
	form.setEnabled('codSecaoResponsavel', true);
	form.setEnabled('nomeSolicitante', true);
	form.setEnabled('matriculaSolicitante', true);

	form.setEnabled('situacaoAssinaturas', true);
	form.setEnabled('dataFimSolicitacao', true);
	form.setEnabled('nrSolAssinaturas', true);
	form.setEnabled('movimentosGerados', true);

	//Jean Varlet - 24/10/2020

	form.setEnabled("estadoCustoAprox", true);
	
    if(atividade == 0 || atividade == 4){//Início   

		form.setEnabled('analistaResponsavel', true);   	
		  	
		form.setEnabled('valorNecessario', true);   	
		form.setEnabled('valorTotItemRat', true);   	
		form.setEnabled('justifica15dias', true);   	
    	//hidden fields
		form.setEnabled('matriculaSolicitante', true);
		form.setEnabled('nomeSolicitante', true);
		
		
		form.setEnabled('nomeResponsavel', true);
		//form.setEnabled('dataSolicitacao', true);
		
		form.setEnabled('produtoRm', true);
		form.setEnabled('custoAproximado', true);
		form.setEnabled('quantidade', true);
		form.setEnabled('orcamentodisp', true);
		form.setEnabled('valorOrcamentoGer', true);
		form.setEnabled('valorEstimativaGer', true);
		form.setEnabled('ordemProduto', true);
		form.setEnabled('valCustoAproxItem', true);
		form.setEnabled('especificacao', true);
		form.setEnabled('rateios', true);
		form.setEnabled('selUnidade', true);
		form.setEnabled('selProjeto', true);
		form.setEnabled('selAcao', true);
		form.setEnabled('selCentroCusto', true);
		form.setEnabled('perRateio', true);
		form.setEnabled('valorRateio', true);
		form.setEnabled('obsItem', true);
		form.setEnabled('valorEstimativaGer', true);
		form.setEnabled('valorOrcamentoGer', true);
		form.setEnabled('codSecaoResponsavel', true);
		form.setEnabled('unidadeSolicitante', true);
		form.setEnabled('nomediretorUnd', true);
		//Jean Varlet - Saldos de naturezas na seleção do item de compra ----
		form.setEnabled('naturezaProd', true);
		form.setEnabled('naturezaSald', true);
		form.setEnabled('naturezaAno', true);
		form.setEnabled('naturezaCod', true);
		form.setEnabled('naturezaProdSel', true);
	
    }
    if(atividade == 5){//Assistente UABS  
  	
		form.setEnabled('descAjustUABS',true);   
		form.setEnabled('descCancelUABS',true);   
		//Campos necessários entre etapas do processo
		form.setEnabled('nomeSolicitante', true);
		form.setEnabled('solResponsavel', true);
		form.setEnabled('matriculaResponsavel', true);
		
		form.setEnabled('gerenteUnd', true);
		form.setEnabled('codSecao', true); 
		form.setEnabled('custoAproxItem', true);
		//hidden fields
		form.setEnabled('matriculaAssUABS', true);
		form.setEnabled('dataAssUABS', true);
		form.setEnabled('nomeAssUABS', true);
		form.setEnabled('decisaoAssUABS', true);

		// Cotações ----
		
		form.setEnabled("selCNPJForn", true);
		form.setEnabled("razaoForn", true);
		form.setEnabled("fantasiaForn", true);
		form.setEnabled("EntPrevisCot", true);
		form.setEnabled("anexarPropCot", true);
		//form.setEnabled("valTotCot", true);
		form.setEnabled("obscotacoes", true);
		form.setEnabled("inputCotacoes", true);
		form.setEnabled("solAprovGerenteValor", true);
    	
    }
    if(atividade == 12){//Ajuste Solicitante

		//hidden fields     	
    	form.setEnabled('matriculaAjuste', true);
		form.setEnabled('dataAjuste', true);
		form.setEnabled('justifica15dias', true);  

		//Campos para interação com os itens de compras 
		form.setEnabled('produtoRm', true);
		form.setEnabled('custoAproximado', true);
		form.setEnabled('quantidade', true);
		form.setEnabled('orcamentodisp', true);
		form.setEnabled('valorOrcamentoGer', true);
		form.setEnabled('valorEstimativaGer', true);
		form.setEnabled('ordemProduto', true);
		form.setEnabled('valCustoAproxItem', true);
		form.setEnabled('especificacao', true);
		form.setEnabled('rateios', true);
		form.setEnabled('selUnidade', true);
		form.setEnabled('selProjeto', true);
		form.setEnabled('selAcao', true);
		form.setEnabled('selCentroCusto', true);
		form.setEnabled('perRateio', true);
		form.setEnabled('valorRateio', true);
		form.setEnabled('obsItem', true);
		form.setEnabled('valorEstimativaGer', true);
		form.setEnabled('valorOrcamentoGer', true);
		form.setEnabled('codSecaoResponsavel', true);
		form.setEnabled('unidadeSolicitante', true);
    	
	}
	if(atividade == 18){

		form.setEnabled('swAprovGerente', true);
		form.setEnabled('matriculaGerenteAprov', true);
		form.setEnabled('nomeGerenteAprov', true);
		form.setEnabled('dataGerenteAprov', true);
		form.setEnabled('decisaoGerenteAprov', true);
	}
	if(atividade == 25){

		form.setEnabled('cancelAnalUABS', true);
		form.setEnabled('justJustifUABS', true);
	}

	if(atividade == 32){
		form.setEnabled('matriculaDecDAF', true);
		form.setEnabled('nomeDecDAF', true);
		form.setEnabled('dataDecDAF', true);
		form.setEnabled('decisaoDecDAF', true);
		form.setEnabled('continuidade', true);
		

		form.setEnabled('dafJustCancelamento', true);
		form.setEnabled('dafJustAjuste', true);
	}
	if(atividade == 38){//Enviar AF Fornecedor
		form.setEnabled('enviarAf', true);
	}
    
    
}